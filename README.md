# What is this ?

This is a framework on top of Vert.x that I use for many of my projects

# Can I use it ?

Of course ! But please be aware that it is not complete, and very weird/rough in some places.

# Will you maintain this ?

As long as my current stack includes Vert.x, probably

# Can I contribute ?

Sure ! Open an MR and tag me !

# Can you add X feature ?

Maybe, maybe not. As this framework is currently for my own use, if I don't need the feature, I may not implement it.
That being said, if I find the idea cool enough, I might :)

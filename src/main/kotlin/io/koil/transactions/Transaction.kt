package io.koil.transactions

import io.koil.core.EFailable
import io.koil.core.Failable
import org.slf4j.LoggerFactory

/** Implement this to pass data between transaction steps in a type-safe manner */
interface TransactionContext {
    companion object {
        fun empty(): TransactionContext = EmptyTransactionContext()
    }
}

class EmptyTransactionContext : TransactionContext

fun interface TxRollback {
    suspend fun rollback()
}

data class TxStepResult<NT : TransactionContext>(
    val newContext: NT,
    val rollbackFn: TxRollback? = null,
)

fun interface TxFn<T : TransactionContext, NT : TransactionContext> {
    suspend fun execute(ctx: T): TxStepResult<NT>
}

fun interface FailableTxFn<T : TransactionContext, NT : TransactionContext> {
    suspend fun execute(ctx: T): EFailable<TxStepResult<NT>>
}

private data class RollbackEntry(
    val name: String,
    val fn: TxRollback,
)

private val identity: TxRollback = TxRollback {}

interface Transaction<T : TransactionContext> {
    companion object {
        fun <T : TransactionContext> create(initialContext: T): Transaction<T> =
            TransactionContinuation(initialContext, emptyList())
    }

    suspend fun <NT : TransactionContext> executeStep(
        name: String,
        fn: TxFn<T, NT>,
    ): Transaction<NT>

    suspend fun <NT : TransactionContext> executeFailableStep(
        name: String,
        fn: FailableTxFn<T, NT>
    ): Transaction<NT>

    /** Returns the final context, or the error if the transaction failed and was rolled back */
    fun commit(): Failable<T, Throwable>
}

private class FailedTransaction<T : TransactionContext>(private val error: Throwable) : Transaction<T> {
    override suspend fun <NT : TransactionContext> executeStep(name: String, fn: TxFn<T, NT>): Transaction<NT> =
        FailedTransaction(error)

    override suspend fun <NT : TransactionContext> executeFailableStep(
        name: String,
        fn: FailableTxFn<T, NT>
    ): Transaction<NT> = FailedTransaction(error)

    override fun commit(): Failable<T, Throwable> = Failable.Failed(error)
}

private class TransactionContinuation<T : TransactionContext>(
    private val ctx: T,
    private val rollbackFns: List<RollbackEntry>,
) : Transaction<T> {
    companion object {
        private val logger = LoggerFactory.getLogger(Transaction::class.java)
    }

    override suspend fun <NT : TransactionContext> executeStep(name: String, fn: TxFn<T, NT>): Transaction<NT> {
        val result = try {
            fn.execute(ctx)
        } catch (t: Throwable) {
            logger.error("Transaction step {} failed", name, t)
            rollback()
            return FailedTransaction(t)
        }

        return TransactionContinuation(
            result.newContext,
            rollbackFns.plus(RollbackEntry(name, result.rollbackFn ?: identity))
        )
    }

    override suspend fun <NT : TransactionContext> executeFailableStep(
        name: String,
        fn: FailableTxFn<T, NT>
    ): Transaction<NT> {
        val result = try {
            fn.execute(ctx)
        } catch (t: Throwable) {
            logger.error("Transaction step {} failed", name, t)
            rollback()
            return FailedTransaction(t)
        }

        if (Failable.isFailed(result)) {
            rollback()
            return FailedTransaction(result.error)
        }

        return TransactionContinuation(
            result.value.newContext,
            rollbackFns.plus(RollbackEntry(name, result.value.rollbackFn ?: identity))
        )
    }

    private suspend fun rollback() {
        for ((name, rollbackFn) in rollbackFns.asReversed()) {
            try {
                rollbackFn.rollback()
            } catch (t: Throwable) {
                logger.error("Rollback failed for step {}", name, t)
            }
        }
    }

    override fun commit(): Failable<T, Throwable> = Failable.Success(ctx)
}

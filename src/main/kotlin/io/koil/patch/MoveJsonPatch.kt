package io.koil.patch

import com.fasterxml.jackson.databind.node.NullNode
import com.fasterxml.jackson.databind.node.ObjectNode
import io.koil.schemas.ArrayJsonSchema
import io.koil.schemas.JsonSchema

val moveJsonPatchSchema = JsonSchema.obj("MoveJsonPatch")
    .description("A json patch to move a property")
    .implIdentifier(MoveJsonPatch::class.qualifiedName!!)
    .polymorphicValue(MoveJsonPatch.OP)
    .property(
        "from",
        JsonSchema.jsonPath().description("The source property to move the value from. Will be set to null.")
    )
    .build()

class MoveJsonPatch(
    path: JsonPath,
    val from: JsonPath,
) : JsonPatch(OP, path) {
    override fun validate(schema: JsonSchema): Boolean {
        val sourceSchema = validatePath(from, schema)
            ?: return false
        val targetSchema = validatePath(path, schema)
            ?: return false

        if (!sourceSchema.nullable && sourceSchema.schema !is ArrayJsonSchema) return false
        // TODO is assignable check
        return true
    }

    override fun apply(schema: JsonSchema, obj: ObjectNode): Boolean {
        if (path.original == from.original) return true

        if (path.isRoot) TODO("Apply all fields of value to obj")

        val fromProperties = getProperties(from, schema, obj)
        if (fromProperties.size != 1) return false
        val fromProperty = fromProperties.first()
        if (fromProperty.containerSchema !is ArrayJsonSchema && fromProperty.schema.validate(
                fromProperty.nullable,
                NullNode.instance
            ) != null
        ) return false
        val value = fromProperty.get()

        fromProperty.remove()

        val mutableProperties = getProperties(path, schema, obj)
        if (mutableProperties.isEmpty()) return false
        // First run validation
        for (prop in mutableProperties) {
            if (prop.schema.validate(prop.nullable, value) != null) return false
        }
        // Then run the actual mutation
        for (prop in mutableProperties) {
            if (!prop.set(value, true)) return false
        }

        return schema.validate(false, obj) == null
    }

    companion object {
        const val OP = "move"
    }
}

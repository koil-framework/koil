package io.koil.patch

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode
import io.koil.schemas.JsonSchema

val testJsonPatchSchema = JsonSchema.obj("TestJsonPatch")
    .description("A json patch to test a property value")
    .implIdentifier(TestJsonPatch::class.qualifiedName!!)
    .polymorphicValue(TestJsonPatch.OP)
    .property("value", JsonSchema.anyJson("The value to test the property against"))
    .build()

class TestJsonPatch(
    path: JsonPath,
    val value: JsonNode,
) : JsonPatch(OP, path) {
    override fun validate(schema: JsonSchema): Boolean {
        val propSchema = validatePath(path, schema)
            ?: return false

        return propSchema.schema.validate(propSchema.nullable, value) == null
    }

    override fun apply(schema: JsonSchema, obj: ObjectNode): Boolean {
        if (path.isRoot) return obj == value

        val mutableProperties = getProperties(path, schema, obj)
        if (mutableProperties.isEmpty()) return false
        for (prop in mutableProperties) {
            val propValue = prop.get()
            if (propValue != value) return false
        }

        return true
    }

    companion object {
        const val OP = "test"
    }
}

package io.koil.patch

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.NullNode
import com.fasterxml.jackson.databind.node.ObjectNode
import io.koil.schemas.ArrayJsonSchema
import io.koil.schemas.JsonSchema
import io.koil.schemas.JsonSchemaObjectProperty
import io.koil.schemas.ObjectJsonSchema

// PoI: https://stackoverflow.com/questions/44304480/how-to-set-delegated-property-value-by-reflection-in-kotlin

internal interface MutableJsonValue {
    val containerSchema: JsonSchema
    val schema: JsonSchema
    val nullable: Boolean

    fun get(): JsonNode
    fun set(value: JsonNode, add: Boolean = false): Boolean
    fun remove()
}

internal class PropertyJsonValue(
    override val containerSchema: JsonSchema,
    override val schema: JsonSchema,
    override val nullable: Boolean,
    private val container: ObjectNode,
    private val property: String,
) : MutableJsonValue {
    override fun get(): JsonNode = container.get(property) ?: NullNode.instance
    override fun set(value: JsonNode, add: Boolean): Boolean {
        container.replace(property, value)
        return true
    }

    override fun remove() {
        container.remove(property)
    }
}

internal class ArrayItemJsonValue(
    override val containerSchema: JsonSchema,
    override val schema: JsonSchema,
    override val nullable: Boolean,
    private val container: ArrayNode,
    /** Null means last index */
    private var index: Int?,
) : MutableJsonValue {
    override fun get(): JsonNode {
        val i = index ?: (container.size() - 1)
        if (i < 0) return NullNode.instance
        return container.get(i)
    }

    override fun set(value: JsonNode, add: Boolean): Boolean {
        if (index != null) {
            if (index!! < 0) return false

            if (add) {
                // Spec requires us to not add the value if the index is out of bounds
                if (index!! > container.size()) return false
                else container.insert(index!!, value)
            } else container[index!!] = value
        } else {
            index = container.size()
            container.add(value)
        }

        return true
    }

    override fun remove() {
        // TODO Empty array check
        if (index != null) container.remove(index!!)
        else container.remove(container.size() - 1)
    }
}

internal data class SubSchemaEntry(
    val schema: JsonSchema,
    val nullable: Boolean,
)

internal fun validatePath(path: JsonPath, schema: JsonSchema): SubSchemaEntry? {
    if (path.isRoot) return SubSchemaEntry(schema, false)

    if (!path.pointer) TODO("Schema validation is only compatible with json pointers, not json paths")

    var subSchema = SubSchemaEntry(schema, false)
    for (segment in path) {
        segment as JsonPathSegment.Property

        val curSchema = subSchema.schema
        if (curSchema is ArrayJsonSchema) {
            if (!segment.maybeArrayIndex) return null
            else subSchema = SubSchemaEntry(curSchema.items, curSchema.nullableItems)
        } else if (curSchema is ObjectJsonSchema) {
            val property = curSchema.properties.firstOrNull { it.name == segment.content }
                ?: return null
            subSchema = SubSchemaEntry(property.schema, property.nullable)
        } else return null
    }

    return subSchema
}

internal fun getProperties(path: JsonPath, schema: JsonSchema, obj: ObjectNode): List<MutableJsonValue> {
    if (!path.pointer) TODO("Schema validation is only compatible with json pointers, not json paths")

    var subSchema = schema
    var container: JsonNode = obj
    for (segment in path) {
        // We only want to go n-1, not n
        if (segment.next == null) break
        segment as JsonPathSegment.Property

        if (subSchema is ArrayJsonSchema) {
            if (!segment.maybeArrayIndex) return emptyList()
            else {
                subSchema = subSchema.items
                // TODO Validate container is the proper type
                // TODO Nullable warning
                container = container.get(segment.arrayIndex!!)
            }
        } else if (subSchema is ObjectJsonSchema) {
            val property = subSchema.properties.firstOrNull { it.name == segment.content }
                ?: return emptyList()
            subSchema = property.schema
            // TODO Validate container is the proper type
            // TODO Nullable warning
            container = container.get(segment.content)
        } else return emptyList()
    }

    val head = path.head as JsonPathSegment.Property
    if (subSchema is ArrayJsonSchema) {
        // TODO Validate container is the proper type
        return listOf(
            ArrayItemJsonValue(
                subSchema,
                subSchema.items,
                subSchema.nullableItems,
                container as ArrayNode,
                head.arrayIndex
            )
        )
    } else if (subSchema is ObjectJsonSchema) {
        // TODO Validate container is the proper type
        val property = subSchema.properties.firstOrNull { it.name == head.content }
            ?: return emptyList()
        return listOf(
            PropertyJsonValue(
                subSchema,
                property.schema,
                property.nullable,
                container as ObjectNode,
                head.content
            )
        )
    }

    return emptyList()
}

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.EXISTING_PROPERTY,
    property = "op",
    visible = false
)
@JsonSubTypes(
    JsonSubTypes.Type(AddJsonPatch::class, name = AddJsonPatch.OP),
    JsonSubTypes.Type(ReplaceJsonPatch::class, name = ReplaceJsonPatch.OP),
    JsonSubTypes.Type(TestJsonPatch::class, name = TestJsonPatch.OP),
    JsonSubTypes.Type(RemoveJsonPatch::class, name = RemoveJsonPatch.OP),
    JsonSubTypes.Type(MoveJsonPatch::class, name = MoveJsonPatch.OP),
    JsonSubTypes.Type(CopyJsonPatch::class, name = CopyJsonPatch.OP),
)
sealed class JsonPatch(val op: String, val path: JsonPath) {
    abstract fun validate(schema: JsonSchema): Boolean
    abstract fun apply(schema: JsonSchema, obj: ObjectNode): Boolean

    companion object {
        val schema = JsonSchema.obj("JsonPatch")
            .implIdentifier(JsonPatch::class.qualifiedName!!)
            .description("A json patch definition")
            .property(
                JsonSchemaObjectProperty(
                    "op",
                    JsonSchema.string()
                        .description("The type of the operation")
                        .enum(
                            setOf(
                                AddJsonPatch.OP,
                                ReplaceJsonPatch.OP,
                                TestJsonPatch.OP,
                                RemoveJsonPatch.OP,
                                MoveJsonPatch.OP,
                                CopyJsonPatch.OP,
                            )
                        ).build(),
                    true,
                    false,
                    "op",
                    emptySet(),
                    null,
                )
            ).property(
                "path",
                JsonSchema.jsonPath()
                    .description("The target property")
            )
            .subType(addJsonPatchSchema)
            .subType(replaceJsonPatchSchema)
            .subType(testJsonPatchSchema)
            .subType(removeJsonPatchchema)
            .subType(moveJsonPatchSchema)
            .subType(copyJsonPatchSchema)
            .build()
    }
}

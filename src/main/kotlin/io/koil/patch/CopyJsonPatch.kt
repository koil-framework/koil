package io.koil.patch

import com.fasterxml.jackson.databind.node.ObjectNode
import io.koil.schemas.JsonSchema

val copyJsonPatchSchema = JsonSchema.obj("CopyJsonPatch")
    .description("A json patch to copy a property")
    .implIdentifier(CopyJsonPatch::class.qualifiedName!!)
    .polymorphicValue(CopyJsonPatch.OP)
    .property("from", JsonSchema.jsonPath().description("The source property to get the value from"))
    .build()

class CopyJsonPatch(
    path: JsonPath,
    val from: JsonPath,
) : JsonPatch(OP, path) {
    override fun validate(schema: JsonSchema): Boolean {
        val sourceSchema = validatePath(from, schema)
            ?: return false
        val targetSchema = validatePath(path, schema)
            ?: return false

        // TODO is assignable check
        return true
    }

    override fun apply(schema: JsonSchema, obj: ObjectNode): Boolean {
        if (path.isRoot) TODO("Apply all fields of value to obj")

        val fromProperties = getProperties(from, schema, obj)
        if (fromProperties.size != 1) return false
        val value = fromProperties.first().get()

        val mutableProperties = getProperties(path, schema, obj)
        if (mutableProperties.isEmpty()) return false
        // First run validation
        for (prop in mutableProperties) {
            if (prop.schema.validate(prop.nullable, value) != null) return false
        }
        // Then run the actual mutation
        for (prop in mutableProperties) {
            prop.set(value)
        }

        return schema.validate(false, obj) == null
    }

    companion object {
        const val OP = "copy"
    }
}

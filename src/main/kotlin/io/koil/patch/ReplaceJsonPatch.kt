package io.koil.patch

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode
import io.koil.schemas.JsonSchema

val replaceJsonPatchSchema = JsonSchema.obj("ReplaceJsonPatch")
    .description("A json patch to replace a property value")
    .implIdentifier(ReplaceJsonPatch::class.qualifiedName!!)
    .polymorphicValue(ReplaceJsonPatch.OP)
    .property("value", JsonSchema.anyJson("The value to set"))
    .build()

class ReplaceJsonPatch(
    path: JsonPath,
    val value: JsonNode,
) : JsonPatch(OP, path) {
    override fun validate(schema: JsonSchema): Boolean {
        val propSchema = validatePath(path, schema)
            ?: return false

        return propSchema.schema.validate(propSchema.nullable, value) == null
    }

    override fun apply(schema: JsonSchema, obj: ObjectNode): Boolean {
        if (path.isRoot) TODO("Apply all fields of value to obj")

        val mutableProperties = getProperties(path, schema, obj)
        if (mutableProperties.isEmpty()) return false
        // First run validation
        for (prop in mutableProperties) {
            if (prop.schema.validate(prop.nullable, value) != null) return false
        }
        // Then run the actual mutation
        for (prop in mutableProperties) {
            prop.set(value)
        }

        return schema.validate(false, obj) == null
    }

    companion object {
        const val OP = "replace"
    }
}

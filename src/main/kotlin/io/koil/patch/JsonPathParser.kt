package io.koil.patch

private val rootPath = JsonPath("", false, null, null)

internal class JsonPathParser(
    private val path: String
) {
    private var i = 1
    private val end = path.length
    private var segmentIndex = 0

    private var tail: JsonPathSegment? = null
    private var current: JsonPathSegment? = null

    private fun ended(): Boolean {
        return i >= end
    }

    private fun increment(): Boolean {
        i++
        return i < end
    }

    private fun readUntil(c: Char): String {
        val sb = StringBuilder()

        while (!ended()) {
            val cc = path[i]
            if (cc == c) break

            sb.append(cc)
            increment()
        }

        return sb.toString()
    }

    private fun peek(): Char {
        if (i + 1 == end) return Char.MIN_VALUE
        return path[i + 1]
    }

    private fun assertNextChar(c: Char): Boolean {
        if (ended()) return true

        if (path[i] == c) {
            i++
            return true
        }

        return false
    }

    private fun readPropertyName(untilEndBracket: Boolean = false, untilQuote: Boolean = false): String? {
        val sb = StringBuilder()

        while (!ended()) {
            val cc = path[i]
            if (cc == '.' || cc == '[') break
            else if (untilEndBracket && cc == ']') break
            else if (untilQuote && (cc == '\'' || cc == '"')) break
            else if (!cc.isLetterOrDigit()) return null


            sb.append(cc)
            increment()
        }

        return sb.toString()
    }

    fun parse(): JsonPath? {
        // Early return for specific cases
        if (path == "$") return rootPath
        else if (path[0] != '$') return null

        while (!ended()) {
            val c = path[i]

            val segment = if (c == '[') {
                // Bracket notation
                parseBracketNotation() ?: return null
            } else if (c == '.') {
                // Dot notation
                if (!increment()) return null // TODO This is wrong

                val content = readPropertyName() ?: return null
                JsonPathSegment.Property(segmentIndex++, content, current, null)

                // TODO Take recursive descent into account here
            } else {
                // Unknown, unexpected char
                return null
            }

            if (current != null) (current as MutableSegment).nextSegment = segment
            if (tail == null) tail = current
            current = segment
        }

        return JsonPath(path, false, tail, current)
    }

    private fun parseBracketNotation(): JsonPathSegment? {
        if (!increment()) return null

        val quote = path[i]
        if (quote == '\'' || quote == '"') {
            // Quoted content
            if (!increment()) return null

            val content = readPropertyName(untilQuote = true) ?: return null
            if (!assertNextChar(quote)) return null
            if (!assertNextChar(']')) return null

            return JsonPathSegment.Property(segmentIndex++, content, current, null)
        } else if (quote == '?' && peek() == '(') {
            // Filter
            i++
            if (!increment()) return null

            val startPos = i
            val expression = parseExpression() ?: return null
            val endPos = i
            if (!assertNextChar(']')) return null

            return JsonPathSegment.Filter(segmentIndex++, path.substring(startPos, endPos), current, null, expression)
        } else if (quote == '(') {
            // Script expression
            if (!increment()) return null

            val startPos = i
            val expression = parseExpression() ?: return null
            val endPos = i
            if (!assertNextChar(']')) return null
        } else {
            // Simple, unquoted content
            val segmentContent = readPropertyName(untilEndBracket = true) ?: return null
            if (!assertNextChar(']')) return null
        }

        TODO()
    }

    private fun parseExpression(): JsonPathExpression? {
        TODO()
    }
}

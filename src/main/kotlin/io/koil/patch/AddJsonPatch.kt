package io.koil.patch

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode
import io.koil.schemas.JsonSchema

val addJsonPatchSchema = JsonSchema.obj("AddJsonPatch")
    .description("A json patch to add a property value")
    .implIdentifier(AddJsonPatch::class.qualifiedName!!)
    .polymorphicValue(AddJsonPatch.OP)
    .property("value", JsonSchema.anyJson("The value to set"))
    .build()

class AddJsonPatch(
    path: JsonPath,
    val value: JsonNode,
) : JsonPatch(OP, path) {
    override fun validate(schema: JsonSchema): Boolean {
        val propSchema = validatePath(path, schema)
            ?: return false

        return propSchema.schema.validate(propSchema.nullable, value) == null
    }

    override fun apply(schema: JsonSchema, obj: ObjectNode): Boolean {
        if (path.isRoot) TODO("Apply all fields of value to obj")

        val mutableProperties = getProperties(path, schema, obj)
        if (mutableProperties.isEmpty()) return false
        // First run validation
        for (prop in mutableProperties) {
            if (prop.schema.validate(prop.nullable, value) != null) return false
        }
        // Then run the actual mutation
        for (prop in mutableProperties) {
            if (!prop.set(value, true)) return false
        }

        return schema.validate(false, obj) == null
    }

    companion object {
        const val OP = "add"
    }
}

package io.koil.patch

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.BooleanNode
import com.fasterxml.jackson.databind.node.NullNode

interface JsonPathExpression {
    /**
     * Evaluates the expression
     *
     * @param localValue The value represented by the @ in the expression
     */
    fun evaluate(localValue: JsonNode): JsonNode

    data class Literal(val value: JsonNode) : JsonPathExpression {
        override fun evaluate(localValue: JsonNode): JsonNode = value
    }

    data class Comparison(
        val lhs: JsonPathExpression,
        val rhs: JsonPathExpression,
        val operator: Operator,
        val negated: kotlin.Boolean,
    ) : JsonPathExpression {
        enum class Operator {
            EQUAL,
            GREATER,
            LOWER,
            GREATER_EQUAL,
            LOWER_EQUAL,
        }

        override fun evaluate(localValue: JsonNode): JsonNode {
            TODO()
//            val lhsValue = lhs.evaluate(localValue)
//            val rhsValue = rhs.evaluate(localValue)

//            val result = when (operator) {
//                Operator.EQUAL -> lhsValue == rhsValue
//                Operator.GREATER -> lhsValue > rhsValue
//                Operator.LOWER -> lhsValue < rhsValue
//                Operator.GREATER_EQUAL -> lhsValue >= rhsValue
//                Operator.LOWER_EQUAL -> lhsValue <= rhsValue
//            }

//            return if (negated) BooleanNode.valueOf(!result)
//            else BooleanNode.valueOf(result)
        }
    }

    data class Operation(
        val lhs: JsonPathExpression,
        val rhs: JsonPathExpression,
        val operator: Operator,
    ) : JsonPathExpression {
        enum class Operator {
            ADD,
            SUBTRACT,
            MULTIPLY,
            DIVIDE,
        }

        override fun evaluate(localValue: JsonNode): JsonNode {
            TODO()
//            val lhsValue = lhs.evaluate(localValue)
//            val rhsValue = rhs.evaluate(localValue)
//
//            val result = when (operator) {
//                Operator.ADD -> lhsValue + rhsValue
//                Operator.SUBTRACT -> lhsValue - rhsValue
//                Operator.MULTIPLY -> lhsValue * rhsValue
//                Operator.DIVIDE -> lhsValue / rhsValue
//            }

//            return result
        }
    }

    data class Boolean(
        val lhs: JsonPathExpression,
        val rhs: JsonPathExpression,
        val operator: Operator,
    ) : JsonPathExpression {
        enum class Operator {
            AND,
            OR,
        }

        override fun evaluate(localValue: JsonNode): JsonNode {
            val lhsValue = lhs.evaluate(localValue)
            val rhsValue = rhs.evaluate(localValue)

            val result = when (operator) {
                Operator.AND -> evalBool(lhsValue) && evalBool(rhsValue)
                Operator.OR -> evalBool(lhsValue) || evalBool(rhsValue)
            }

            return BooleanNode.valueOf(result)
        }
    }
}

/**
 * Evaluates the value to truthy/falsy (in the JS sense)
 */
private fun evalBool(value: JsonNode): Boolean {
    return when {
        value.isBoolean -> value.booleanValue()
        value.isInt -> value.intValue() != 0
        value.isLong -> value.longValue() != 0L
        value.isFloat -> value.floatValue() != 0.0f
        value.isDouble -> value.doubleValue() != 0.0
        value.isTextual -> value.textValue() != ""
        value.isArray || value.isObject -> !value.isEmpty
        value.isNull || value.isMissingNode -> false
        else -> false
    }
}

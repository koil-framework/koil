package io.koil.patch

import com.fasterxml.jackson.databind.node.NullNode
import com.fasterxml.jackson.databind.node.ObjectNode
import io.koil.schemas.JsonSchema

val removeJsonPatchchema = JsonSchema.obj("RemoveJsonPatch")
    .description("A json patch to remove a property")
    .implIdentifier(RemoveJsonPatch::class.qualifiedName!!)
    .polymorphicValue(RemoveJsonPatch.OP)
    .build()

class RemoveJsonPatch(
    path: JsonPath,
) : JsonPatch(OP, path) {
    override fun validate(schema: JsonSchema): Boolean {
        return validatePath(path, schema) != null
    }

    override fun apply(schema: JsonSchema, obj: ObjectNode): Boolean {
        if (path.isRoot) TODO("Apply all fields of value to obj")

        val mutableProperties = getProperties(path, schema, obj)
        if (mutableProperties.isEmpty()) return false
        // First run validation
        for (prop in mutableProperties) {
            if (prop.schema.validate(prop.nullable, NullNode.instance) != null) return false
        }
        // Then run the actual mutation
        for (prop in mutableProperties) {
            prop.remove()
        }

        return schema.validate(false, obj) == null
    }

    companion object {
        const val OP = "remove"
    }
}

package io.koil.patch

abstract class MutableSegment {
    internal abstract var nextSegment: JsonPathSegment?
}

class JsonPathSegmentIterator(tail: JsonPathSegment) : Iterator<JsonPathSegment> {
    private var current: JsonPathSegment? = tail

    override fun hasNext(): Boolean = current != null

    override fun next(): JsonPathSegment {
        val toReturn = current!!
        current = current!!.next
        return toReturn
    }
}

interface JsonPathSegment : Iterable<JsonPathSegment> {
    /** The relative index of this segment. If this segment is tail, index = 0, if this segment is head, index = number of segments - 1 */
    val index: Int

    /** The content of the segment, as it appeared in the original path/pointer */
    val content: String
    val previous: JsonPathSegment?
    val next: JsonPathSegment?

    override fun iterator(): Iterator<JsonPathSegment> = JsonPathSegmentIterator(this)

    class Property(
        override val index: Int,
        override val content: String,
        override val previous: JsonPathSegment?,
        override var nextSegment: JsonPathSegment?,
    ) : JsonPathSegment, MutableSegment() {
        override val next: JsonPathSegment? get() = nextSegment

        val maybeArrayIndex: Boolean = content == "-" || content.toIntOrNull() != null
        val arrayIndex: Int? get() = content.toIntOrNull()
    }

    class Filter(
        override val index: Int,
        override val content: String,
        override val previous: JsonPathSegment?,
        override var nextSegment: JsonPathSegment?,
        val expression: JsonPathExpression,
    ) : JsonPathSegment, MutableSegment() {
        override val next: JsonPathSegment? get() = nextSegment
    }
}

private val emptySegment = JsonPathSegment.Property(0, "", null, null)

class JsonPath internal constructor(
    /** The original path as passed to the parse method */
    val original: String,
    /** Indicates the source of this path was a Json Pointer */
    val pointer: Boolean,
    /** The segments, starting from the tail (i.e. the first element of the path, from left to right) */
    val tail: JsonPathSegment?,
    /** The segments, starting from the head (i.e. the last element of the path, from left to right) */
    val head: JsonPathSegment?,
) : Iterable<JsonPathSegment> {
    val targetsMultipleElements: Boolean get() = if (pointer) false else TODO()

    val isRoot = tail == null && head == null

    override fun iterator(): Iterator<JsonPathSegment> = tail!!.iterator()

    override fun toString(): String {
        if (pointer) {
            if (tail == null) return ""
            val str = StringBuilder()
            for (segment in tail) {
                str.append("/").append(segment.content)
            }
            return str.toString()
        } else {
            TODO()
        }
    }

    companion object {
        private val rootPointer = JsonPath("", true, null, null)
        private val emptyPointer = JsonPath("/", true, emptySegment, emptySegment)

        fun parse(pathOrPointer: String): JsonPath? =
            if (pathOrPointer[0] == '$') parsePath(pathOrPointer)
            else if (pathOrPointer.contains("/")) parsePointer(pathOrPointer)
            else parsePath(pathOrPointer)

        fun parsePointer(pointer: String): JsonPath? {
            // Early return for specific cases
            if (pointer.isEmpty()) return rootPointer
            else if (pointer == "/") return emptyPointer
            else if (pointer[0] != '/') return null

            // Tail is the first element of the path in this case
            var tail: JsonPathSegment? = null
            // The current element, AKA the head
            var current: JsonPathSegment? = null

            // first char is the contextual slash, skip
            var i = 1
            val end = pointer.length
            var startOffset = 0
            // The index of the current segment
            var segIndex = 0

            while (i < end) {
                val c = pointer[i]

                // common case, got a segment
                if (c == '/') {
                    val segment =
                        JsonPathSegment.Property(segIndex++, pointer.substring(startOffset + 1, i), current, null)
                    if (current != null) (current as MutableSegment).nextSegment = segment
                    if (tail == null) tail = segment
                    current = segment

                    startOffset = i++
                    continue
                }

                ++i

                // quoting is different; offline this case
                if (c == '~' && i < end) { // possibly, quote
                    // 04-Oct-2022, tatu: Let's decode escaped segment instead of recursive call
                    val sb = StringBuilder(32)
                    i = extractEscapedPointerSegment(pointer, startOffset + 1, i, sb)
                    val seg = sb.toString()

                    val segment = JsonPathSegment.Property(segIndex++, seg, current, null)
                    if (current != null) (current as MutableSegment).nextSegment = segment
                    if (tail == null) tail = segment
                    current = segment

                    if (i < 0) { // end!
                        return JsonPath(pointer, true, tail, current)
                    }

                    startOffset = i++
                    continue
                }
            }

            val segment = JsonPathSegment.Property(segIndex, pointer.substring(startOffset + 1), current, null)
            if (current != null) (current as MutableSegment).nextSegment = segment
            if (tail == null) tail = segment
            current = segment

            return JsonPath(pointer, true, tail, current)
        }

        fun parsePath(path: String): JsonPath? = JsonPathParser(path).parse()
    }
}

/**
 * Method called to extract the next segment of the path, in case
 * where we seem to have encountered a (tilde-) escaped character
 * within segment.
 *
 * @param input Full input for the tail being parsed
 * @param firstCharOffset Offset of the first character of segment (one after slash)
 * @param i Offset to character after tilde
 * @param sb StringBuilder into which unquoted segment is added
 *
 * @return Offset at which slash was encountered, if any, or -1 if expression ended without seeing unescaped slash
 */
private fun extractEscapedPointerSegment(input: String, firstCharOffset: Int, i: Int, sb: StringBuilder): Int {
    var ii = i
    val end: Int = input.length
    val toCopy: Int = i - 1 - firstCharOffset
    if (toCopy > 0) {
        sb.append(input, firstCharOffset, i - 1)
    }

    appendEscape(sb, input[ii++])
    while (ii < end) {
        val c = input[ii]

        // end is nigh!
        if (c == '/') return ii

        ++ii
        if (c == '~' && ii < end) {
            appendEscape(sb, input[ii++])
            continue
        }

        sb.append(c)
    }

    // end of the road, last segment
    return -1
}

private fun appendEscape(sb: StringBuilder, c: Char) {
    if (c != '0' && c != '1') sb.append('~')

    when (c) {
        '0' -> sb.append('~')
        '1' -> sb.append('/')
        else -> sb.append(c)
    }
}

package io.koil.core

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import io.koil.patch.JsonPath
import java.time.Duration
import java.util.UUID
import kotlin.time.toJavaDuration

class DurationSerializer : StdSerializer<Duration>(Duration::class.java) {
    override fun serialize(value: Duration?, gen: JsonGenerator, provider: SerializerProvider) {
        if(value == null) gen.writeNull()
        else gen.writeString(value.toString())
    }
}

class DurationDeserializer : StdDeserializer<Duration?>(Duration::class.java) {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): Duration? {
        var value = p.valueAsString ?: return null

        // This is a kotlin style duration, make sure we don't have a week param in (mikrotik format)
        if(!value.startsWith('P')) {
            // Convert weeks to days by parsing components of the duration, then adding the number of weeks to the days
            val matcher = Regex("(\\d+[a-z])")
                .toPattern()
                .matcher(value)

            val components = mutableMapOf<String, Int>()
            while(matcher.find()) {
                val entry = matcher.group()
                components[entry.substring(entry.length - 1)] = entry.substring(0, entry.length - 1).toInt()
            }

            if(components.containsKey("w")) {
                val weeks = components.remove("w")!!
                components.compute("d") { _, orig ->
                    orig?.plus(weeks * 7) ?: (weeks * 7)
                }
            }

            value = components.map { it.value.toString() + it.key }
                .joinToString("")
        }

        return kotlin.time.Duration.parse(value).toJavaDuration()
    }
}

class UUIDSerializer : StdSerializer<UUID>(UUID::class.java) {
    override fun serialize(value: UUID?, gen: JsonGenerator, provider: SerializerProvider) {
        if(value == null) gen.writeNull()
        else gen.writeString(value.toBase62String())
    }
}

class UUIDDeserializer : StdDeserializer<UUID?>(UUID::class.java) {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): UUID? {
        val value = p.valueAsString ?: return null

        return if (value.length == 36) UUID.fromString(value)
        else value.fromBase62ToUUID()
    }
}

class JsonPathSerializer : StdSerializer<JsonPath>(JsonPath::class.java) {
    override fun serialize(value: JsonPath?, gen: JsonGenerator, provider: SerializerProvider) {
        if(value == null) gen.writeNull()
        else gen.writeString(value.toString())
    }
}

class JsonPathDeserializer : StdDeserializer<JsonPath?>(JsonPath::class.java) {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): JsonPath? {
        val value = p.valueAsString ?: return null
        return JsonPath.parse(value)
    }
}

class KoilJacksonModule(base62Uuid: Boolean = true) : SimpleModule() {
    init {
        addDeserializer(SemanticVersion::class.java, SemanticVersionDeserializer())
        addSerializer(SemanticVersion::class.java, SemanticVersionSerializer())
        addDeserializer(Duration::class.java, DurationDeserializer())
        addSerializer(Duration::class.java, DurationSerializer())
        addDeserializer(UUID::class.java, UUIDDeserializer())
        if(base62Uuid) addSerializer(UUID::class.java, UUIDSerializer())
        addDeserializer(JsonPath::class.java, JsonPathDeserializer())
        addSerializer(JsonPath::class.java, JsonPathSerializer())
    }
}

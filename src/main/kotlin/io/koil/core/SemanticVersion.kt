package io.koil.core

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer

data class SemanticVersion(
    val major: Int,
    val minor: Int,
    val patch: Int,
) : Comparable<SemanticVersion> {
    override fun toString(): String = "$major.$minor.$patch"

    /** Returns true if the other version is before this version (inclusive) */
    fun isBefore(other: SemanticVersion): Boolean {
        if (major < other.major) return true
        else if (major > other.major) return false
        if (minor < other.minor) return true
        else if (minor > other.minor) return false
        if (patch <= other.patch) return true
        return false
    }

    /** Returns true if the other version is after this version (inclusive) */
    fun isAfter(other: SemanticVersion): Boolean {
        return !isBefore(other)
    }

    override fun compareTo(other: SemanticVersion): Int {
        if(this == other) return 0
        else if (isBefore(other)) return -1
        return 1
    }

    companion object {
        fun fromString(str: String?): SemanticVersion? {
            if (str.isNullOrBlank()) return null

            val components = str.split(".")
            if (components.size != 3) return null

            return SemanticVersion(
                components[0].toIntOrNull() ?: return null,
                components[1].toIntOrNull() ?: return null,
                components[2].toIntOrNull() ?: return null,
            )
        }
    }
}

class SemanticVersionSerializer : StdSerializer<SemanticVersion>(SemanticVersion::class.java) {
    override fun serialize(value: SemanticVersion?, gen: JsonGenerator, provider: SerializerProvider) {
        if(value == null) gen.writeNull()
        else gen.writeString(value.toString())
    }
}

class SemanticVersionDeserializer : StdDeserializer<SemanticVersion?>(SemanticVersion::class.java) {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): SemanticVersion? {
        val value = p.valueAsString ?: return null
        return SemanticVersion.fromString(value)
    }
}

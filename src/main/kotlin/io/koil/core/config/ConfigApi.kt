package io.koil.core.config

import com.fasterxml.jackson.databind.node.TextNode
import io.koil.schemas.JsonSchema

class ConfigApi(val holder: SubConfigHolder) {
    companion object {
        val schema = JsonSchema.obj("ConfigApi")
            .property("httpClient", HttpClientConfig.schema)
            .property {
                it.name("basePath")
                    .schema(JsonSchema.string())
                    .defaultValue(TextNode.valueOf("/api/v1/service-configs"))
            }.property {
                it.name("nameParamName")
                    .schema(JsonSchema.string())
                    .defaultValue(TextNode.valueOf("service"))
            }.property {
                it.name("versionParamName")
                    .schema(JsonSchema.string())
                    .defaultValue(TextNode.valueOf("version"))
            }
            .build()
    }

    val httpClient: HttpClientConfig get() = HttpClientConfig(holder.getObject("httpClient"))
    val basePath: String get() = holder.getString("basePath", "/api/v1/service-configs")
    val nameParamName: String get() = holder.getString("nameParamName", "service")
    val versionParamName: String get() = holder.getString("versionParamName", "version")
}

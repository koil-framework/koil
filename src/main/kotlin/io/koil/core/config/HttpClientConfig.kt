package io.koil.core.config

import io.koil.schemas.JsonSchema
import io.vertx.core.http.HttpClientOptions
import io.vertx.core.net.PemKeyCertOptions
import io.vertx.core.net.PemTrustOptions
import io.vertx.core.net.SSLOptions

// TODO Ability to have multiple CAs/cert pairs

class HttpClientConfig(val holder: SubConfigHolder) {
    companion object {
        val schema = JsonSchema.obj("HttpClientConfig")
            .property("host", JsonSchema.string(), true)
            .property("port", JsonSchema.int(), true)
            .property("ssl", JsonSchema.boolean(), true)
            .property("sslPrivateKeyPath", JsonSchema.string(), true)
            .property("sslCertPath", JsonSchema.string(), true)
            .property("caCertPath", JsonSchema.string(), true)
            .build()
    }

    val host: String? get() = holder.getStringNullable("host")
    val port: Int? get() = holder.getIntNullable("port")
    val ssl: Boolean? get() = holder.getBooleanNullable("ssl")
    val sslPrivateKeyPath: String? get() = holder.getStringNullable("sslPrivateKeyPath")
    val sslCertPath: String? get() = holder.getStringNullable("sslCertPath")
    val caCertPath: String? get() = holder.getStringNullable("caCertPath")

    fun createOptions(addDefaults: Boolean = false): HttpClientOptions = configure(HttpClientOptions(), addDefaults)

    fun configure(options: HttpClientOptions, addDefaults: Boolean = false): HttpClientOptions {
        val trustOptions = PemTrustOptions()
        if (caCertPath != null) trustOptions.addCertPath(caCertPath)

        val keyCertOptions = PemKeyCertOptions()
        if (sslCertPath != null && sslPrivateKeyPath != null) {
            keyCertOptions.setCertPath(sslCertPath)
                .setKeyPath(sslPrivateKeyPath)
        }

        if (addDefaults) {
            if (host != null) options.setDefaultHost(host)
            if (port != null) options.setDefaultPort(port!!)
            if (ssl != null) options.setSsl(ssl!!)
        }

        return options
            .setTrustOptions(trustOptions)
            .setKeyCertOptions(keyCertOptions)
    }

    fun sslOptions(): SSLOptions {
        val trustOptions = PemTrustOptions()
        if (caCertPath != null) trustOptions.addCertPath(caCertPath)

        val keyCertOptions = PemKeyCertOptions()
        if (sslCertPath != null && sslPrivateKeyPath != null) {
            keyCertOptions.setCertPath(sslCertPath)
                .setKeyPath(sslPrivateKeyPath)
        }

        return SSLOptions()
            .setTrustOptions(trustOptions)
            .setKeyCertOptions(keyCertOptions)
    }
}

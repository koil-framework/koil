package io.koil.core.config

import com.fasterxml.jackson.databind.node.BooleanNode
import com.fasterxml.jackson.databind.node.IntNode
import com.fasterxml.jackson.databind.node.TextNode
import io.koil.schemas.BooleanJsonSchema
import io.koil.schemas.JsonSchema
import io.vertx.core.http.ClientAuth
import io.vertx.core.http.HttpServerOptions
import io.vertx.core.net.PemKeyCertOptions
import io.vertx.core.net.PemTrustOptions

// TODO Allow ciphers to be configured
// TODO Ability to have multiple CAs/cert pairs

class HttpServerConfig(val holder: SubConfigHolder) {
    companion object {
        val schema = JsonSchema.obj("HttpServerConfig")
            .property {
                it.name("port")
                    .schema(JsonSchema.int().minimum(1).maximum(65535))
                    .defaultValue(IntNode.valueOf(8080))
            }.property("host", JsonSchema.string(), true)
            .property {
                it.name("ssl")
                    .schema(BooleanJsonSchema())
                    .defaultValue(BooleanNode.FALSE)
            }.property {
                it.name("allowTls12")
                    .schema(BooleanJsonSchema())
                    .defaultValue(BooleanNode.FALSE)
            }.property("sslPrivateKeyPath", JsonSchema.string(), true)
            .property("sslCertPath", JsonSchema.string(), true)
            .property {
                it.name("mTls")
                    .schema(JsonSchema.string().enum(setOf("disable", "request", "require")))
                    .defaultValue(TextNode.valueOf("disable"))
            }.property("caCertPath", JsonSchema.string(), true)
            .property {
                it.name("allowOrigin")
                    .schema(JsonSchema.string())
                    .defaultValue(TextNode.valueOf("*"))
            }.property {
                it.name("allowMethods")
                    .schema(JsonSchema.string())
                    .defaultValue(TextNode.valueOf("GET,POST,PUT,PATCH,DELETE"))
            }.property {
                it.name("allowHeaders")
                    .schema(JsonSchema.string())
                    .defaultValue(TextNode.valueOf("Content-Type,Authorization"))
            }
            .build()
    }

    val port: Int get() = holder.getInt("port", 8080)
    val host: String? get() = holder.getStringNullable("host")
    val ssl: Boolean get() = holder.getBoolean("ssl", false)
    val allowTls12: Boolean get() = holder.getBoolean("allowTls12", false)
    val sslPrivateKeyPath: String? get() = holder.getStringNullable("sslPrivateKeyPath")
    val sslCertPath: String? get() = holder.getStringNullable("sslCertPath")
    val mTls: String get() = holder.getString("mTls", "disable")
    val caCertPath: String? get() = holder.getStringNullable("caCertPath")
    val allowOrigin: String get() = holder.getString("allowOrigin", "*")
    val allowMethods: String get() = holder.getString("allowMethods", "GET,POST,PUT,PATCH,DELETE")
    val allowHeaders: String get() = holder.getString("allowHeaders", "Content-Type,Authorization")

    fun createOptions(): HttpServerOptions {
        val opts = HttpServerOptions()
            .setPort(port)

        if (host != null) {
            opts.setHost(host)
        }

        if (ssl && sslCertPath != null && sslPrivateKeyPath != null) {
            opts.setSsl(true)
                .setEnabledSecureTransportProtocols(setOf("TLSv1.3"))
                .addEnabledCipherSuite("TLS_AES_256_GCM_SHA384")
                .addEnabledCipherSuite("TLS_AES_128_GCM_SHA256")
                .setKeyCertOptions(
                    PemKeyCertOptions()
                        .setCertPath(sslCertPath)
                        .setKeyPath(sslPrivateKeyPath)
                )

            if (allowTls12) {
                opts.addEnabledSecureTransportProtocol("TLSv1.2")
                    .addEnabledCipherSuite("TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384")
                    .addEnabledCipherSuite("TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256")
                    .addEnabledCipherSuite("TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384")
                    .addEnabledCipherSuite("TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256")
            }
        }

        if (caCertPath != null) {
            opts.setTrustOptions(
                PemTrustOptions()
                    .addCertPath(caCertPath)
            )
        }

        if (mTls != "disable") {
            opts.setClientAuth(
                if (mTls == "require") ClientAuth.REQUIRED
                else ClientAuth.REQUEST
            )
        }

        return opts
    }
}

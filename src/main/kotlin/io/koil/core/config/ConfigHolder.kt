package io.koil.core.config

import com.fasterxml.jackson.core.JsonPointer
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.ObjectNode
import io.koil.di.DiModule
import io.koil.schemas.ObjectJsonSchema
import io.vertx.core.Handler
import io.vertx.core.Vertx
import org.slf4j.LoggerFactory

private data class UpdateHandler(
    val handler: Handler<Void>,
    val fields: Set<String>,
)

class PropertyNotFoundException(property: String) :
    Exception("Config property $property not found")

class PropertyTypeMismatchException(property: String, expected: String) :
    Exception("Config property $property must be of type $expected")

open class SubConfigHolder(internal val o: ObjectNode) {
    private val updateHandlers = mutableListOf<UpdateHandler>()
    private val subHolders = mutableMapOf<String, SubConfigHolder>()

    fun createTree(schema: ObjectJsonSchema) {
        for (prop in schema.properties) {
            if (prop.schema !is ObjectJsonSchema) continue

            val existingProperty = o[prop.name]
            val subHolder = if (existingProperty !is ObjectNode) {
                SubConfigHolder(o.putObject(prop.name))
            } else {
                SubConfigHolder(existingProperty)
            }

            subHolders[prop.name] = subHolder
            subHolder.createTree(prop.schema)
        }
    }

    /**
     * Registers a handler to be run on config updates.
     *
     * `fields` is used to specify which updated fields should trigger this handler.
     * The handler will only be called once, even if multiple fields are updated at the same time.
     * If there are no fields specified, the handler will always be called when ANY field changes.
     *
     * Handlers are called in the order they are registered.
     */
    fun onUpdate(vararg fields: String, handler: Handler<Void>) {
        updateHandlers.add(UpdateHandler(handler, fields.toSet()))
    }

    internal fun notify(updatedFields: List<UpdatedSegment>) {
        val fields = updatedFields.map(UpdatedSegment::segment).toSet()
        for (updateHandler in updateHandlers) {
            if (updateHandler.fields.isEmpty() || updateHandler.fields.any(fields::contains)) {
                updateHandler.handler.handle(null)
            }
        }

        // Notify sub-objects
        for (updatedField in updatedFields) {
            if (updatedField.subSegments.isEmpty()) continue

            val subHolder = subHolders[updatedField.segment] ?: continue
            subHolder.notify(updatedField.subSegments)
        }
    }

    private fun get(property: String, type: String, typeFn: JsonNode.() -> Boolean): JsonNode {
        val node = o[property]
            ?: throw PropertyNotFoundException(property)

        if (!node.typeFn()) throw PropertyTypeMismatchException(property, type)
        return node
    }

    private fun <T> getWithDefault(
        property: String,
        typeFn: JsonNode.() -> Boolean,
        getFn: JsonNode.() -> T,
        default: T,
    ): T {
        val node = o[property]
            ?: return default

        if (!node.typeFn()) return default
        return node.getFn()
    }

    private fun <T> getNullable(
        property: String,
        typeFn: JsonNode.() -> Boolean,
        getFn: JsonNode.() -> T,
    ): T? {
        val node = o[property]
            ?: return null

        if (!node.typeFn()) return null
        return node.getFn()
    }

    private fun <T> getWithDefault2(
        property: String,
        getFn: JsonNode.(T) -> T,
        default: T,
    ): T {
        val node = o[property]
            ?: return default

        return node.getFn(default)
    }

    @Throws(PropertyNotFoundException::class)
    fun getString(property: String): String = get(property, "string", JsonNode::isTextual).textValue()
    fun getStringNullable(property: String): String? =
        getNullable(property, JsonNode::isTextual, JsonNode::textValue)

    fun getString(property: String, default: String): String =
        getWithDefault(property, JsonNode::isTextual, JsonNode::textValue, default)

    @Throws(PropertyNotFoundException::class)
    fun getInt(property: String): Int = get(property, "int", JsonNode::canConvertToInt).intValue()
    fun getIntNullable(property: String): Int? = getNullable(property, JsonNode::canConvertToInt, JsonNode::intValue)
    fun getInt(property: String, default: Int): Int =
        getWithDefault2(property, JsonNode::asInt, default)

    @Throws(PropertyNotFoundException::class)
    fun getLong(property: String): Long = get(property, "long", JsonNode::canConvertToLong).longValue()
    fun getLong(property: String, default: Long): Long =
        getWithDefault2(property, JsonNode::asLong, default)

    @Throws(PropertyNotFoundException::class)
    fun getFloat(property: String): Float = get(property, "float", JsonNode::isFloatingPointNumber).floatValue()
    fun getFloat(property: String, default: Float): Float =
        getWithDefault(property, JsonNode::isFloatingPointNumber, JsonNode::floatValue, default)

    @Throws(PropertyNotFoundException::class)
    fun getDouble(property: String): Double = get(property, "double", JsonNode::isFloatingPointNumber).doubleValue()
    fun getDouble(property: String, default: Double): Double =
        getWithDefault2(property, JsonNode::asDouble, default)

    @Throws(PropertyNotFoundException::class)
    fun getBoolean(property: String): Boolean = get(property, "boolean", JsonNode::isBoolean).booleanValue()
    fun getBooleanNullable(property: String): Boolean? =
        getNullable(property, JsonNode::isBoolean, JsonNode::booleanValue)

    fun getBoolean(property: String, default: Boolean): Boolean =
        getWithDefault2(property, JsonNode::asBoolean, default)

    @Throws(PropertyNotFoundException::class)
    fun getArray(property: String): ArrayNode = get(property, "array", JsonNode::isArray) as ArrayNode
    fun getArray(property: String, default: ArrayNode): ArrayNode =
        getWithDefault2(property, { if (this is ArrayNode) this else it }, default)

    @Throws(PropertyNotFoundException::class)
    fun getObject(property: String): SubConfigHolder = subHolders[property] ?: throw PropertyNotFoundException(property)
}

/**
 * Warning: in order to work properly, the DI container the holder is registered on must be isolated by verticle
 * (either by copy, or by parent/child isolation). This object is NOT thread-safe !
 *
 * This also means this holder should be retrieved inside the verticle, and cannot be injected at construction.
 */
class ConfigHolder(
    private val schema: ObjectJsonSchema,
    o: ObjectNode,
) : SubConfigHolder(o) {
    companion object {
        private val logger = LoggerFactory.getLogger(ConfigHolder::class.java)
    }

    private constructor(other: ConfigHolder) : this(other.schema, other.o.deepCopy()) {
        createTree(other.schema)
    }

    fun createDiModule(eventBusRegistration: Boolean): DiModule = DiModule { container ->
        container.register({
            val copy = ConfigHolder(this)

            if (eventBusRegistration) {
                val vertx = it.get<Vertx>()
                vertx.eventBus().localConsumer(ConfigUpdates.EVENT_BUS_CHANNEL) { msg ->
                    try {
                        apply(msg.body())
                    } catch (e: Exception) {
                        logger.error("Failed to apply config updates", e)
                    }
                }
            }

            copy
        })
    }

    private fun apply(updates: ConfigUpdates) {
        // TODO Validate patches using schema

        val segData = mutableMapOf<String, UpdatedSegmentBuilder>()
        for (update in updates.updates) {
            try {
                applyUpdate(update, segData)
            } catch (e: Exception) {
                logger.error("Failed to apply update to {}", update.pointer, e)
            }
        }

        val notifData = segData.map { UpdatedSegment(it.key, it.value.build()) }
        notify(notifData)
    }

    private fun applyUpdate(update: ConfigUpdate, segData: MutableMap<String, UpdatedSegmentBuilder>) {
        // TODO Update should only be applied once for the whole service, and the updated config + changelist should be propagated

        val pointer = JsonPointer.compile(update.pointer)

        var currentSegmentData = segData
        var previousPointer = pointer
        var currentPointer = pointer.tail()
        var currentHolder: SubConfigHolder = this
        while (currentPointer != null) {
            if (currentPointer.matchingProperty.isNullOrBlank()) {
                previousPointer = currentPointer
                currentPointer = currentPointer.tail()
                continue
            }

            val prop = currentPointer.matchingProperty
            val subSeg = currentSegmentData.computeIfAbsent(prop) { UpdatedSegmentBuilder() }
            currentSegmentData = subSeg.subSegments
            currentHolder = currentHolder.getObject(prop)

            previousPointer = currentPointer
            currentPointer = currentPointer.tail()
        }

        currentHolder.o.replace(previousPointer.matchingProperty, update.newValue)
    }
}

internal data class UpdatedSegment(
    val segment: String,
    val subSegments: List<UpdatedSegment>,
)

internal class UpdatedSegmentBuilder {
    val subSegments = mutableMapOf<String, UpdatedSegmentBuilder>()

    internal fun build(): List<UpdatedSegment> = subSegments.map {
        UpdatedSegment(it.key, it.value.build())
    }
}

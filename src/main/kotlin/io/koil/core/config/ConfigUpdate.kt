package io.koil.core.config

import com.fasterxml.jackson.databind.JsonNode
import io.koil.core.LocalCodec
import io.vertx.core.eventbus.EventBus

data class ConfigUpdates(
    val updates: List<ConfigUpdate>
) {
    companion object {
        const val EVENT_BUS_CHANNEL = "koil_ConfigUpdate"

        fun registerCodecs(eventBus: EventBus) {
            eventBus.registerDefaultCodec(ConfigUpdates::class.java, LocalCodec(ConfigUpdates::class.java))
        }
    }
}

data class ConfigUpdate(
    val pointer: String,
    val newValue: JsonNode,
)

package io.koil.core.config

import com.fasterxml.jackson.databind.node.BooleanNode
import com.fasterxml.jackson.databind.node.IntNode
import com.fasterxml.jackson.databind.node.TextNode
import io.koil.schemas.BooleanJsonSchema
import io.koil.schemas.JsonSchema

class ManagementConfig(val holder: SubConfigHolder) {
    companion object {
        val schema = JsonSchema.obj("ManagementConfig")
            .property {
                it.name("basePath")
                    .schema(JsonSchema.string())
                    .defaultValue(TextNode.valueOf(""))
            }.property {
                it.name("host")
                    .schema(JsonSchema.string())
                    .defaultValue(TextNode.valueOf("::"))
            }.property {
                it.name("port")
                    .schema(JsonSchema.int().minimum(1).maximum(65535))
                    .defaultValue(IntNode.valueOf(8085))
            }.property {
                it.name("healthEndpoint")
                    .schema(JsonSchema.string())
                    .defaultValue(TextNode.valueOf("/health"))
            }.property {
                it.name("metricsEndpoint")
                    .schema(JsonSchema.string())
                    .defaultValue(TextNode.valueOf("/metrics"))
            }.property {
                it.name("enableMetricsEndpoint")
                    .schema(BooleanJsonSchema())
                    .defaultValue(BooleanNode.TRUE)
            }
            .build()
    }

    val basePath: String get() = holder.getString("basePath", "")
    val host: String get() = holder.getString("host", "::")
    val port: Int get() = holder.getInt("port", 8085)

    val healthEndpoint: String get() = holder.getString("healthEndpoint", "/health")
    val metricsEndpoint: String get() = holder.getString("metricsEndpoint", "/metrics")

    val enableMetricsEndpoint: Boolean get() = holder.getBoolean("enableMetricsEndpoint", true)
}

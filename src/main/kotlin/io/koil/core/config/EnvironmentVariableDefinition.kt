package io.koil.core.config

data class EnvironmentVariableDefinition(
    val key: String,
    val target: String,
)

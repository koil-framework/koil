package io.koil.core.config

import com.fasterxml.jackson.databind.node.BooleanNode
import com.fasterxml.jackson.databind.node.LongNode
import com.fasterxml.jackson.databind.node.TextNode
import io.koil.schemas.BooleanJsonSchema
import io.koil.schemas.JsonSchema

class ServiceConfig(val holder: SubConfigHolder) {
    companion object {
        val schema = JsonSchema.obj("ServiceConfig")
            .property {
                it.name("serviceName")
                    .schema(JsonSchema.string())
                    .defaultValue(TextNode.valueOf("unknown-service"))
            }.property {
                it.name("serviceVersion")
                    .schema(JsonSchema.string())
                    .defaultValue(TextNode.valueOf("0.0.0"))
            }.property {
                it.name("configVersion")
                    .schema(JsonSchema.int())
                    .defaultValue(LongNode.valueOf(1))
            }.property {
                it.name("useRemoteBootstrap")
                    .schema(BooleanJsonSchema())
                    .defaultValue(BooleanNode.FALSE)
            }
            .property("instanceId", JsonSchema.string())
            .property("configApi", ConfigApi.schema)
            .build()
    }

    val serviceName: String get() = holder.getString("serviceName", "unknown-service")
    val serviceVersion: String get() = holder.getString("serviceVersion", "0.0.0")
    val configVersion: Int get() = holder.getInt("configVersion", 1)
    val useRemoteBootstrap: Boolean get() = holder.getBoolean("useRemoteBootstrap", false)
    val instanceId: String get() = holder.getString("instanceId")

    val configApi: ConfigApi get() = ConfigApi(holder.getObject("configApi"))
}

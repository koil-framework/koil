package io.koil.core.config

import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import io.koil.core.BufferInputStream
import io.koil.core.merge
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.http.HttpClient
import io.vertx.core.http.HttpClientRequest
import io.vertx.core.http.HttpClientResponse
import io.vertx.core.http.RequestOptions
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.core.json.pointer.JsonPointer
import java.math.BigDecimal
import java.math.BigInteger
import java.util.*

interface ConfigDefinition {
    val optional: Boolean
    fun load(jsonMapper: JsonMapper): Future<ObjectNode>
    fun loadWrapper(jsonMapper: JsonMapper): Future<ObjectNode> {
        val future = load(jsonMapper)
        return if (optional) future.recover { Future.succeededFuture(jsonMapper.createObjectNode()) }
        else future
    }

    /** This method is called to get the string to log */
    fun createTrace(): String

    companion object {
        fun load(jsonMapper: JsonMapper, defs: List<ConfigDefinition>): Future<ObjectNode> {
            if (defs.isEmpty()) return Future.succeededFuture(jsonMapper.createObjectNode())

            return Future.join(defs.map { it.loadWrapper(jsonMapper) }).map {
                it.list<ObjectNode>()
                    .reduce { acc: ObjectNode, cur: ObjectNode -> acc.merge(cur) }
            }
        }
    }
}

data class EnvironmentVariableConfigDefinition(
    val allowedEnvVars: List<EnvironmentVariableDefinition>,
    override val optional: Boolean = true,
) : ConfigDefinition {
    override fun load(jsonMapper: JsonMapper): Future<ObjectNode> {
        return try {
            val json = jsonMapper.createObjectNode()
            val props = System.getenv()

            for (dev in allowedEnvVars) {
                val value = props[dev.key] ?: continue
                JsonPointer.from(dev.target).writeJson(json, JsonObjectHelper.convert(value), true)
            }

            Future.succeededFuture(json)
        } catch (e: Exception) {
            Future.failedFuture(e)
        }
    }

    override fun createTrace(): String =
        (if (optional) "*" else "") + allowedEnvVars.joinToString(prefix = "Env(", postfix = ")") { it.key }
}

data class SystemPropertiesConfigDefinition(override val optional: Boolean = true) : ConfigDefinition {
    override fun load(jsonMapper: JsonMapper): Future<ObjectNode> {
        return try {
            val props = System.getProperties()
            val names = props.stringPropertyNames()

            val json = jsonMapper.createObjectNode()
            for (name in names) {
                JsonPointer.create()
                    .append(name.split(".").dropLastWhile(String::isBlank))
                    .writeJson(json, JsonObjectHelper.convert(props.getProperty(name)), true)
            }

            Future.succeededFuture(json)
        } catch (e: Exception) {
            Future.failedFuture(e)
        }
    }

    override fun createTrace(): String = (if (optional) "*" else "") + "SystemProps"
}

data class FileConfigDefinition(
    private val vertx: Vertx,
    val path: String,
    override val optional: Boolean = true,
) : ConfigDefinition {
    override fun load(jsonMapper: JsonMapper): Future<ObjectNode> {
        return vertx.fileSystem()
            .readFile(path)
            .map { jsonMapper.readTree(BufferInputStream(it)) as ObjectNode }
    }

    override fun createTrace(): String = (if (optional) "*" else "") + "File($path)"
}

data class HttpConfigDefinition(
    private val httpClient: HttpClient,
    private val requestOptions: RequestOptions,
    override val optional: Boolean = true,
) : ConfigDefinition {
    override fun load(jsonMapper: JsonMapper): Future<ObjectNode> {
        return httpClient.request(requestOptions)
            .flatMap(HttpClientRequest::send)
            .flatMap(HttpClientResponse::body)
            .map { jsonMapper.readTree(BufferInputStream(it)) as ObjectNode }
    }

    override fun createTrace(): String = (if (optional) "*" else "") + "Http(${requestOptions.uri})"
}

data class JsonConfigDefinition(val json: ObjectNode) : ConfigDefinition {
    override val optional = false
    override fun load(jsonMapper: JsonMapper): Future<ObjectNode> = Future.succeededFuture(json)

    override fun createTrace(): String = (if (optional) "*" else "") + "Json"
}

/**
 * Some utility methods to create json objects from a set of String.
 *
 * @author [Clement Escoffier](http://escoffier.me)
 */
object JsonObjectHelper {
    fun convert(value: String): Any {
        Objects.requireNonNull(value)
        val v = value.trim()

        val bool = asBoolean(v)
        if (bool != null) return bool

        val number = asNumber(v)
        if (number != null) return number

        val obj = asJsonObject(v)
        if (obj != null) return obj

        val arr = asJsonArray(v)
        if (arr != null) return arr

        return value
    }

    private fun asNumber(s: String): Any? {
        try {
            return BigInteger(s).toLong()
        } catch (ignore: NumberFormatException) {
        }

        try {
            return BigDecimal(s).toDouble()
        } catch (ignore: NumberFormatException) {
        }

        return null
    }

    private fun asBoolean(s: String): Boolean? {
        return if (s.equals("true", ignoreCase = true)) {
            true
        } else if (s.equals("false", ignoreCase = true)) {
            false
        } else {
            null
        }
    }

    private fun asJsonObject(s: String): JsonObject? {
        if (s.startsWith("{") && s.endsWith("}")) {
            return try {
                JsonObject(s)
            } catch (e: Exception) {
                null
            }
        }
        return null
    }

    private fun asJsonArray(s: String): JsonArray? {
        if (s.startsWith("[") && s.endsWith("]")) {
            return try {
                JsonArray(s)
            } catch (e: Exception) {
                null
            }
        } else if (!s.startsWith("[") && !s.endsWith("]") && s.contains(",")) {
            // Allow comma-separated syntax
            return asJsonArray("[$s]")
        }

        return null
    }
}

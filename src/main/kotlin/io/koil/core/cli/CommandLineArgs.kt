package io.koil.core.cli

data class CommandLineArgs(
    val env: String?,
    val instanceId: String?,
    val configPaths: Set<String>,
) {
    companion object {
        fun parse(args: Array<String>): CommandLineArgs {
            var profile: String? = null
            val additionalConfigPaths = mutableSetOf<String>()
            var instId: String? = null

            for (arg in args) {
                if (arg.startsWith("--profile=") && profile == null) {
                    profile = arg.substring("--profile=".length)
                } else if (arg.startsWith("-p=") && profile == null) {
                    profile = arg.substring("-p=".length)
                } else if (arg.startsWith("--config-path=")) {
                    additionalConfigPaths.add(arg.substring("--config-path=".length))
                } else if (arg.startsWith("-c=")) {
                    additionalConfigPaths.add(arg.substring("-c=".length))
                } else if (arg.startsWith("--instance-id=")) {
                    instId = arg.substring("--instance-id=".length)
                } else if (arg.startsWith("-id=")) {
                    instId = arg.substring("-id=".length)
                }
            }

            return CommandLineArgs(profile, instId, additionalConfigPaths)
        }
    }
}

package io.koil.core.verticles

import com.fasterxml.jackson.databind.json.JsonMapper
import io.koil.core.HealthStatus
import io.koil.core.SERVICE_HEALTH_CHANNEL
import io.koil.core.config.ManagementConfig
import io.koil.di.DiContainer
import io.vertx.core.AbstractVerticle
import io.vertx.core.Future
import io.vertx.core.Promise
import io.vertx.core.eventbus.Message
import io.vertx.core.http.HttpServer
import io.vertx.core.http.HttpServerOptions
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.micrometer.PrometheusScrapingHandler
import org.slf4j.LoggerFactory

private data class ExtendedHealthResponse(
    val status: HealthStatus,
    val services: Map<String, HealthStatus>,
)

class ManagementVerticle(
    private val container: DiContainer,
) : AbstractVerticle() {
    companion object {
        private val logger = LoggerFactory.getLogger(ManagementVerticle::class.java)
    }

    private lateinit var mapper: JsonMapper
    private lateinit var managementConfig: ManagementConfig
    private lateinit var httpServer: HttpServer
    private val servicesStatus = mutableMapOf<String, HealthStatus>()

    override fun start(startPromise: Promise<Void>) {
        val childContainer = container.createChild()
            .registerInstance(vertx)

        mapper = childContainer.get()
        managementConfig = childContainer.get()

        createHttpServer()
            .onSuccess {
                httpServer = it
                logger.info("Management HTTP server running on {}:{}", managementConfig.host, managementConfig.port)
                startPromise.complete()
            }
            .onFailure(startPromise::fail)

        managementConfig.holder.onUpdate(
            "host",
            "port",
            "basePath",
            "healthEndpoint",
            "metricsEndpoint",
            "enableMetricsEndpoint",
        ) { reloadHttpServer() }

        vertx.eventBus().localConsumer(SERVICE_HEALTH_CHANNEL, this::onServiceHealthUpdate)
    }

    private val serverOpts
        get() = HttpServerOptions()
            .setHost(managementConfig.host)
            .setPort(managementConfig.port)

    private fun createRouter(): Router {
        val router = Router.router(vertx)
        router.get("${managementConfig.basePath}${managementConfig.healthEndpoint}")
            .handler(this::healthHandler)

        if (managementConfig.enableMetricsEndpoint) {
            router.get("${managementConfig.basePath}${managementConfig.metricsEndpoint}")
                .handler(PrometheusScrapingHandler.create())
        }

        return router
    }

    private fun createHttpServer(): Future<HttpServer> {
        return vertx.createHttpServer(serverOpts)
            .requestHandler(createRouter())
            .listen()
    }

    private fun reloadHttpServer() {
        httpServer.close()
            .compose { createHttpServer() }
            .onSuccess {
                logger.info(
                    "Reloaded management HTTP server (running on {}:{})",
                    managementConfig.host,
                    managementConfig.port
                )
            }.onFailure {
                logger.error("Failed to reload management HTTP server", it)
            }
    }

    private fun computeOverallStatus(): HealthStatus {
        if (servicesStatus.isEmpty()) return HealthStatus.OK

        val totalCount = servicesStatus.size
        var okCount = 0
        var koCount = 0
        var degradedCount = 0
        var startingCount = 0
        for (service in servicesStatus) {
            when (service.value) {
                HealthStatus.OK -> okCount++
                HealthStatus.KO -> koCount++
                HealthStatus.DEGRADED -> degradedCount++
                HealthStatus.STARTING -> startingCount++
            }
        }

        return if (totalCount == koCount) HealthStatus.KO
        else if (koCount > 0 || degradedCount > 0) HealthStatus.DEGRADED
        else if (startingCount > 0) HealthStatus.STARTING
        else HealthStatus.OK
    }

    private fun healthHandler(ctx: RoutingContext) {
        val overallStatus = computeOverallStatus()
        // TODO Make those status codes configurable
        val statusCode = when (overallStatus) {
            HealthStatus.OK, HealthStatus.STARTING -> 200
            HealthStatus.DEGRADED, HealthStatus.KO -> 503
        }

        ctx.response().setStatusCode(statusCode)

        if (ctx.queryParam("extended").isNotEmpty()) {
            ctx.response()
                .putHeader("Content-Type", "application/json")
                .send(mapper.writeValueAsString(ExtendedHealthResponse(overallStatus, servicesStatus)))
        } else {
            ctx.response()
                .putHeader("Content-Type", "text/plain")
                .send(overallStatus.name)
        }
    }

    private fun onServiceHealthUpdate(message: Message<JsonObject>) {
        var service: String? = null
        try {
            service = message.body().getString("service")
            val status = HealthStatus.valueOf(message.body().getString("status"))
            servicesStatus[service] = status
        } catch (e: Exception) {
            logger.error("Failed to set health status for service {}", service ?: "unknown-service", e)
        }
    }
}

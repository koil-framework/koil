package io.koil.core

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.pattern.ClassicConverter
import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.pattern.color.ANSIConstants
import ch.qos.logback.core.pattern.color.ForegroundCompositeConverterBase
import ch.qos.logback.core.util.OptionHelper
import io.vertx.core.impl.ContextInternal
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap


// Heavily inspired by https://github.com/reactiverse/reactiverse-contextual-logging/tree/main

object LoggingContext {
    fun set(key: String, value: String) {
        val context = ContextInternal.current() ?: return
        val localContext = context.localContextData() ?: return

        val contextData = localContext.computeIfAbsent(LoggingContext::class.java) {
            ConcurrentHashMap<String, String>()
        } as ConcurrentMap<String, String>

        contextData[key] = value
    }

    fun get(key: String): String? {
        val context = ContextInternal.current() ?: return null
        val localContext = context.localContextData() ?: return null
        val contextData = localContext[LoggingContext::class.java] as ConcurrentMap<String, String>? ?: return null

        return contextData[key]
    }
}

/**
 * Contextual data converter for Logback.
 */
class LogbackConverter : ClassicConverter() {
    private var key: String? = null
    private var defaultValue = ""

    private fun reset() {
        key = null
        defaultValue = ""
    }

    override fun start() {
        val keyInfo: Array<String?> = OptionHelper.extractDefaultReplacement(firstOption)
        key = keyInfo[0]
        if (keyInfo[1] != null) {
            defaultValue = keyInfo[1] ?: ""
        }
        super.start()
    }

    override fun convert(event: ILoggingEvent): String {
        if (key != null) {
            return LoggingContext.get(key!!) ?: defaultValue
        }
        return defaultValue
    }

    override fun stop() {
        reset()
        super.stop()
    }
}

class LevelHighlightingCompositeConverter : ForegroundCompositeConverterBase<ILoggingEvent>() {
    override fun getForegroundColorCode(event: ILoggingEvent): String {
        return when (event.level.levelInt) {
            Level.ERROR_INT -> ANSIConstants.RED_FG // same as default color scheme
            Level.WARN_INT -> ANSIConstants.YELLOW_FG
            Level.INFO_INT -> ANSIConstants.GREEN_FG
            Level.DEBUG_INT -> ANSIConstants.BLUE_FG
            else -> ANSIConstants.DEFAULT_FG
        }
    }
}

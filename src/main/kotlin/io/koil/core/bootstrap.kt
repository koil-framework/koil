package io.koil.core

import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import io.koil.core.config.*
import io.vertx.core.Future
import io.vertx.core.Vertx

private val allowedEnvVars = listOf(
    EnvironmentVariableDefinition("KOIL_SERVICE_NAME", "/serviceName"),
    EnvironmentVariableDefinition("KOIL_SERVICE_VERSION", "/serviceVersion"),
    EnvironmentVariableDefinition("KOIL_USE_REMOTE_BOOTSTRAP", "/useRemoteBoostrap"),
    EnvironmentVariableDefinition("KOIL_CONFIG_API_HOST", "/configApi/host"),
    EnvironmentVariableDefinition("KOIL_CONFIG_API_PORT", "/configApi/port"),
    EnvironmentVariableDefinition("KOIL_CONFIG_API_SSL", "/configApi/ssl"),
    EnvironmentVariableDefinition("KOIL_CONFIG_API_BASE_PATH", "/configApi/basePath"),
    EnvironmentVariableDefinition("KOIL_CONFIG_API_NAME_PARAM_NAME", "/configApi/nameParamName"),
    EnvironmentVariableDefinition("KOIL_CONFIG_API_VERSION_PARAM_NAME", "/configApi/versionParamName"),
)

fun bootstrapConfig(
    bootstrapVertx: Vertx,
    env: String,
    fullConfigPaths: Set<String>,
    jsonMapper: JsonMapper,
): Future<ObjectNode> {
    val defs = mutableListOf<ConfigDefinition>()

    for (basePath in fullConfigPaths) {
        defs.add(FileConfigDefinition(bootstrapVertx, "$basePath/bootstrap.json"))
        defs.add(FileConfigDefinition(bootstrapVertx, "$basePath/bootstrap-$env.json"))
    }

    defs.add(SystemPropertiesConfigDefinition())
    defs.add(EnvironmentVariableConfigDefinition(allowedEnvVars))

    for (basePath in fullConfigPaths) {
        defs.add(FileConfigDefinition(bootstrapVertx, "$basePath/bootstrap-overrides.json"))
        defs.add(FileConfigDefinition(bootstrapVertx, "$basePath/bootstrap-$env-overrides.json"))
    }

    // TODO Schema validation
    return ConfigDefinition.load(jsonMapper, defs)
}

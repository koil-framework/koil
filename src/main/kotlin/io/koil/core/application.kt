package io.koil.core

import com.fasterxml.jackson.databind.json.JsonMapper
import io.koil.core.cli.CommandLineArgs
import io.koil.core.config.*
import io.koil.schemas.ObjectJsonSchema
import io.micrometer.core.instrument.Tag
import io.micrometer.core.instrument.binder.jvm.ClassLoaderMetrics
import io.micrometer.core.instrument.binder.jvm.JvmGcMetrics
import io.micrometer.core.instrument.binder.jvm.JvmMemoryMetrics
import io.micrometer.core.instrument.binder.jvm.JvmThreadMetrics
import io.micrometer.core.instrument.binder.system.ProcessorMetrics
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.http.HttpMethod
import io.vertx.core.http.RequestOptions
import io.vertx.micrometer.backends.BackendRegistries
import org.slf4j.LoggerFactory
import java.util.*
import java.util.function.BiConsumer
import java.util.function.Supplier

private val GENERATED_INSTANCE_ID = UUID.randomUUID().toBase62String()
private val logger = LoggerFactory.getLogger("io.koil.Application")

fun startApplication(
    args: Array<String>,
    jsonMapper: JsonMapper,
    configSchema: ObjectJsonSchema,
    onStart: BiConsumer<String, ConfigHolder>,
    configBasePaths: Set<String> = setOf(),
    allowedEnvVars: List<EnvironmentVariableDefinition> = emptyList(),
) {
    val cliArgs = CommandLineArgs.parse(args)

    val env = cliArgs.env ?: "dev"
    val fullConfigPaths = configBasePaths
        .plus(cliArgs.configPaths)
        .map { if (it.endsWith("/")) it.dropLast(1) else it }
        .toSet()
        .ifEmpty { setOf(".") }
    val instanceId = cliArgs.instanceId ?: GENERATED_INSTANCE_ID

    logger.debug("Starting pre-bootstrap using {} config", env)

    // Create a bootstrap vertx instance to get the config
    val bootstrap = Vertx.vertx()

    val start = System.nanoTime()
    val bootstrapConfig = bootstrapConfig(bootstrap, env, fullConfigPaths, jsonMapper)
        .toCompletionStage()
        .toCompletableFuture()
        .join()
    logger.debug("Bootstrap config processed in {} µs", (System.nanoTime() - start) / 1000L)

    val configDefs = mutableListOf<ConfigDefinition>(JsonConfigDefinition(bootstrapConfig))

    // Given env=dev and the config paths are [/1, /2], the order we expect here is the following:
    // /1/application.json
    // /2/application.json
    // /1/application-dev.json
    // /2/application-dev.json
    // /1/credentials.json
    // /2/credentials.json
    // /1/credentials-dev.json
    // /2/credentials-dev.json
    val fileNames = listOf(
        "application.json",
        "application-$env.json",
        "credentials.json",
        "credentials-$env.json",
    )
    for (fileName in fileNames) {
        for (basePath in fullConfigPaths) {
            configDefs.add(FileConfigDefinition(bootstrap, "$basePath/$fileName"))
        }
    }

    for (basePath in fullConfigPaths) {
        configDefs.add(FileConfigDefinition(bootstrap, "$basePath/application.json"))
        configDefs.add(FileConfigDefinition(bootstrap, "$basePath/application-$env.json"))
        configDefs.add(FileConfigDefinition(bootstrap, "$basePath/credentials.json"))
        configDefs.add(FileConfigDefinition(bootstrap, "$basePath/credentials-$env.json"))
    }

    if (allowedEnvVars.isNotEmpty()) configDefs.add(EnvironmentVariableConfigDefinition(allowedEnvVars))

    val bootstrapConfigObj = ConfigHolder(ServiceConfig.schema, bootstrapConfig)
    bootstrapConfigObj.createTree(ServiceConfig.schema)
    val serviceConfig = ServiceConfig(bootstrapConfigObj)

    if (serviceConfig.useRemoteBootstrap) {
        logger.info("Processing bootstrap using remote config API in {}", env)

        val configApi = serviceConfig.configApi
        val clientConfig = configApi.httpClient

        val client = bootstrap.createHttpClient(clientConfig.createOptions())
        val request = RequestOptions()
            .setMethod(HttpMethod.GET)
            .putHeader("Accept", "application/json")
            .setSsl(clientConfig.ssl)
            .setHost(clientConfig.host)
            .setPort(clientConfig.port)
            .setURI(
                "${configApi.basePath}?${configApi.nameParamName}=${
                    serviceConfig.serviceName
                }&${configApi.versionParamName}=${
                    serviceConfig.serviceVersion
                }&instanceId=$instanceId"
            )
        configDefs.add(HttpConfigDefinition(client, request, false))
    } else {
        logger.info("Processing local bootstrap in {}", env)
    }

    configDefs.add(JsonConfigDefinition(jsonMapper.createObjectNode().put("instanceId", instanceId)))

    if (logger.isDebugEnabled) {
        logger.debug("Config definitions used to resolve the config")
        for (configDef in configDefs) logger.debug("- {}", configDef.createTrace())
    }

    ConfigDefinition.load(jsonMapper, configDefs)
        .compose { bootstrap.close().map(it) }
        .onFailure { logger.error("Failed to get config", it) }
        .onSuccess {
            logger.info(
                "Starting {}:{}[{}]",
                it.get("serviceName").textValue() ?: "unknown",
                it.get("serviceVersion").textValue() ?: "unknown",
                env,
            )
            onStart.accept(env, ConfigHolder(configSchema, it))
        }
        .onFailure { logger.error("Failed to start main vertx instance", it) }
}

fun collectDefaultMetrics(env: String, config: SubConfigHolder) {
    // Add metrics to be collected
    // TODO Ability to reload common tags
    val registry = BackendRegistries.getDefaultNow()
    registry.config().commonTags(
        listOf(
            Tag.of("env", env),
            Tag.of("service", config.getString("serviceName")),
            Tag.of("instance", config.getString("instanceId"))
        )
    )
    ClassLoaderMetrics().bindTo(registry)
    JvmMemoryMetrics().bindTo(registry)
    JvmGcMetrics().bindTo(registry)
    ProcessorMetrics().bindTo(registry)
    JvmThreadMetrics().bindTo(registry)
}

fun handleVerticleStart(future: Future<String>, verticleName: String): Future<String> {
    return future.onSuccess { event: String ->
        logger.info("Deployed {} verticle {}", verticleName, event)
    }.onFailure { t: Throwable ->
        logger.error("Error while deploying {} verticle", verticleName, t)
    }
}

private fun waitForFuture(f: Future<*>, errorMessage: String) {
    try {
        f.map(Unit)
            .recover { Future.succeededFuture(Unit) }
            .toCompletionStage()
            .toCompletableFuture()
            .join()
    } catch (e: Exception) {
        logger.error(errorMessage, e)
    }
}

fun shutdownApplication(vertx: Vertx, dependencies: List<Supplier<Future<Void>>>): Thread {
    return Thread({
        val undeployFutures: MutableList<Future<*>> = ArrayList()
        for (dId in vertx.deploymentIDs()) {
            logger.info("Undeploying verticle {}", dId)
            undeployFutures.add(vertx.undeploy(dId))
        }

        waitForFuture(
            Future.join(undeployFutures)
                .onFailure { logger.error("Failed to undeploy verticles", it) },
            "Failed to undeploy verticles"
        )

        logger.info("Shutting down dependencies")
        waitForFuture(
            Future.join(dependencies.map { it.get() })
                .onFailure { logger.error("Failed to shutdown dependencies", it) },
            "Failed to shutdown dependencies"
        )

        logger.info("Shutting down vertx instance")
        waitForFuture(
            vertx.close().onFailure { logger.error("Failed to close vertx", it) },
            "Failed to close vertx"
        )
    }, "vertx-shutdown-hook")
}

fun registerShutdownHook(vertx: Vertx, dependencies: List<Supplier<Future<Void>>>) {
    Runtime.getRuntime().addShutdownHook(shutdownApplication(vertx, dependencies))
}

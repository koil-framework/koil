package io.koil.core

import com.fasterxml.jackson.databind.json.JsonMapper
import io.vertx.core.buffer.Buffer
import java.io.OutputStream

class BufferOutputStream(
    private val buffer: Buffer,
) : OutputStream() {
    override fun write(b: Int) {
        buffer.appendByte(b.toByte())
    }

    override fun write(b: ByteArray) {
        buffer.appendBytes(b)
    }
}

fun JsonMapper.writeValueAsBuffer(value: Any?): Buffer {
    val buffer = Buffer.buffer()
    writeValue(BufferOutputStream(buffer), value)
    return buffer
}

package io.koil.core

import java.util.function.Function
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

sealed interface Failable<V, E> {
    fun <T> map(fn: Function<V, T>): Failable<T, E>

    @JvmInline
    value class Failed<V, E>(val error: E) : Failable<V, E> {
        fun <NV> convert(): Failable<NV, E> {
            return this as Failable<NV, E>
        }

        override fun <T> map(fn: Function<V, T>): Failable<T, E> {
            return convert()
        }
    }

    @JvmInline
    value class Success<V, E>(val value: V) : Failable<V, E> {
        override fun <T> map(fn: Function<V, T>): Failable<T, E> {
            return Success(fn.apply(value))
        }
    }

    companion object {
        @OptIn(ExperimentalContracts::class)
        fun <V, E> isFailed(failable: Failable<V, E>): Boolean {
            contract {
                returns(true) implies (failable is Failed<V, E>)
                returns(false) implies (failable is Success<V, E>)
            }
            return failable is Failed
        }

        @OptIn(ExperimentalContracts::class)
        fun <V, E> isSuccess(failable: Failable<V, E>): Boolean {
            contract {
                returns(true) implies (failable is Success<V, E>)
                returns(false) implies (failable is Failed<V, E>)
            }
            return failable is Success
        }
    }
}

typealias EFailable<T> = Failable<T, Throwable>

fun <T> EFailable<T>.throwIfFailed(): T {
    if (Failable.isFailed(this)) throw this.error
    else return this.value
}

fun <T, E> T.wrap(): Failable<T, E> = Failable.Success(this)

package io.koil.core

import java.io.ByteArrayOutputStream
import java.nio.ByteBuffer
import java.util.*
import java.util.HexFormat
import kotlin.math.ceil
import kotlin.math.ln


private const val STANDARD_BASE = 256
private const val TARGET_BASE = 62

private val ALPHABET = byteArrayOf(
    '0'.code.toByte(),
    '1'.code.toByte(),
    '2'.code.toByte(),
    '3'.code.toByte(),
    '4'.code.toByte(),
    '5'.code.toByte(),
    '6'.code.toByte(),
    '7'.code.toByte(),
    '8'.code.toByte(),
    '9'.code.toByte(),
    'a'.code.toByte(),
    'b'.code.toByte(),
    'c'.code.toByte(),
    'd'.code.toByte(),
    'e'.code.toByte(),
    'f'.code.toByte(),
    'g'.code.toByte(),
    'h'.code.toByte(),
    'i'.code.toByte(),
    'j'.code.toByte(),
    'k'.code.toByte(),
    'l'.code.toByte(),
    'm'.code.toByte(),
    'n'.code.toByte(),
    'o'.code.toByte(),
    'p'.code.toByte(),
    'q'.code.toByte(),
    'r'.code.toByte(),
    's'.code.toByte(),
    't'.code.toByte(),
    'u'.code.toByte(),
    'v'.code.toByte(),
    'w'.code.toByte(),
    'x'.code.toByte(),
    'y'.code.toByte(),
    'z'.code.toByte(),
    'A'.code.toByte(),
    'B'.code.toByte(),
    'C'.code.toByte(),
    'D'.code.toByte(),
    'E'.code.toByte(),
    'F'.code.toByte(),
    'G'.code.toByte(),
    'H'.code.toByte(),
    'I'.code.toByte(),
    'J'.code.toByte(),
    'K'.code.toByte(),
    'L'.code.toByte(),
    'M'.code.toByte(),
    'N'.code.toByte(),
    'O'.code.toByte(),
    'P'.code.toByte(),
    'Q'.code.toByte(),
    'R'.code.toByte(),
    'S'.code.toByte(),
    'T'.code.toByte(),
    'U'.code.toByte(),
    'V'.code.toByte(),
    'W'.code.toByte(),
    'X'.code.toByte(),
    'Y'.code.toByte(),
    'Z'.code.toByte(),
)

/**
 * Creates the lookup table from character to index of character in character set.
 */
private fun createLookupTable(alphabet: ByteArray): ByteArray {
    val lookup = ByteArray(256)
    for (i in alphabet.indices) {
        lookup[alphabet[i].toInt()] = (i and 0xFF).toByte()
    }
    return lookup
}

object Base62 {
    private val lookup: ByteArray = createLookupTable(ALPHABET)

    /**
     * Encodes a sequence of bytes in Base62 encoding.
     *
     * @param message a byte sequence.
     * @return a sequence of Base62-encoded bytes.
     */
    fun encode(message: ByteArray): ByteArray {
        val indices = convert(message, STANDARD_BASE, TARGET_BASE)
        return translate(indices, ALPHABET)
    }

    /**
     * Decodes a sequence of Base62-encoded bytes.
     *
     * @param encoded a sequence of Base62-encoded bytes.
     * @return a byte sequence.
     * @throws IllegalArgumentException when `encoded` is not encoded over the Base62 alphabet.
     */
    fun decode(encoded: ByteArray): ByteArray {
        require(isBase62Encoding(encoded)) { "Input is not encoded correctly" }
        val prepared = translate(encoded, lookup)
        return convert(prepared, TARGET_BASE, STANDARD_BASE)
    }

    /**
     * Uses the elements of a byte array as indices to a dictionary and returns the corresponding values
     * in form of a byte array.
     */
    private fun translate(indices: ByteArray, dictionary: ByteArray): ByteArray {
        val translation = ByteArray(indices.size)
        for (i in indices.indices) {
            translation[i] = dictionary[indices[i].toInt()]
        }
        return translation
    }

    /**
     * Converts a byte array from a source base to a target base using the alphabet.
     */
    private fun convert(message: ByteArray, sourceBase: Int, targetBase: Int): ByteArray {
        /**
         * This algorithm is inspired by: http://codegolf.stackexchange.com/a/21672
         */
        val estimatedLength = estimateOutputLength(message.size, sourceBase, targetBase)
        val out = ByteArrayOutputStream(estimatedLength)
        var source = message
        while (source.size > 0) {
            val quotient = ByteArrayOutputStream(source.size)
            var remainder = 0
            for (i in source.indices) {
                val accumulator = (source[i].toInt() and 0xFF) + remainder * sourceBase
                val digit = (accumulator - accumulator % targetBase) / targetBase
                remainder = accumulator % targetBase
                if (quotient.size() > 0 || digit > 0) {
                    quotient.write(digit)
                }
            }
            out.write(remainder)
            source = quotient.toByteArray()
        }

        // pad output with zeroes corresponding to the number of leading zeroes in the message
        var i = 0
        while (i < message.size - 1 && message[i].toInt() == 0) {
            out.write(0)
            i++
        }
        return reverse(out.toByteArray())
    }

    /**
     * Checks whether a sequence of bytes is encoded over a Base62 alphabet.
     *
     * @param bytes a sequence of bytes.
     * @return `true` when the bytes are encoded over a Base62 alphabet, `false` otherwise.
     */
    fun isBase62Encoding(bytes: ByteArray?): Boolean {
        if (bytes == null) {
            return false
        }
        for (e in bytes) {
            if ('0'.code.toByte() > e || '9'.code.toByte() < e) {
                if ('a'.code.toByte() > e || 'z'.code.toByte() < e) {
                    if ('A'.code.toByte() > e || 'Z'.code.toByte() < e) {
                        return false
                    }
                }
            }
        }
        return true
    }

    /**
     * Estimates the length of the output in bytes.
     */
    private fun estimateOutputLength(inputLength: Int, sourceBase: Int, targetBase: Int): Int {
        return ceil(ln(sourceBase.toDouble()) / ln(targetBase.toDouble()) * inputLength)
            .toInt()
    }

    /**
     * Reverses a byte array.
     */
    private fun reverse(arr: ByteArray): ByteArray {
        val length = arr.size
        val reversed = ByteArray(length)
        for (i in 0 until length) {
            reversed[length - i - 1] = arr[i]
        }
        return reversed
    }
}

fun UUID.toBase62(): ByteArray {
    val byteArray = ByteBuffer.allocate(16)
        .putLong(this.mostSignificantBits)
        .putLong(this.leastSignificantBits)
        .array()
    return Base62.encode(byteArray)
}

fun UUID.toBase64(): ByteArray {
    val byteArray = ByteBuffer.allocate(16)
        .putLong(this.mostSignificantBits)
        .putLong(this.leastSignificantBits)
        .array()
    return Base64.getUrlEncoder().withoutPadding().encode(byteArray)
}

fun ByteArray.fromBase62ToUUID(): UUID {
    val dec = try {
        Base62.decode(this)
    } catch (e: IllegalArgumentException) {
        throw IllegalArgumentException("Invalid Base62 sequence", e)
    }

    if (dec.size != 16) {
        throw IllegalArgumentException("UUIDs can only be created from 128 bits")
    }
    val buf = ByteBuffer.allocate(16).put(dec)
    return UUID(buf.getLong(0), buf.getLong(8))
}

fun ByteArray.fromBase64ToUUID(): UUID {
    val dec = try {
        Base64.getDecoder().decode(this)
    } catch (e: IllegalArgumentException) {
        throw IllegalArgumentException("Invalid Base64 sequence", e)
    }

    if (dec.size != 16) {
        throw IllegalArgumentException("UUIDs can only be created from 128 bits")
    }
    val buf = ByteBuffer.allocate(16).put(dec)
    return UUID(buf.getLong(0), buf.getLong(8))
}

fun ByteArray.fromBase64ToBase62(): ByteArray {
    try {
        val dec = Base64.getDecoder().decode(this)
        return Base62.encode(dec)
    } catch (e: IllegalArgumentException) {
        throw IllegalArgumentException("Invalid Base64 sequence", e)
    }
}

fun ByteArray.fromBase62ToBase64(): ByteArray {
    try {
        val dec = Base62.decode(this)
        return Base64.getUrlEncoder().withoutPadding().encode(dec)
    } catch (e: IllegalArgumentException) {
        throw IllegalArgumentException("Invalid Base62 sequence", e)
    }
}

fun ByteArray.fromBase64(): ByteArray {
    try {
        return Base64.getDecoder().decode(this)
    } catch (e: IllegalArgumentException) {
        throw IllegalArgumentException("Invalid Base64 sequence", e)
    }
}

fun ByteArray.fromBase62(): ByteArray {
    try {
        return Base62.decode(this)
    } catch (e: IllegalArgumentException) {
        throw IllegalArgumentException("Invalid Base62 sequence", e)
    }
}

fun ByteArray.toBase62(): ByteArray {
    return Base62.encode(this)
}

fun ByteArray.toBase64(): ByteArray {
    return Base64.getEncoder().encode(this)
}

fun ByteArray.toBase62String(): String {
    return String(toBase62())
}

fun ByteArray.toBase64String(): String {
    return String(toBase64())
}

fun UUID.toBase64String() = String(this.toBase64())
fun UUID.toBase62String() = String(this.toBase62())

fun String.fromBase62ToBase64(): String = String(toByteArray().fromBase62ToBase64())
fun String.fromBase64ToBase62(): String = String(toByteArray().fromBase64ToBase62())

fun String.fromBase62ToUUID(): UUID = toByteArray().fromBase62ToUUID()
fun String.fromBase64ToUUID(): UUID = toByteArray().fromBase64ToUUID()

fun String.fromBase62(): ByteArray = toByteArray().fromBase62()
fun String.fromBase64(): ByteArray = toByteArray().fromBase64()

private val hexFormat = HexFormat.of()

fun String.fromHex(): ByteArray = hexFormat.parseHex(this)
fun String.fromHexToBase62(): ByteArray = fromHex().toBase62()
fun String.fromHexToBase64(): ByteArray = fromHex().toBase64()
fun String.fromHexToBase62String(): String = fromHex().toBase62String()
fun String.fromHexToBase64String(): String = fromHex().toBase64String()
fun String.fromBase62ToHex(): String = hexFormat.formatHex(fromBase62())
fun String.fromBase64ToHex(): String = hexFormat.formatHex(fromBase64())

package io.koil.core

import io.vertx.core.buffer.Buffer
import io.vertx.core.eventbus.MessageCodec

class LocalCodec<T>(private val cls: Class<T>) : MessageCodec<T, T> {
    override fun name(): String = "Local${cls.simpleName}Codec"
    override fun systemCodecID(): Byte = -1

    override fun transform(s: T): T = s

    override fun decodeFromWire(pos: Int, buffer: Buffer?): T = throw NotImplementedError()
    override fun encodeToWire(buffer: Buffer?, s: T?) = throw NotImplementedError()
}

package io.koil.core

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode

fun <T> JsonNode.ifObject(fn: (ObjectNode) -> T): T? = if(isObject) fn(this as ObjectNode) else null

fun ObjectNode.merge(other: ObjectNode): ObjectNode {
    val fieldNames = other.fieldNames()
    while (fieldNames.hasNext()) {
        val fieldName = fieldNames.next()
        val jsonNode = this[fieldName]
        val value = other[fieldName]

        // if field exists and is an embedded object
        if (jsonNode != null && jsonNode is ObjectNode && value is ObjectNode) {
            jsonNode.merge(value)
        } else {
            replace(fieldName, value)
        }
    }

    return this
}

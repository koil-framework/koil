package io.koil.core

import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject

enum class HealthStatus {
    STARTING,
    OK,
    DEGRADED,
    KO
}

const val SERVICE_HEALTH_CHANNEL = "service-health"

class ServiceHealth(vertx: Vertx) {
    private val eventBus = vertx.eventBus()

    fun setServiceHealth(service: String, status: HealthStatus) {
        eventBus.send(SERVICE_HEALTH_CHANNEL, JsonObject.of("service", service, "status", status))
    }
}

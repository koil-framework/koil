@file:OptIn(DelicateCoroutinesApi::class)

package io.koil.core

import io.vertx.core.Future
import io.vertx.core.Promise
import io.vertx.core.Vertx
import io.vertx.kotlin.coroutines.coAwait
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

fun Vertx.runSuspendOnContext(action: suspend () -> Unit) {
    GlobalScope.launch(dispatcher()) {
        action()
    }
}

fun <T> Vertx.runSuspendOnContext(action: suspend () -> T): Future<T> {
    val promise = Promise.promise<T>()
    GlobalScope.launch(dispatcher()) {
        try {
            promise.complete(action())
        } catch (t: Throwable) {
            promise.fail(t)
        }
    }
    return promise.future()
}

fun <T> Vertx.runSuspendOnContext(promise: Promise<T>, action: suspend () -> T) {
    GlobalScope.launch(dispatcher()) {
        try {
            promise.complete(action())
        } catch (t: Throwable) {
            promise.fail(t)
        }
    }
}

data class Quad<W, X, Y, Z>(val first: W, val second: X, val third: Y, val forth: Z)

suspend fun <X, Y> loadAll(future1: Future<X>, future2: Future<Y>): Pair<X, Y> {
    val join = Future.join(future1, future2).coAwait()
    return join.resultAt(0) as X to join.resultAt(1) as Y
}

suspend fun <X, Y, Z> loadAll(future1: Future<X>, future2: Future<Y>, future3: Future<Z>): Triple<X, Y, Z> {
    val join = Future.join(future1, future2, future3).coAwait()
    return Triple(join.resultAt(0) as X, join.resultAt(1) as Y, join.resultAt(2) as Z)
}

suspend fun <W, X, Y, Z> loadAll(
    future1: Future<W>,
    future2: Future<X>,
    future3: Future<Y>,
    future4: Future<Z>
): Quad<W, X, Y, Z> {
    val join = Future.join(future1, future2, future3, future4).coAwait()
    return Quad(
        join.resultAt(0) as W,
        join.resultAt(1) as X,
        join.resultAt(2) as Y,
        join.resultAt(3) as Z,
    )
}

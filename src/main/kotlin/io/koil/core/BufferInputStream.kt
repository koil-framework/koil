package io.koil.core

import com.fasterxml.jackson.databind.json.JsonMapper
import io.vertx.core.buffer.Buffer
import java.io.IOException
import java.io.InputStream
import kotlin.math.min

class BufferInputStream(
    private val buffer: Buffer,
    private val startIndex: Int = 0,
    length: Int = buffer.length() - startIndex,
) : InputStream() {
    private var closed = false

    private val endIndex = startIndex + length
    private var currentIndex = startIndex
    private var mark = currentIndex

    /**
     * Returns the number of read bytes by this stream so far.
     */
    fun readBytes(): Int {
        return currentIndex - startIndex
    }

    @Throws(IOException::class)
    override fun close() {
        try {
            super.close()
        } finally {
            // The Closable interface says "If the stream is already closed then invoking this method has no effect."
            if (!closed) closed = true
        }
    }

    @Throws(IOException::class)
    override fun available(): Int {
        return endIndex - currentIndex
    }

    override fun mark(readlimit: Int) {
        mark = currentIndex
    }

    override fun markSupported(): Boolean {
        return true
    }

    @Throws(IOException::class)
    override fun read(): Int {
        if (available() == 0) return -1
        return buffer.getByte(currentIndex++).toInt() and 0xff
    }

    @Throws(IOException::class)
    override fun read(b: ByteArray, off: Int, len: Int): Int {
        val available = available()
        if (available == 0) return -1
        else if(len == 0) return 0

        val actualLen = min(available, len)
        buffer.getBytes(currentIndex, currentIndex + actualLen, b, off)
        currentIndex += len
        return actualLen
    }

    @Throws(IOException::class)
    override fun reset() {
        currentIndex = mark
    }

    @Throws(IOException::class)
    override fun skip(n: Long): Long {
        return if (n > Int.MAX_VALUE) {
            skipBytes(Int.MAX_VALUE).toLong()
        } else {
            skipBytes(n.toInt()).toLong()
        }
    }

    @Throws(IOException::class)
    fun skipBytes(n: Int): Int {
        val nBytes = min(available(), n)
        currentIndex += nBytes
        return nBytes
    }
}

fun <T> JsonMapper.readValue(buffer: Buffer, cls: Class<T>): T {
    return readValue(BufferInputStream(buffer), cls)
}

package io.koil.schemas

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode
import io.koil.patch.JsonPath
import io.koil.ser.MessagePackExt
import io.vertx.core.buffer.Buffer
import java.time.Instant
import java.time.OffsetDateTime
import java.util.*

interface JsonSchemaBuilder {
    fun build(): JsonSchema
}

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.EXISTING_PROPERTY,
    property = "type",
    visible = false
)
@JsonSubTypes(
    JsonSubTypes.Type(StringJsonSchema::class, name = StringJsonSchema.TYPE),
    JsonSubTypes.Type(IntegerJsonSchema::class, name = IntegerJsonSchema.TYPE),
    JsonSubTypes.Type(FloatJsonSchema::class, name = FloatJsonSchema.TYPE),
    JsonSubTypes.Type(BooleanJsonSchema::class, name = BooleanJsonSchema.TYPE),
    JsonSubTypes.Type(ArrayJsonSchema::class, name = ArrayJsonSchema.TYPE),
    JsonSubTypes.Type(ObjectJsonSchema::class, name = ObjectJsonSchema.TYPE),
    JsonSubTypes.Type(RefJsonSchema::class, name = RefJsonSchema.TYPE),
    JsonSubTypes.Type(AnyJsonSchema::class, name = AnyJsonSchema.TYPE),
)
sealed class JsonSchema {
    companion object {
        fun deserializeMessagePack(ext: MessagePackExt): JsonSchema =
            deserializeMessagePack(ext.type, ext.content)

        fun deserializeMessagePack(type: Byte, buffer: Buffer): JsonSchema {
            return when (type) {
                StringJsonSchema.MSGPACK_TYPE -> StringJsonSchema.deserializeMessagePack(buffer)
                IntegerJsonSchema.MSGPACK_TYPE -> IntegerJsonSchema.deserializeMessagePack(buffer)
                FloatJsonSchema.MSGPACK_TYPE -> FloatJsonSchema.deserializeMessagePack(buffer)
                BooleanJsonSchema.MSGPACK_TYPE -> BooleanJsonSchema.deserializeMessagePack(buffer)
                ArrayJsonSchema.MSGPACK_TYPE -> ArrayJsonSchema.deserializeMessagePack(buffer)
                ObjectJsonSchema.MSGPACK_TYPE -> ObjectJsonSchema.deserializeMessagePack(buffer)
                RefJsonSchema.MSGPACK_TYPE -> RefJsonSchema.deserializeMessagePack(buffer)
                AnyJsonSchema.MSGPACK_TYPE -> AnyJsonSchema.deserializeMessagePack(buffer)
                else -> throw RuntimeException("Unsupported type: $type")
            }
        }

        fun string(): StringJsonSchema.Builder = StringJsonSchema.Builder()
        fun boolean(description: String? = null): BooleanJsonSchema = BooleanJsonSchema(description)
        fun int(): IntegerJsonSchema.Builder = IntegerJsonSchema.Builder().int32()
        fun long(): IntegerJsonSchema.Builder = IntegerJsonSchema.Builder().int64()
        fun float(): FloatJsonSchema.Builder = FloatJsonSchema.Builder().float()
        fun double(): FloatJsonSchema.Builder = FloatJsonSchema.Builder().double()
        fun array(): ArrayJsonSchema.Builder = ArrayJsonSchema.Builder()
        fun array(items: JsonSchema): ArrayJsonSchema.Builder = ArrayJsonSchema.Builder().items(items)
        fun array(items: JsonSchemaBuilder): ArrayJsonSchema.Builder = ArrayJsonSchema.Builder().items(items)
        fun obj(): ObjectJsonSchema.Builder = ObjectJsonSchema.Builder()
        fun obj(sourceName: String): ObjectJsonSchema.Builder = ObjectJsonSchema.Builder().sourceName(sourceName)
        fun ref(id: String): RefJsonSchema = RefJsonSchema(null, id)
        fun ref(description: String, id: String): RefJsonSchema = RefJsonSchema(description, id)
        fun anyJson(description: String? = null): AnyJsonSchema = AnyJsonSchema(description)
        fun rawObject(): ObjectJsonSchema.Builder = ObjectJsonSchema.Builder().sourceName("Raw").raw()

        fun uuid(): StringJsonSchema.Builder = StringJsonSchema.Builder()
            .implIdentifier(UUID::class.qualifiedName!!)
            .format("uuid")
            .minLength(16)
            .maxLength(36)

        fun jsonPath(): StringJsonSchema.Builder = StringJsonSchema.Builder()
            .format("json-path")
            .implIdentifier(JsonPath::class.qualifiedName!!)

        fun instantDateTime(): StringJsonSchema.Builder = StringJsonSchema.Builder()
            .format("date-time")
            .implIdentifier(Instant::class.qualifiedName!!)

        fun offsetDateTime(): StringJsonSchema.Builder = StringJsonSchema.Builder()
            .format("date-time")
            .implIdentifier(OffsetDateTime::class.qualifiedName!!)

        fun map(keys: JsonSchema, values: JsonSchema): ObjectJsonSchema.Builder = ObjectJsonSchema.Builder()
            .sourceName("Map")
            .additionalProperties(keys, values)

        val schema = obj("JsonSchema")
            .implIdentifier("io.koil.schemas.JsonSchema")
            .property {
                it.name("type")
                    .polymorphicKey()
                    .schema(
                        string()
                            .description("The type of the schema")
                            .enum(setOf("STRING", "INTEGER", "FLOAT", "BOOLEAN", "ARRAY", "OBJECT", "REF", "ANY")),
                    )
            }.property(
                "description",
                string().description("The description of the schema"),
                true,
            )
            .subType(stringJsonSchema)
            .subType(integerJsonSchema)
            .subType(floatJsonSchema)
            .subType(booleanJsonSchema)
            .subType(arrayJsonSchema)
            .subType(objectJsonSchema)
            .subType(refJsonSchema)
            .subType(anyJsonSchema)
            .build()
    }

    abstract val type: String
    abstract val description: String?

    abstract fun validateRaw(nullable: Boolean, value: String?): ValidationConstraintError?

    // TODO Object should return the property name that failed, which is not permitted by this data structure
    abstract fun validate(nullable: Boolean, value: JsonNode?): ValidationConstraintError?

    abstract fun toJsonSchemaDraft7Element(): ObjectNode
    abstract fun serializeMessagePack(): Buffer
}

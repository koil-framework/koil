package io.koil.schemas

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.databind.node.TextNode
import io.koil.ser.MessagePackDecoder
import io.vertx.core.buffer.Buffer
import kotlin.enums.EnumEntries

val stringJsonSchema = ObjectJsonSchema.Builder()
    .sourceName("StringJsonSchema")
    .description("A json schema representing a string")
    .polymorphicValue(StringJsonSchema.TYPE)
    .property(
        "format",
        StringJsonSchema.Builder().description("An optional pre-defined validation format").minLength(1),
        true
    )
    .property("const", StringJsonSchema.Builder().description("An optional constant value"), true)
    .property(
        "enumSourceName",
        StringJsonSchema.Builder().description("The name of the enum in the source code").minLength(1),
        true
    )
    .property(
        "enumValues",
        ArrayJsonSchema.Builder()
            .items(StringJsonSchema.Builder().description("A possible value"))
            .description("The list of acceptable values")
            .minItems(1)
            .uniqueItems(true),
        true,
    )
    .property(
        "pattern",
        StringJsonSchema.Builder().description("An optional regex pattern to validate the value").minLength(1),
        true
    )
    .property(
        "minLength",
        IntegerJsonSchema.Builder().int32().description("An optional minimum length for the value").minimum(0),
        true
    )
    .property(
        "maxLength",
        IntegerJsonSchema.Builder().int32().description("An optional maximum length for the value").minimum(0),
        true
    )
    .property(
        "implIdentifier",
        StringJsonSchema.Builder()
            .description("An identifier pointing to an existing implementation of this object")
            .minLength(1),
        true,
    ).build()

data class StringJsonSchema(
    override val description: String?,
    /** An optional pre-defined validation format */
    val format: String?,
    /** An optional constant value */
    val const: String?,
    /** The name of the enum in the source code */
    val enumSourceName: String?,
    /** The list of acceptable values */
    val enumValues: Set<String>?,
    /** An optional regex pattern to validate the value */
    val pattern: String?,
    /** An optional minimum length for the value */
    val minLength: Int?,
    /** An optional maximum length for the value */
    val maxLength: Int?,
    /** An identifier pointing to an existing implementation of this object */
    val implIdentifier: String?,
) : JsonSchema() {
    companion object {
        const val MSGPACK_TYPE: Byte = 10
        const val TYPE = "STRING"

        fun deserializeMessagePack(buffer: Buffer): StringJsonSchema {
            val builder = Builder()

            // Means there are no optional properties
            if (buffer.length() == 1) builder.build()

            val decoder = MessagePackDecoder(buffer)
            val count = decoder.mapSize
            var enumSourceName: String? = null
            var enumValues: MutableSet<String>? = null

            for (i in 0 until count) {
                val type = decoder.int
                when (type) {
                    0 -> builder.description(decoder.string)
                    1 -> builder.format(decoder.string)
                    2 -> builder.minLength(decoder.int)
                    3 -> builder.maxLength(decoder.int)
                    4 -> builder.const(decoder.string)
                    5 -> builder.pattern(decoder.string)
                    6 -> enumSourceName = decoder.string
                    7 -> {
                        val size = decoder.arrayLength
                        enumValues = mutableSetOf()
                        for (y in 0 until size) {
                            enumValues.add(decoder.string)
                        }
                    }

                    8 -> builder.implIdentifier(decoder.string)
                } // TODO Consume unknown values
            }

            if (enumValues != null) builder.enum(enumValues, enumSourceName)
            return builder.build()
        }
    }

    override val type = TYPE

    @JsonIgnore
    val regex = lazy { pattern?.let(::Regex) }

    override fun validateRaw(nullable: Boolean, value: String?): ValidationConstraintError? =
        validate(nullable, value?.let(TextNode::valueOf))

    override fun validate(nullable: Boolean, value: JsonNode?): ValidationConstraintError? {
        val isNull = value == null || value.isNull || value.isMissingNode
        if (!isNull && !value!!.isTextual) return ValidationConstraintError.badType("string")

        val strValue = value?.textValue()
            ?: return if (nullable) null else ValidationConstraintError.notNullable()

        if (const != null && const != strValue) {
            return ValidationConstraintError("const", "does not match expected const value '$const'")
        }

        if (minLength != null && strValue.length < minLength) {
            return ValidationConstraintError("minLength", "is less than $minLength characters long")
        }
        if (maxLength != null && strValue.length > maxLength) {
            return ValidationConstraintError("maxLength", "is more than $maxLength characters long")
        }

        // TODO We should keep track of whether lowercase enum is enabled
        // TODO Support case insensitive enums
        if (enumValues != null && !enumValues.contains(strValue) && !enumValues.contains(strValue.lowercase())) {
            return ValidationConstraintError("enum", "has unknown enum value")
        }

        val regex = regex.value
        if (regex != null && !regex.matches(strValue)) {
            return ValidationConstraintError("pattern", "does not match expected pattern")
        }

        return if (format == null) null
        else FormatRegistry.getFormat(format)?.validate(strValue)
    }

    override fun toJsonSchemaDraft7Element(): ObjectNode {
        val schema = JsonNodeFactory.instance.objectNode()
            .put("type", "string")
        if (description != null) schema.put("description", description)
        if (format != null) schema.put("format", format)
        if (minLength != null) schema.put("minLength", minLength)
        if (maxLength != null) schema.put("maxLength", maxLength)
        if (const != null) schema.put("const", const)
        if (pattern != null) schema.put("pattern", pattern)
        if (enumValues != null) {
            schema.putArray("enum")
                .addAll(enumValues.map(TextNode::valueOf))
        }
        return schema
    }

    override fun serializeMessagePack(): Buffer {
        val ser = MsgPackSer()
        ser.putOptionalProperty(0, description)
        ser.putOptionalProperty(1, format)
        ser.putOptionalProperty(2, minLength)
        ser.putOptionalProperty(3, maxLength)
        ser.putOptionalProperty(4, const)
        ser.putOptionalProperty(5, pattern)
        ser.putOptionalProperty(6, enumSourceName)
        ser.putOptionalProperty(7, enumValues)
        ser.putOptionalProperty(8, implIdentifier)
        return ser.build(MSGPACK_TYPE)
    }

    class Builder : JsonSchemaBuilder {
        private var description: String? = null
        private var format: String? = null
        private var const: String? = null
        private var enumSourceName: String? = null
        private var enumValues: Set<String>? = null
        private var pattern: String? = null
        private var minLength: Int? = null
        private var maxLength: Int? = null
        private var implIdentifier: String? = null

        fun description(description: String) = apply { this.description = description }
        fun format(format: String) = apply { this.format = format }
        fun const(const: String) = apply { this.const = const }
        fun enum(values: Set<String>, sourceName: String? = null) = apply {
            enumSourceName = sourceName
            enumValues = values
        }

        fun <E : Enum<E>> enum(values: EnumEntries<E>, sourceName: String? = null, lowercase: Boolean = false) =
            enum(values.map { if (lowercase) it.name.lowercase() else it.name }.toSet(), sourceName)

        fun pattern(pattern: String) = apply { this.pattern = pattern }
        fun minLength(minLength: Int) = apply { this.minLength = minLength }
        fun maxLength(maxLength: Int) = apply { this.maxLength = maxLength }
        fun implIdentifier(implIdentifier: String) = apply { this.implIdentifier = implIdentifier }

        override fun build(): StringJsonSchema = StringJsonSchema(
            description,
            format,
            const,
            enumSourceName,
            enumValues,
            pattern,
            minLength,
            maxLength,
            implIdentifier,
        )
    }
}

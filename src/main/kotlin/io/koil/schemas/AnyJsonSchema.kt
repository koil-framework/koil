package io.koil.schemas

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ObjectNode
import io.koil.ser.MessagePackDecoder
import io.vertx.core.buffer.Buffer

val anyJsonSchema = ObjectJsonSchema.Builder()
    .sourceName("AnyJsonSchema")
    .description("A json schema representing any value")
    .polymorphicValue(AnyJsonSchema.TYPE)
    .build()

data class AnyJsonSchema(
    override val description: String?,
) : JsonSchema() {
    companion object {
        const val MSGPACK_TYPE: Byte = 18
        const val TYPE = "ANY"

        fun deserializeMessagePack(buffer: Buffer): AnyJsonSchema {
            var description: String? = null

            if (buffer.length() == 1) return AnyJsonSchema(description)

            val decoder = MessagePackDecoder(buffer)
            val count = decoder.mapSize
            for (i in 0 until count) {
                val type = decoder.int
                when (type) {
                    0 -> description = decoder.string
                } // TODO Consume unknown values
            }

            return AnyJsonSchema(description)
        }
    }

    override val type = TYPE

    override fun validateRaw(nullable: Boolean, value: String?): ValidationConstraintError? = null
    override fun validate(nullable: Boolean, value: JsonNode?): ValidationConstraintError? = null

    override fun toJsonSchemaDraft7Element(): ObjectNode {
        val sch = JsonNodeFactory.instance.objectNode()
        sch.putArray("type")
            .add("number")
            .add("integer")
            .add("string")
            .add("boolean")
            .add("object")
            .add("array")
        return sch
    }

    override fun serializeMessagePack(): Buffer {
        val ser = MsgPackSer()
        ser.putOptionalProperty(0, description)
        return ser.build(MSGPACK_TYPE)
    }
}

package io.koil.schemas

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ObjectNode
import io.koil.schemas.ArrayJsonSchema.Companion.TYPE
import io.koil.ser.MessagePackDecoder
import io.koil.ser.MessagePackEncoder
import io.vertx.core.buffer.Buffer

val arrayJsonSchema = ObjectJsonSchema.Builder()
    .sourceName("ArrayJsonSchema")
    .description("A json schema representing an array of values")
    .polymorphicValue(TYPE)
    .property("items", RefJsonSchema("The schema of the array items", "super"))
    .property("uniqueItems", BooleanJsonSchema("True to make sure the array only contains unique items"))
    .property("nullableItems", BooleanJsonSchema("True to allow null items"))
    .property(
        "minItems",
        IntegerJsonSchema.Builder().int32().minimum(0).description("An optional minimum number of items in the array"),
        true,
    )
    .property(
        "maxItems",
        IntegerJsonSchema.Builder().int32().minimum(0).description("An optional maximum number of items in the array"),
        true,
    )
    .build()

data class ArrayJsonSchema(
    override val description: String?,
    /** The schema of the array items */
    val items: JsonSchema,
    /** True to make sure the array only contains unique items */
    val uniqueItems: Boolean,
    /** True to allow null items */
    val nullableItems: Boolean,
    /** An optional minimum number of items in the array */
    val minItems: Int?,
    /** An optional maximum number of items in the array */
    val maxItems: Int?,
) : JsonSchema() {
    companion object {
        const val MSGPACK_TYPE: Byte = 14
        const val TYPE = "ARRAY"

        fun deserializeMessagePack(buffer: Buffer): ArrayJsonSchema {
            val decoder = MessagePackDecoder(buffer)

            val uniqueItems = decoder.boolean
            val nullableItems = decoder.boolean
            val schemaExt = decoder.ext

            var description: String? = null
            var minItems: Int? = null
            var maxItems: Int? = null

            val count = decoder.mapSize
            for (i in 0 until count) {
                val type = decoder.int
                when (type) {
                    0 -> description = decoder.string
                    1 -> minItems = decoder.int
                    2 -> maxItems = decoder.int
                } // TODO Consume unknown values
            }

            return ArrayJsonSchema(
                description,
                JsonSchema.deserializeMessagePack(schemaExt.type, schemaExt.content),
                uniqueItems,
                nullableItems,
                minItems,
                maxItems
            )
        }
    }

    override val type = TYPE

    override fun validateRaw(nullable: Boolean, value: String?): ValidationConstraintError? {
        if (value == null && !nullable) return ValidationConstraintError.notNullable()

        // TODO Can't we do more here ?
        return null
    }

    override fun validate(nullable: Boolean, value: JsonNode?): ValidationConstraintError? {
        val isNull = value == null || value.isNull || value.isMissingNode

        if (isNull && !nullable) return ValidationConstraintError.notNullable()
        else if (!isNull && !value!!.isArray) return ValidationConstraintError.badType("array")
        else if (isNull) return null
        value as ArrayNode

        if (minItems != null && value.size() < minItems) {
            return ValidationConstraintError("minItems", "has less than $minItems items")
        }
        if (maxItems != null && value.size() > maxItems) {
            return ValidationConstraintError("maxItems", "has more than $maxItems items")
        }

        // No need to validate unique items as they will always be converted to Java sets

        for ((index, item) in value.withIndex()) {
            val res = items.validate(nullableItems, item)
            if (res != null) {
                return ValidationConstraintError("items", "Item $index did not pass validation: ${res.message}")
            }
        }

        return null
    }

    override fun toJsonSchemaDraft7Element(): ObjectNode {
        val schema = JsonNodeFactory.instance.objectNode()
            .put("type", "array")
            .put("additionalItems", false)
        if (description != null) schema.put("description", description)
        if (minItems != null) schema.put("minItems", minItems)
        if (maxItems != null) schema.put("maxItems", maxItems)
        if (uniqueItems) schema.put("uniqueItems", true)
        schema.replace("items", items.toJsonSchemaDraft7Element())
        return schema
    }

    override fun serializeMessagePack(): Buffer {
        val props = Buffer.buffer()
        MessagePackEncoder.putBoolean(props, uniqueItems)
        MessagePackEncoder.putBoolean(props, nullableItems)
        props.appendBuffer(items.serializeMessagePack())

        val ser = MsgPackSer()
        ser.putOptionalProperty(0, description)
        ser.putOptionalProperty(1, minItems)
        ser.putOptionalProperty(2, maxItems)
        return ser.build(MSGPACK_TYPE, props)
    }

    class Builder : JsonSchemaBuilder {
        private var description: String? = null
        private lateinit var items: JsonSchema
        private var uniqueItems: Boolean = false
        private var nullableItems: Boolean = false
        private var minItems: Int? = null
        private var maxItems: Int? = null

        fun description(description: String): Builder = apply { this.description = description }
        fun items(items: JsonSchema): Builder = apply { this.items = items }
        fun items(items: JsonSchemaBuilder): Builder = apply { this.items = items.build() }
        fun uniqueItems(uniqueItems: Boolean = true): Builder = apply { this.uniqueItems = uniqueItems }
        fun nullableItems(nullableItems: Boolean = true): Builder = apply { this.nullableItems = nullableItems }
        fun minItems(minItems: Int): Builder = apply { this.minItems = minItems }
        fun maxItems(maxItems: Int): Builder = apply { this.maxItems = maxItems }

        override fun build(): ArrayJsonSchema = ArrayJsonSchema(
            description,
            items,
            uniqueItems,
            nullableItems,
            minItems,
            maxItems,
        )
    }
}

package io.koil.schemas

import io.koil.web.schema.UUIDValidator
import io.koil.web.schema.Validator

object FormatRegistry {
    private val formats = mutableMapOf<String, Validator<String>>()

    init {
        register("uuid", UUIDValidator)
    }

    fun register(format: String, validator: Validator<String>) {
        formats[format] = validator
    }

    fun getFormat(format: String): Validator<String>? = formats[format]
}

data class ValidationConstraintError(
    val constraint: String,
    val message: String,
) {
    companion object {
        fun notNullable(): ValidationConstraintError = ValidationConstraintError("nullable", "cannot be null")
        fun badType(type: String): ValidationConstraintError = ValidationConstraintError("type", "is not a $type")
        fun unknownSubtype(subType: String): ValidationConstraintError =
            ValidationConstraintError("subtype", "has unknown subtype $subType")
    }
}

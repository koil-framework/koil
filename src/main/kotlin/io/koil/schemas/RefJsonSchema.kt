package io.koil.schemas

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ObjectNode
import io.koil.schemas.RefJsonSchema.Companion.TYPE
import io.koil.ser.MessagePackDecoder
import io.koil.ser.MessagePackEncoder
import io.vertx.core.buffer.Buffer
import java.nio.charset.StandardCharsets

val refJsonSchema = ObjectJsonSchema.Builder()
    .sourceName("RefJsonSchema")
    .description("A json schema representing a reference to another schema")
    .polymorphicValue(TYPE)
    .property(
        "identifier",
        StringJsonSchema.Builder()
            .description(
                """
                The identifier of the referenced schema.
                
                Here are special values that can be used:
                - "self": references the current schema (to allow single-level recursive schemas)
                - "super": references the schema super-type (to allow multi-level recursive schemas)
            """.trimIndent()
            )
            .minLength(1)
    )
    .build()

data class RefJsonSchema(
    override val description: String?,
    /**
     * The identifier of the referenced schema
     *
     * Here are special values that can be used:
     * - "this": references the current schema (to allow single-level recursive schemas)
     * - "super": references the schema super-type (to allow multi-level recursive schemas)
     */
    val identifier: String,
) : JsonSchema() {
    companion object {
        const val MSGPACK_TYPE: Byte = 17
        const val TYPE = "REF"

        fun deserializeMessagePack(buffer: Buffer): RefJsonSchema {
            val decoder = MessagePackDecoder(buffer)

            val identifier = decoder.string
            val description = if (decoder.isNextString) decoder.string else null

            return RefJsonSchema(description, identifier)
        }
    }

    override val type = TYPE

    override fun validateRaw(nullable: Boolean, value: String?): ValidationConstraintError? {
        throw NotImplementedError("Called validateRaw on a schema ref")
    }

    override fun validate(nullable: Boolean, value: JsonNode?): ValidationConstraintError? {
        throw NotImplementedError("Called validate on a schema ref")
    }

    override fun toJsonSchemaDraft7Element(): ObjectNode {
        return JsonNodeFactory.instance.objectNode()
            .put("\$ref", identifier)
    }

    override fun serializeMessagePack(): Buffer {
        val buf = Buffer.buffer()

        val identifierBytes = identifier.toByteArray(StandardCharsets.UTF_8)
        val descriptionBytes = description?.toByteArray(StandardCharsets.UTF_8)

        MessagePackEncoder.putExtHeader(
            buf,
            MSGPACK_TYPE,
            MessagePackEncoder.getStringSize(identifierBytes.size) +
                    (descriptionBytes?.size?.let(MessagePackEncoder::getStringSize) ?: 0)
        )
        MessagePackEncoder.putRawString(buf, identifierBytes)
        if (descriptionBytes != null) MessagePackEncoder.putRawString(buf, descriptionBytes)
        return buf
    }
}

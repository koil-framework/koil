package io.koil.schemas

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.LongNode
import com.fasterxml.jackson.databind.node.ObjectNode
import io.koil.schemas.IntegerJsonSchema.Companion.TYPE
import io.koil.ser.MessagePackDecoder
import io.koil.ser.MessagePackEncoder
import io.vertx.core.buffer.Buffer

val integerJsonSchema = ObjectJsonSchema.Builder()
    .sourceName("IntegerJsonSchema")
    .description("A json schema representing an integral number")
    .polymorphicValue(TYPE)
    .property("int64", BooleanJsonSchema("True to make the value a 64-bits integer, false to make it 32-bits"))
    .property("minimum", IntegerJsonSchema.Builder().int64().description("An optional minimum value"), true)
    .property("maximum", IntegerJsonSchema.Builder().int64().description("An optional maximum value"), true)
    .build()

data class IntegerJsonSchema(
    override val description: String?,
    /** True to make the value a 64 bits integer, false to make it 32 bits */
    val int64: Boolean,
    /** An optional minimum value */
    val minimum: Long?,
    /** An optional maximum value */
    val maximum: Long?,
) : JsonSchema() {
    companion object {
        const val MSGPACK_TYPE: Byte = 11
        const val TYPE = "INTEGER"

        fun deserializeMessagePack(buffer: Buffer): IntegerJsonSchema {
            val builder = Builder()
            val decoder = MessagePackDecoder(buffer)

            builder.int64(decoder.boolean)

            // Means there are no optional properties
            if (buffer.length() == 2) builder.build()

            val count = decoder.mapSize

            for (i in 0 until count) {
                val type = decoder.int
                when (type) {
                    0 -> builder.description(decoder.string)
                    1 -> builder.minimum(decoder.long)
                    2 -> builder.maximum(decoder.long)
                } // TODO Consume unknown values
            }

            return builder.build()
        }
    }

    override val type = TYPE

    override fun validateRaw(nullable: Boolean, value: String?): ValidationConstraintError? {
        if (value == null && !nullable) return ValidationConstraintError.notNullable()
        val converted = value?.toLongOrNull()
        if (value != null && converted == null) return ValidationConstraintError.badType("integer")
        return validate(nullable, converted?.let(LongNode::valueOf))
    }

    override fun validate(nullable: Boolean, value: JsonNode?): ValidationConstraintError? {
        val isNull = value == null || value.isNull || value.isMissingNode
        if (!isNull && !value!!.isIntegralNumber) return ValidationConstraintError.badType("integer")

        val longValue = value?.longValue()
            ?: return if (nullable) null else ValidationConstraintError.notNullable()

        if (minimum != null && minimum > longValue) {
            return ValidationConstraintError("minimum", "is less than $minimum")
        }
        if (maximum != null && maximum < longValue) {
            return ValidationConstraintError("maximum", "is greater than $maximum")
        }

        return null
    }

    override fun toJsonSchemaDraft7Element(): ObjectNode {
        val schema = JsonNodeFactory.instance.objectNode()
            .put("type", "integer")
            .put("format", if (int64) "int64" else "int32")
        if (description != null) schema.put("description", description)
        if (minimum != null) schema.put("minimum", minimum)
        if (maximum != null) schema.put("maximum", maximum)
        return schema
    }

    override fun serializeMessagePack(): Buffer {
        val props = Buffer.buffer()
        MessagePackEncoder.putBoolean(props, int64)

        val ser = MsgPackSer()
        ser.putOptionalProperty(0, description)
        ser.putOptionalProperty(1, minimum)
        ser.putOptionalProperty(2, maximum)
        return ser.build(MSGPACK_TYPE, props)
    }

    class Builder : JsonSchemaBuilder {
        private var description: String? = null
        private var int64: Boolean = true
        private var minimum: Long? = null
        private var maximum: Long? = null

        fun description(description: String) = apply { this.description = description }
        fun int32(int32: Boolean = true) = apply { this.int64 = !int32 }
        fun int64(int64: Boolean = true) = apply { this.int64 = int64 }
        fun minimum(minimum: Long) = apply { this.minimum = minimum }
        fun maximum(maximum: Long) = apply { this.maximum = maximum }

        override fun build(): IntegerJsonSchema = IntegerJsonSchema(description, int64, minimum, maximum)
    }
}

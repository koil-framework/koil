package io.koil.schemas

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ObjectNode
import io.koil.ser.MessagePackDecoder
import io.koil.ser.MessagePackEncoder
import io.vertx.core.buffer.Buffer

val objectPropertySchema = ObjectJsonSchema.Builder()
    .sourceName("JsonSchemaObjectProperty")
    .implIdentifier("io.koil.schemas.JsonSchemaObjectProperty")
    .property(
        "name",
        StringJsonSchema.Builder()
            .description("The name of the property. Must be unique within an object")
            .minLength(1),
    )
    .property("schema", RefJsonSchema("The schema of the property", "JsonSchema"))
    .property(
        "polymorphicKey",
        BooleanJsonSchema("If the type is part of a polymorphic type, true to indicate this property is the key to differentiate sub-types")
    ).property(
        "nullable",
        BooleanJsonSchema("True to make the property nullable")
    ).property(
        "sourceName",
        StringJsonSchema.Builder()
            .description("The name of the property in the source code if the name of the property in the source code should be different from the json name")
            .minLength(1),
    ).property(
        "aliases",
        ArrayJsonSchema.Builder()
            .items(StringJsonSchema.Builder().minLength(1))
            .description("A list of additional names that should be accepted as this property when parsing the json")
            .uniqueItems(),
    ).property(
        "defaultValue",
        AnyJsonSchema("An optional default value to use when creating new objects"),
        true,
    ).build()

val objectJsonSchema = ObjectJsonSchema.Builder()
    .sourceName("ObjectJsonSchema")
    .description("A json schema representing an object")
    .polymorphicValue(ObjectJsonSchema.TYPE)
    .property(
        "sourceName",
        StringJsonSchema.Builder().minLength(1).description("The name of the object in the source code")
    )
    .property(
        "raw",
        BooleanJsonSchema("True to make this object a raw json object, not knowing properties in advance. All validations are ignored for raw objects.")
    ).property(
        "implIdentifier",
        StringJsonSchema.Builder()
            .description("An identifier pointing to an existing implementation of this object")
            .minLength(1),
        true,
    ).property(
        "additionalPropertiesKeys",
        RefJsonSchema(
            "If the object accepts additional properties, the schema allowing the validation of the additional properties keys",
            "super"
        ),
        true,
    ).property(
        "additionalPropertiesValues",
        RefJsonSchema(
            "If the object accepts additional properties, the schema allowing the validation of the additional properties values",
            "super"
        ),
        true,
    ).property(
        "nullableAdditionalProperties",
        BooleanJsonSchema("True to allow additional properties to have a null value"),
    ).property(
        "minProperties",
        IntegerJsonSchema.Builder()
            .int32()
            .description("An optional minimum number of properties in the object")
            .minimum(0),
        true,
    ).property(
        "maxProperties",
        IntegerJsonSchema.Builder()
            .int32()
            .description("An optional maximum number of properties in the object")
            .minimum(0),
        true,
    ).property(
        "subTypes",
        ArrayJsonSchema.Builder()
            .items(RefJsonSchema(null, "super"))
            .description("The list of sub-types if this schema represents a super-type"),
        true,
    ).property(
        "polymorphicValue",
        StringJsonSchema.Builder()
            .description("If this object is a subtype, the const value of the polymorphic key for this subtype")
            .minLength(1),
        true,
    ).property(
        "properties",
        ArrayJsonSchema.Builder()
            .items(objectPropertySchema)
            .description("The list of declared properties for this object"),
    ).build()

data class JsonSchemaObjectProperty(
    /* The name of the property. Must be unique within an object */
    val name: String,
    /** The schema of the property */
    val schema: JsonSchema,
    /** If the type is part of a polymorphic type, true to indicate this property is the key to differentiate sub-types */
    val polymorphicKey: Boolean,
    /** True to make the property nullable */
    val nullable: Boolean,
    /** The name of the property in the source code if the name of the property in the source code should be different from the json name */
    val sourceName: String,
    /** A list of additional names that should be accepted as this property when parsing the json */
    val aliases: Set<String>,
    /** An optional default value to use when creating new objects */
    val defaultValue: JsonNode?,
) {
    companion object {
        const val MSGPACK_TYPE: Byte = 16

        fun deserializeMessagePack(buffer: Buffer): JsonSchemaObjectProperty {
            val decoder = MessagePackDecoder(buffer)

            val name = decoder.string
            val schema = JsonSchema.deserializeMessagePack(decoder.ext)
            val polymorphicKey = decoder.boolean
            val nullable = decoder.boolean
            val sourceName = decoder.string
            val aliasesLength = decoder.arrayLength
            val aliases = mutableSetOf<String>()
            for (i in 0 until aliasesLength) {
                aliases.add(decoder.string)
            }
            val defaultValue = decoder.json

            return JsonSchemaObjectProperty(
                name,
                schema,
                polymorphicKey,
                nullable,
                sourceName,
                aliases,
                defaultValue,
            )
        }
    }

    fun serializeMessagePack(): Buffer {
        val buf = Buffer.buffer()
        MessagePackEncoder.putString(buf, name)
        buf.appendBuffer(schema.serializeMessagePack())
        MessagePackEncoder.putBoolean(buf, polymorphicKey)
        MessagePackEncoder.putBoolean(buf, nullable)
        MessagePackEncoder.putString(buf, sourceName)
        MessagePackEncoder.putArrayHeader(buf, aliases.size)
        for (alias in aliases) {
            MessagePackEncoder.putString(buf, alias)
        }
        MessagePackEncoder.putJson(buf, defaultValue)

        val buffer = Buffer.buffer()
        MessagePackEncoder.putExt(buffer, MSGPACK_TYPE, buf)
        return buffer
    }

    class Builder {
        private lateinit var name: String
        private lateinit var schema: JsonSchema
        private var polymorphicKey: Boolean = false
        private var nullable: Boolean = false
        private lateinit var sourceName: String
        private var aliases: Set<String> = emptySet()
        private var defaultValue: JsonNode? = null

        fun name(name: String, sourceName: String = name) = apply {
            this.name = name
            this.sourceName = sourceName
        }

        fun schema(schema: JsonSchema) = apply { this.schema = schema }
        fun schema(schema: JsonSchemaBuilder) = schema(schema.build())
        fun polymorphicKey(polymorphicKey: Boolean = true) = apply { this.polymorphicKey = polymorphicKey }
        fun nullable(nullable: Boolean = true) = apply { this.nullable = nullable }
        fun aliases(aliases: Set<String>) = apply { this.aliases = aliases }
        fun defaultValue(defaultValue: JsonNode?) = apply { this.defaultValue = defaultValue }

        fun build(): JsonSchemaObjectProperty =
            JsonSchemaObjectProperty(name, schema, polymorphicKey, nullable, sourceName, aliases, defaultValue)
    }
}

data class ObjectJsonSchema(
    override val description: String?,
    /** The name of the object in the source code */
    val sourceName: String,
    /** True to make this object a raw json object, not knowing properties in advance. All validations are ignored for raw objects. */
    val raw: Boolean,
    /** An identifier pointing to an existing implementation of this object */
    val implIdentifier: String?,
    /** If the object accepts additional properties, the schema allowing the validation of the additional properties keys */
    val additionalPropertiesKeys: JsonSchema?,
    /** If the object accepts additional properties, the schema allowing the validation of the additional properties values */
    val additionalPropertiesValues: JsonSchema?,
    /** True to allow additional properties to have a null value */
    val nullableAdditionalProperties: Boolean,
    /** An optional minimum number of properties in the object */
    val minProperties: Int?,
    /** An optional maximum number of properties in the object */
    val maxProperties: Int?,
    /** The list of declared properties for this object */
    val properties: List<JsonSchemaObjectProperty>,
    /** The list of sub-types if this schema represents a super-type */
    val subTypes: List<JsonSchema>?,
    /** If this object is a subtype, the const value of the polymorphic key for this subtype */
    val polymorphicValue: String?,
) : JsonSchema() {
    companion object {
        const val MSGPACK_TYPE: Byte = 15
        const val TYPE = "OBJECT"

        fun deserializeMessagePack(buffer: Buffer): ObjectJsonSchema {
            val decoder = MessagePackDecoder(buffer)
            val builder = Builder()

            builder.sourceName(decoder.string)
            builder.raw(decoder.boolean)
            builder.nullableAdditionalProperties(decoder.boolean)

            val propCount = decoder.arrayLength
            for (i in 0 until propCount) {
                val ext = decoder.ext
                builder.property(JsonSchemaObjectProperty.deserializeMessagePack(ext.content))
            }

            var additionalPropertiesKeys: JsonSchema? = null
            var additionalPropertiesValues: JsonSchema? = null

            val count = decoder.mapSize
            for (i in 0 until count) {
                val type = decoder.int
                when (type) {
                    0 -> builder.description(decoder.string)
                    1 -> builder.implIdentifier(decoder.string)
                    2 -> builder.minProperties(decoder.int)
                    3 -> builder.maxProperties(decoder.int)
                    4 -> additionalPropertiesKeys = JsonSchema.deserializeMessagePack(decoder.ext)
                    5 -> additionalPropertiesValues = JsonSchema.deserializeMessagePack(decoder.ext)
                    6 -> builder.polymorphicValue(decoder.string)
                    7 -> {
                        val size = decoder.arrayLength
                        for (z in 0 until size) {
                            builder.subType(JsonSchema.deserializeMessagePack(decoder.ext))
                        }
                    }
                } // TODO Consume unknown values
            }

            if (additionalPropertiesKeys != null && additionalPropertiesValues != null) {
                builder.additionalProperties(additionalPropertiesKeys, additionalPropertiesValues)
            }

            return builder.build()
        }
    }

    override val type = TYPE

    override fun validateRaw(nullable: Boolean, value: String?): ValidationConstraintError? {
        if (value == null && !nullable) return ValidationConstraintError.notNullable()

        // TODO Can't we do more here ?
        return null
    }

    override fun validate(nullable: Boolean, value: JsonNode?): ValidationConstraintError? =
        validate(nullable, value, null)

    private fun validate(
        nullable: Boolean,
        value: JsonNode?,
        supertypeProperties: List<JsonSchemaObjectProperty>?,
    ): ValidationConstraintError? {
        val isNull = value == null || value.isNull || value.isMissingNode
        if (isNull && !nullable) return ValidationConstraintError.notNullable()
        else if (!isNull && !value!!.isObject) return ValidationConstraintError.badType("object")
        else if (isNull || raw) return null

        value as ObjectNode

        // Validate subtypes through the subtype schema
        if (subTypes != null) {
            val typeProperty = properties.first { it.polymorphicKey }
            // TODO Take aliases into account
            val typePropertyValue = value.get(typeProperty.name)
            typeProperty.schema.validate(false, typePropertyValue)?.let { return it }

            // TODO We only support string polymorphic keys right now
            val typeValue = typePropertyValue.textValue()
            for (subType in subTypes) {
                if (subType !is ObjectJsonSchema) throw IllegalStateException("Refs must be resolved before validation")

                if (subType.polymorphicValue == typeValue) {
                    return subType.validate(nullable, value, supertypeProperties?.plus(properties) ?: properties)
                }
            }

            return ValidationConstraintError.unknownSubtype(typeValue)
        }

        if (minProperties != null && value.size() < minProperties) {
            return ValidationConstraintError("minProperties", "has less than $minProperties properties")
        }
        if (maxProperties != null && value.size() > maxProperties) {
            return ValidationConstraintError("maxProperties", "has more than $maxProperties properties")
        }

        // TODO Take aliases into account

        val missingProperties = HashSet(properties.map { it.name })
        if (supertypeProperties != null) missingProperties.addAll(supertypeProperties.map { it.name })

        for ((name, propValue) in value.fields()) {
            val prop = properties.firstOrNull { it.name == name || it.aliases.contains(name) }
                ?: supertypeProperties?.firstOrNull { it.name == name || it.aliases.contains(name) }
            if (prop != null) {
                val res = prop.schema.validate(prop.nullable, propValue)
                if (res != null) {
                    return ValidationConstraintError("property", "property $name failed validation: ${res.message}")
                }

                missingProperties.remove(name)
            } else {
                if (additionalPropertiesKeys == null || additionalPropertiesValues == null) {
                    return ValidationConstraintError("unknownProperty", "unknown property $name")
                }

                val keyRes = additionalPropertiesKeys.validateRaw(false, name)
                if (keyRes != null) {
                    return ValidationConstraintError(
                        "additionalPropertyKey",
                        "additional property key $name failed validation: ${keyRes.message}"
                    )
                }

                val valueRes = additionalPropertiesValues.validate(nullableAdditionalProperties, propValue)
                if (valueRes != null) {
                    return ValidationConstraintError(
                        "additionalPropertyValue",
                        "additional property value $name failed validation: ${valueRes.message}"
                    )
                }
            }
        }

        for (missingProperty in missingProperties) {
            val property = properties.firstOrNull { it.name == missingProperty }
                ?: supertypeProperties?.firstOrNull { it.name == missingProperty }
                ?: throw IllegalStateException("Failed to find property $missingProperty in either sub-type or supertype schema")
            if (!property.nullable) {
                return ValidationConstraintError("property", "property $missingProperty cannot be null")
            }
        }

        return null
    }

    override fun toJsonSchemaDraft7Element(): ObjectNode {
        // TODO Add polymorphic subtypes

        val schema = JsonNodeFactory.instance.objectNode()
            .put("type", "object")
            .put("title", sourceName)
        if (description != null) schema.put("description", description)
        if (minProperties != null) schema.put("minProperties", minProperties)
        if (maxProperties != null) schema.put("maxProperties", maxProperties)

        if (raw) {
            schema.put("additionalProperties", true)
            return schema
        }

        val propertiesSchema = schema.putObject("properties")
        val requiredArray = schema.putArray("required")

        for (property in properties) {
            if (!property.nullable) requiredArray.add(property.name)
            propertiesSchema.replace(property.name, property.schema.toJsonSchemaDraft7Element())
        }

        if (additionalPropertiesKeys == null || additionalPropertiesValues == null) {
            schema.put("additionalProperties", false)
        } else {
            schema.replace("additionalProperties", additionalPropertiesValues.toJsonSchemaDraft7Element())
        }

        return schema
    }

    override fun serializeMessagePack(): Buffer {
        val props = Buffer.buffer()
        MessagePackEncoder.putString(props, sourceName)
        MessagePackEncoder.putBoolean(props, raw)
        MessagePackEncoder.putBoolean(props, nullableAdditionalProperties)
        MessagePackEncoder.putArrayHeader(props, properties.size)
        for (property in properties) {
            props.appendBuffer(property.serializeMessagePack())
        }

        val ser = MsgPackSer()
        ser.putOptionalProperty(0, description)
        ser.putOptionalProperty(1, implIdentifier)
        ser.putOptionalProperty(2, minProperties)
        ser.putOptionalProperty(3, maxProperties)
        ser.putOptionalExt(4, additionalPropertiesKeys?.serializeMessagePack())
        ser.putOptionalExt(5, additionalPropertiesValues?.serializeMessagePack())
        ser.putOptionalProperty(6, polymorphicValue)
        ser.putOptionalExtArray(7, subTypes, JsonSchema::serializeMessagePack)
        return ser.build(MSGPACK_TYPE, props)
    }

    fun merge(other: ObjectJsonSchema): ObjectJsonSchema {
        val resolvedMinProperties =
            if (minProperties == null && other.minProperties == null) null
            else if (minProperties != null && other.minProperties != null) minProperties + other.minProperties
            else other.minProperties ?: minProperties

        val resolvedMaxProperties =
            if (maxProperties == null && other.maxProperties == null) null
            else if (maxProperties != null && other.maxProperties != null) maxProperties + other.maxProperties
            else other.maxProperties ?: maxProperties

        val resolvedProperties = mutableListOf<JsonSchemaObjectProperty>()
        // TODO Merge properties by name, aliases and source name
        resolvedProperties.addAll(properties)
        resolvedProperties.addAll(other.properties)

        // TODO Super type/sub type merge

        return ObjectJsonSchema(
            other.description,
            other.sourceName,
            other.raw,
            other.implIdentifier,
            other.additionalPropertiesKeys ?: additionalPropertiesKeys,
            other.additionalPropertiesValues ?: additionalPropertiesValues,
            other.nullableAdditionalProperties || nullableAdditionalProperties,
            resolvedMinProperties,
            resolvedMaxProperties,
            resolvedProperties,
            other.subTypes,
            other.polymorphicValue,
        )
    }

    /** Creates a builder from this schema to add new elements. This schema is immutable, and will not be modified */
    fun extend(): Builder = Builder(this)

    /** Creates a copy of this schema, making all properties nullable */
    fun partial(
        /** A description to replace the current one. Null to use the current description */
        newDescription: String? = null,
        /** A prefix to add to the object source name */
        prefix: String = "Partial"
    ): ObjectJsonSchema = copy(
        description = newDescription ?: description,
        sourceName = prefix + sourceName,
        properties = properties.map { it.copy(nullable = true) },
    )

    class Builder() : JsonSchemaBuilder {
        private var description: String? = null
        private lateinit var sourceName: String
        private var raw = false
        private var minProperties: Int? = null
        private var maxProperties: Int? = null
        private val properties = mutableListOf<JsonSchemaObjectProperty>()
        private var additionalPropertiesKeys: JsonSchema? = null
        private var additionalPropertiesValues: JsonSchema? = null
        private var nullableAdditionalProperties = false
        private var implIdentifier: String? = null
        private var subTypes: MutableList<JsonSchema>? = null
        private var polymorphicValue: String? = null

        internal constructor(other: ObjectJsonSchema) : this() {
            description = other.description
            sourceName = other.sourceName
            raw = other.raw
            minProperties = other.minProperties
            maxProperties = other.maxProperties
            properties.addAll(other.properties)
            additionalPropertiesKeys = other.additionalPropertiesKeys
            additionalPropertiesValues = other.additionalPropertiesValues
            nullableAdditionalProperties = other.nullableAdditionalProperties
            implIdentifier = other.implIdentifier
            subTypes = other.subTypes?.toMutableList()
            polymorphicValue = other.polymorphicValue
        }

        fun description(description: String) = apply { this.description = description }
        fun sourceName(sourceName: String) = apply { this.sourceName = sourceName }
        fun raw(raw: Boolean = true) = apply { this.raw = raw }
        fun minProperties(minProperties: Int) = apply { this.minProperties = minProperties }
        fun maxProperties(maxProperties: Int) = apply { this.maxProperties = maxProperties }
        fun implIdentifier(implIdentifier: String) = apply { this.implIdentifier = implIdentifier }

        fun property(property: JsonSchemaObjectProperty) = apply { properties.add(property) }

        fun property(fn: (builder: JsonSchemaObjectProperty.Builder) -> JsonSchemaObjectProperty.Builder) = apply {
            properties.add(fn(JsonSchemaObjectProperty.Builder()).build())
        }

        fun property(name: String, schema: JsonSchema, nullable: Boolean = false) =
            property { it.name(name).schema(schema).nullable(nullable) }

        fun property(name: String, schema: JsonSchemaBuilder, nullable: Boolean = false) =
            property { it.name(name).schema(schema).nullable(nullable) }

        fun additionalProperties(keys: JsonSchema, values: JsonSchema) = apply {
            additionalPropertiesKeys = keys
            additionalPropertiesValues = values
        }

        fun additionalProperties(keys: JsonSchemaBuilder, values: JsonSchema) =
            additionalProperties(keys.build(), values)

        fun additionalProperties(keys: JsonSchema, values: JsonSchemaBuilder) =
            additionalProperties(keys, values.build())

        fun additionalProperties(keys: JsonSchemaBuilder, values: JsonSchemaBuilder) =
            additionalProperties(keys.build(), values.build())

        fun nullableAdditionalProperties(nullableAdditionalProperties: Boolean) =
            apply { this.nullableAdditionalProperties = nullableAdditionalProperties }

        fun subType(schema: JsonSchema) = apply {
            if (schema !is ObjectJsonSchema && schema !is RefJsonSchema) {
                throw IllegalArgumentException("Schema must be an object or a ref")
            } else if (schema is ObjectJsonSchema && schema.polymorphicValue == null) {
                throw IllegalArgumentException("Sub-types schemas must have their polymorphic values set")
                // TODO Validate refs polymorphic values
            }

            if (subTypes == null) subTypes = mutableListOf()
            subTypes!!.add(schema)
        }

        fun subType(schema: Builder) = apply {
            if (subTypes == null) subTypes = mutableListOf()
            if (schema.polymorphicValue == null) {
                throw IllegalArgumentException("Sub-types schemas must have their polymorphic values set")
            }
            subTypes!!.add(schema.build())
        }

        fun polymorphicValue(value: String) = apply { this.polymorphicValue = value }

        // TODO Validate supertype/subtype polymorphic key property

        override fun build(): ObjectJsonSchema = ObjectJsonSchema(
            description,
            sourceName,
            raw,
            implIdentifier,
            additionalPropertiesKeys,
            additionalPropertiesValues,
            nullableAdditionalProperties,
            minProperties,
            maxProperties,
            properties,
            subTypes,
            polymorphicValue,
        )
    }
}



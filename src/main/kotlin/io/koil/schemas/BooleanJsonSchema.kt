package io.koil.schemas

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ObjectNode
import io.koil.schemas.BooleanJsonSchema.Companion.TYPE
import io.koil.ser.MessagePackDecoder
import io.vertx.core.buffer.Buffer

val booleanJsonSchema = ObjectJsonSchema.Builder()
    .sourceName("BooleanJsonSchema")
    .description("A json schema representing a boolean")
    .polymorphicValue(TYPE)
    .build()

data class BooleanJsonSchema(
    override val description: String? = null,
) : JsonSchema() {
    companion object {
        const val MSGPACK_TYPE: Byte = 13
        const val TYPE = "BOOLEAN"

        fun deserializeMessagePack(buffer: Buffer): BooleanJsonSchema {
            var description: String? = null

            if (buffer.length() == 1) return BooleanJsonSchema(description)

            val decoder = MessagePackDecoder(buffer)
            val count = decoder.mapSize
            for (i in 0 until count) {
                val type = decoder.int
                when (type) {
                    0 -> description = decoder.string
                } // TODO Consume unknown values
            }

            return BooleanJsonSchema(description)
        }
    }

    override val type = TYPE

    override fun validateRaw(nullable: Boolean, value: String?): ValidationConstraintError? {
        if (value == null && !nullable) return ValidationConstraintError.notNullable()
        return null
    }

    override fun validate(nullable: Boolean, value: JsonNode?): ValidationConstraintError? {
        val isNull = value == null || value.isNull || value.isMissingNode

        if (isNull && !nullable) return ValidationConstraintError.notNullable()
        else if (!isNull && !value!!.isBoolean) return ValidationConstraintError.badType("boolean")
        return null
    }

    override fun toJsonSchemaDraft7Element(): ObjectNode {
        val schema = JsonNodeFactory.instance.objectNode()
            .put("type", "boolean")
        if (description != null) schema.put("description", description)
        return schema
    }

    override fun serializeMessagePack(): Buffer {
        val ser = MsgPackSer()
        ser.putOptionalProperty(0, description)
        return ser.build(MSGPACK_TYPE)
    }
}

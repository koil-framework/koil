package io.koil.schemas

import io.koil.ser.MessagePackEncoder
import io.vertx.core.buffer.Buffer

class MsgPackSer {
    private var count: Int = 0
    private val propBuffer = Buffer.buffer()

    fun putOptionalProperty(index: Int, value: String?) {
        if (value == null) return

        MessagePackEncoder.putInt(propBuffer, index)
        MessagePackEncoder.putString(propBuffer, value)
        count++
    }

    fun putOptionalProperty(index: Int, value: Boolean?) {
        if (value == null) return

        MessagePackEncoder.putInt(propBuffer, index)
        MessagePackEncoder.putBoolean(propBuffer, value)
        count++
    }

    fun putOptionalProperty(index: Int, value: Int?) {
        if (value == null) return

        MessagePackEncoder.putInt(propBuffer, index)
        MessagePackEncoder.putInt(propBuffer, value)
        count++
    }

    fun putOptionalProperty(index: Int, value: Long?) {
        if (value == null) return

        MessagePackEncoder.putInt(propBuffer, index)
        MessagePackEncoder.putLong(propBuffer, value)
        count++
    }

    fun putOptionalProperty(index: Int, value: Float?) {
        if (value == null) return

        MessagePackEncoder.putInt(propBuffer, index)
        MessagePackEncoder.putFloat(propBuffer, value)
        count++
    }

    fun putOptionalProperty(index: Int, value: Double?) {
        if (value == null) return

        MessagePackEncoder.putInt(propBuffer, index)
        MessagePackEncoder.putDouble(propBuffer, value)
        count++
    }

    fun putOptionalProperty(index: Int, value: Set<String>?) {
        if (value == null) return

        MessagePackEncoder.putInt(propBuffer, index)
        MessagePackEncoder.putArrayHeader(propBuffer, value.size)
        for (v in value) {
            MessagePackEncoder.putString(propBuffer, v)
        }
        count++
    }

    fun putOptionalExt(index: Int, value: Buffer?) {
        if (value == null) return

        MessagePackEncoder.putInt(propBuffer, index)
        propBuffer.appendBuffer(value)
        count++
    }

    fun <T> putOptionalExtArray(index: Int, value: List<T>?, ser: (T) -> Buffer) {
        if (value == null) return

        MessagePackEncoder.putInt(propBuffer, index)
        MessagePackEncoder.putArrayHeader(propBuffer, value.size)
        for (v in value) {
            propBuffer.appendBuffer(ser(v))
        }

        count++
    }

    fun build(extType: Byte, fixedProperties: Buffer? = null): Buffer {
        val buf = Buffer.buffer()

        val propHeaderSize = MessagePackEncoder.getMapHeaderSize(count)
        MessagePackEncoder.putExtHeader(
            buf,
            extType,
            (fixedProperties?.length() ?: 0) + propHeaderSize + propBuffer.length()
        )

        // Fixed properties
        if (fixedProperties != null) buf.appendBuffer(fixedProperties)

        // Optional properties
        MessagePackEncoder.putMapHeader(buf, count)
        if (count != 0) buf.appendBuffer(propBuffer)

        return buf
    }
}

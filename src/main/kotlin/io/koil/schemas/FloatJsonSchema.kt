package io.koil.schemas

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.DoubleNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ObjectNode
import io.koil.ser.MessagePackDecoder
import io.koil.ser.MessagePackEncoder
import io.vertx.core.buffer.Buffer

val floatJsonSchema = ObjectJsonSchema.Builder()
    .sourceName("FloatJsonSchema")
    .description("A json schema representing an floating point number")
    .polymorphicValue(FloatJsonSchema.TYPE)
    .property(
        "double",
        BooleanJsonSchema("True to make the value a 64-bits floating-point number, false to make it 32 bits")
    )
    .property("minimum", FloatJsonSchema.Builder().double().description("An optional minimum value"), true)
    .property("maximum", FloatJsonSchema.Builder().double().description("An optional maximum value"), true)
    .build()

data class FloatJsonSchema(
    override val description: String?,
    /** True to make the value a 64-bits floating-point number, false to make it 32-bits */
    val double: Boolean,
    /** An optional minimum value */
    val minimum: Double?,
    /** An optional maximum value */
    val maximum: Double?,
) : JsonSchema() {
    companion object {
        const val MSGPACK_TYPE: Byte = 12
        const val TYPE = "FLOAT"

        fun deserializeMessagePack(buffer: Buffer): FloatJsonSchema {
            val builder = Builder()
            val decoder = MessagePackDecoder(buffer)

            builder.double(decoder.boolean)

            // Means there are no optional properties
            if (buffer.length() == 2) builder.build()

            val count = decoder.mapSize

            for (i in 0 until count) {
                val type = decoder.int
                when (type) {
                    0 -> builder.description(decoder.string)
                    1 -> builder.minimum(decoder.double)
                    2 -> builder.maximum(decoder.double)
                } // TODO Consume unknown values
            }

            return builder.build()
        }
    }

    override val type = TYPE

    override fun validateRaw(nullable: Boolean, value: String?): ValidationConstraintError? {
        if (value == null && !nullable) return ValidationConstraintError.notNullable()
        val converted = value?.toDoubleOrNull()
        if (value != null && converted == null) return ValidationConstraintError.badType("float")
        return validate(nullable, converted?.let(DoubleNode::valueOf))
    }

    override fun validate(nullable: Boolean, value: JsonNode?): ValidationConstraintError? {
        val isNull = value == null || value.isNull || value.isMissingNode
        if (!isNull && !value!!.isNumber) return ValidationConstraintError.badType("float")

        val doubleValue = value?.doubleValue()
            ?: return if (nullable) null else ValidationConstraintError.notNullable()

        if (minimum != null && minimum > doubleValue) {
            return ValidationConstraintError("minimum", "is less than $minimum")
        }
        if (maximum != null && maximum < doubleValue) {
            return ValidationConstraintError("maximum", "is greater than $maximum")
        }

        return null
    }

    override fun toJsonSchemaDraft7Element(): ObjectNode {
        val schema = JsonNodeFactory.instance.objectNode()
            .put("type", "number")
            .put("format", if (double) "double" else "float")
        if (description != null) schema.put("description", description)
        if (minimum != null) schema.put("minimum", minimum)
        if (maximum != null) schema.put("maximum", maximum)
        return schema
    }

    override fun serializeMessagePack(): Buffer {
        val props = Buffer.buffer()
        MessagePackEncoder.putBoolean(props, double)

        val ser = MsgPackSer()
        ser.putOptionalProperty(0, description)
        ser.putOptionalProperty(1, minimum)
        ser.putOptionalProperty(2, maximum)
        return ser.build(MSGPACK_TYPE, props)
    }

    class Builder : JsonSchemaBuilder {
        private var description: String? = null
        private var double: Boolean = true
        private var minimum: Double? = null
        private var maximum: Double? = null

        fun description(description: String) = apply { this.description = description }
        fun float(float: Boolean = true) = apply { this.double = !float }
        fun double(double: Boolean = true) = apply { this.double = double }
        fun minimum(minimum: Double) = apply { this.minimum = minimum }
        fun maximum(maximum: Double) = apply { this.maximum = maximum }

        override fun build(): FloatJsonSchema = FloatJsonSchema(description, double, minimum, maximum)
    }
}

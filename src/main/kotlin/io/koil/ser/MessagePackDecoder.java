package io.koil.ser;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.*;
import io.vertx.core.buffer.Buffer;
import org.jetbrains.annotations.Nullable;

import java.nio.charset.StandardCharsets;

public class MessagePackDecoder {
    private final Buffer buffer;
    private int offset;

    public MessagePackDecoder(Buffer buffer) {
        this(buffer, 0);
    }

    public MessagePackDecoder(Buffer buffer, int offset) {
        this.buffer = buffer;
        this.offset = offset;
    }

    private String typeToName(byte type) {
        if ((type & (byte) 0xe0) == MessagePackTypes.FIXSTR_PREFIX) return "FIXSTR";
        else if ((type & (byte) 0xf0) == MessagePackTypes.FIXARRAY_PREFIX) return "FIXARRAY";
        else if ((type & (byte) 0xf0) == MessagePackTypes.FIXMAP_PREFIX) return "FIXMAP";

        int v = type & 0xFF;
        if (v <= 0x7f || v >= 0xe0) return "FIXINT";

        return switch (type) {
            case MessagePackTypes.FLOAT32 -> "FLOAT32";
            case MessagePackTypes.FLOAT64 -> "FLOAT64";
            case MessagePackTypes.INT8 -> "INT8";
            case MessagePackTypes.INT16 -> "INT16";
            case MessagePackTypes.INT32 -> "INT32";
            case MessagePackTypes.STR8 -> "STR8";
            case MessagePackTypes.STR16 -> "STR16";
            case MessagePackTypes.STR32 -> "STR32";
            case MessagePackTypes.FALSE -> "FALSE (Boolean)";
            case MessagePackTypes.TRUE -> "TRUE (Boolean)";
            case MessagePackTypes.NIL -> "NIL";
            case MessagePackTypes.NEVER_USED -> "NEVER";
            case MessagePackTypes.ARRAY16 -> "ARRAY16";
            case MessagePackTypes.ARRAY32 -> "ARRAY32";
            case MessagePackTypes.BIN8 -> "BIN8";
            case MessagePackTypes.BIN16 -> "BIN16";
            case MessagePackTypes.BIN32 -> "BIN32";
            case MessagePackTypes.FIXEXT1 -> "FIXEXT1";
            case MessagePackTypes.FIXEXT2 -> "FIXEXT2";
            case MessagePackTypes.FIXEXT4 -> "FIXEXT4";
            case MessagePackTypes.FIXEXT8 -> "FIXEXT8";
            case MessagePackTypes.FIXEXT16 -> "FIXEXT16";
            case MessagePackTypes.EXT8 -> "EXT8";
            case MessagePackTypes.EXT16 -> "EXT16";
            case MessagePackTypes.EXT32 -> "EXT32";
            case MessagePackTypes.MAP16 -> "MAP16";
            case MessagePackTypes.MAP32 -> "MAP32";

            default -> "UNKNOWN (" + type + ")";
        };
    }

    private int getStringSize() {
        byte type = buffer.getByte(offset);

        if ((type & (byte) 0xe0) == MessagePackTypes.FIXSTR_PREFIX) {
            offset++;
            return type & 0x1f;
        }

        switch (type) {
            case MessagePackTypes.STR8:
                offset += 2;
                return buffer.getByte(offset - 1);
            case MessagePackTypes.STR16:
                offset += 3;
                return buffer.getShort(offset - 2);
            case MessagePackTypes.STR32:
                offset += 5;
                return buffer.getInt(offset - 4);
            default:
                throw new MessagePackException("STR", typeToName(type), offset);
        }
    }

    public boolean isNextString() {
        byte type = buffer.getByte(offset);

        if ((type & (byte) 0xe0) == MessagePackTypes.FIXSTR_PREFIX) {
            return true;
        }

        return type == MessagePackTypes.STR8 || type == MessagePackTypes.STR16 || type == MessagePackTypes.STR32;
    }

    public String getString() {
        int length = getStringSize();
        if (length == 0) return "";

        byte[] bytes = new byte[length];
        buffer.getBytes(offset, offset + length, bytes);
        offset += length;
        return new String(bytes, StandardCharsets.UTF_8);
    }

    private boolean isBoolean(byte type) {
        return type == MessagePackTypes.TRUE || type == MessagePackTypes.FALSE;
    }

    public boolean isNextBoolean() {
        return isBoolean(buffer.getByte(offset));
    }

    public boolean getBoolean() {
        byte type = buffer.getByte(offset);
        if (!isBoolean(type)) throw new MessagePackException("BOOLEAN", typeToName(type), offset);
        offset++;
        return type == MessagePackTypes.TRUE;
    }

    public boolean isNextFloat() {
        return buffer.getByte(offset) == MessagePackTypes.FLOAT32;
    }

    public float getFloat() {
        byte type = buffer.getByte(offset);
        if (type != MessagePackTypes.FLOAT32)
            throw new MessagePackException("FLOAT32", typeToName(type), offset);

        offset += 5;
        return buffer.getFloat(offset - 4);
    }

    public boolean isNextDouble() {
        byte type = buffer.getByte(offset);
        return type == MessagePackTypes.FLOAT32 || type == MessagePackTypes.FLOAT64;
    }

    public double getDouble() {
        byte type = buffer.getByte(offset);

        if (type == MessagePackTypes.FLOAT32) {
            offset += 5;
            return buffer.getFloat(offset - 4);
        } else if (type == MessagePackTypes.FLOAT64) {
            offset += 9;
            return buffer.getDouble(offset - 8);
        } else {
            throw new MessagePackException("FLOAT64", typeToName(type), offset);
        }
    }

    public boolean isNextInt() {
        byte type = buffer.getByte(offset);

        // FIXINT
        int v = type & 0xFF;
        if (v <= 0x7f || v >= 0xe0) return true;

        return type == MessagePackTypes.UINT8 || type == MessagePackTypes.UINT16 || type == MessagePackTypes.UINT32 ||
                type == MessagePackTypes.INT8 || type == MessagePackTypes.INT16 || type == MessagePackTypes.INT32;
    }

    public int getInt() {
        byte type = buffer.getByte(offset++);

        // FIXINT
        int v = type & 0xFF;
        if (v <= 0x7f || v >= 0xe0) return type;

        if (type == MessagePackTypes.UINT8) {
            offset += 2;
            byte u8 = buffer.getByte(offset - 1);
            return u8 & 0xff;
        } else if (type == MessagePackTypes.UINT16) {
            offset += 3;
            short u16 = buffer.getShort(offset - 2);
            return u16 & 0xffff;
        } else if (type == MessagePackTypes.UINT32) {
            offset += 5;
            int u32 = buffer.getInt(offset - 4);
            if (u32 < 0) throw new MessagePackException("UINT32 overflow");
            return u32;
        } else if (type == MessagePackTypes.INT8) {
            offset += 2;
            return buffer.getByte(offset - 1);
        } else if (type == MessagePackTypes.INT16) {
            offset += 3;
            return buffer.getShort(offset - 2);
        } else if (type == MessagePackTypes.INT32) {
            offset += 5;
            return buffer.getInt(offset - 4);
        } else {
            throw new MessagePackException("INT32", typeToName(type), offset);
        }
    }

    public boolean isNextLong() {
        byte type = buffer.getByte(offset);

        // FIXINT
        int v = type & 0xFF;
        if (v <= 0x7f || v >= 0xe0) return true;

        return type == MessagePackTypes.UINT8 || type == MessagePackTypes.UINT16 || type == MessagePackTypes.UINT32 || type == MessagePackTypes.UINT64 ||
                type == MessagePackTypes.INT8 || type == MessagePackTypes.INT16 || type == MessagePackTypes.INT32 || type == MessagePackTypes.INT64;
    }

    public long getLong() {
        if (isNextInt()) return getInt();

        byte type = buffer.getByte(offset++);

        if (type == MessagePackTypes.UINT64) {
            offset += 9;
            long u64 = buffer.getLong(offset - 8);
            if (u64 < 0L) throw new MessagePackException("UINT64 overflow");
            return u64;
        } else if (type == MessagePackTypes.INT64) {
            offset += 9;
            return buffer.getLong(offset - 8);
        } else {
            throw new MessagePackException("INT64", typeToName(type), offset);
        }
    }

    public boolean isNextNull() {
        return buffer.getByte(offset) == MessagePackTypes.NIL;
    }

    public void consumeNull() {
        if (!isNextNull()) throw new MessagePackException("NIL", typeToName(buffer.getByte(offset)), offset);
        offset++;
    }

    public boolean isNextArray() {
        byte type = buffer.getByte(offset);
        return (type & (byte) 0xf0) == MessagePackTypes.FIXARRAY_PREFIX || type == MessagePackTypes.ARRAY16 || type == MessagePackTypes.ARRAY32;
    }

    public int getArrayLength() {
        byte type = buffer.getByte(offset);

        if ((type & (byte) 0xf0) == MessagePackTypes.FIXARRAY_PREFIX) {
            offset++;
            return type & 0x0f;
        } else if (type == MessagePackTypes.ARRAY16) {
            offset += 3;
            short u16 = buffer.getShort(offset - 2);
            return u16 & 0xffff;
        } else if (type == MessagePackTypes.ARRAY32) {
            offset += 5;
            int u32 = buffer.getInt(offset - 4);
            if (u32 < 0) throw new MessagePackException("UINT32 overflow");
            return u32;
        } else {
            throw new MessagePackException("ARRAY", typeToName(type), offset);
        }
    }

    public boolean isNextBin() {
        byte type = buffer.getByte(offset);
        return type == MessagePackTypes.BIN8 || type == MessagePackTypes.BIN16 || type == MessagePackTypes.BIN32;
    }

    public Buffer getBuffer() {
        byte type = buffer.getByte(offset);
        if (!isNextBin()) throw new MessagePackException("BIN", typeToName(type), offset);

        int size = switch (type) {
            case MessagePackTypes.BIN8 -> {
                offset += 2;
                yield buffer.getByte(offset - 1);
            }
            case MessagePackTypes.BIN16 -> {
                offset += 3;
                yield buffer.getShort(offset - 2);
            }
            case MessagePackTypes.BIN32 -> {
                offset += 5;
                yield buffer.getInt(offset - 4);
            }
            default -> throw new RuntimeException("Cannot happen");
        };

        Buffer b = buffer.getBuffer(offset, offset + size);
        offset += size;
        return b;
    }

    public boolean isNextExt() {
        byte type = buffer.getByte(offset);
        return type == MessagePackTypes.EXT8 || type == MessagePackTypes.EXT16 || type == MessagePackTypes.EXT32 ||
                type == MessagePackTypes.FIXEXT1 || type == MessagePackTypes.FIXEXT2 || type == MessagePackTypes.FIXEXT4 || type == MessagePackTypes.FIXEXT8 || type == MessagePackTypes.FIXEXT16;
    }

    public MessagePackExt getExt() {
        byte type = buffer.getByte(offset);
        if (!isNextExt()) throw new MessagePackException("BIN", typeToName(type), offset);

        offset++;

        switch (type) {
            case MessagePackTypes.FIXEXT1: {
                byte extType = buffer.getByte(offset);
                offset += 2;
                return new MessagePackExt(extType, buffer.getBuffer(offset - 1, offset));
            }
            case MessagePackTypes.FIXEXT2: {
                byte extType = buffer.getByte(offset);
                offset += 3;
                return new MessagePackExt(extType, buffer.getBuffer(offset - 2, offset));
            }
            case MessagePackTypes.FIXEXT4: {
                byte extType = buffer.getByte(offset);
                offset += 5;
                return new MessagePackExt(extType, buffer.getBuffer(offset - 4, offset));
            }
            case MessagePackTypes.FIXEXT8: {
                byte extType = buffer.getByte(offset);
                offset += 9;
                return new MessagePackExt(extType, buffer.getBuffer(offset - 8, offset));
            }
            case MessagePackTypes.FIXEXT16: {
                byte extType = buffer.getByte(offset);
                offset += 17;
                return new MessagePackExt(extType, buffer.getBuffer(offset - 16, offset));
            }
            case MessagePackTypes.EXT8: {
                int length = buffer.getByte(offset) & 0xff;
                byte extType = buffer.getByte(offset + 1);
                offset += 2 + length;
                return new MessagePackExt(extType, buffer.getBuffer(offset - length, offset));
            }
            case MessagePackTypes.EXT16: {
                int length = buffer.getShort(offset) & 0xffff;
                byte extType = buffer.getByte(offset + 2);
                offset += 3 + length;
                return new MessagePackExt(extType, buffer.getBuffer(offset - length, offset));
            }
            case MessagePackTypes.EXT32: {
                int length = buffer.getInt(offset);
                if (length < 0) throw new MessagePackException("UINT32 overflow");
                byte extType = buffer.getByte(offset + 4);
                offset += 5 + length;
                return new MessagePackExt(extType, buffer.getBuffer(offset - length, offset));
            }
            default:
                throw new RuntimeException("Cannot happen");
        }
    }

    public boolean isNextMap() {
        byte type = buffer.getByte(offset);

        return (type & (byte) 0xf0) == MessagePackTypes.FIXMAP_PREFIX || type == MessagePackTypes.MAP16 || type == MessagePackTypes.MAP32;
    }

    public int getMapSize() {
        byte type = buffer.getByte(offset);

        if ((type & (byte) 0xf0) == MessagePackTypes.FIXMAP_PREFIX) {
            offset++;
            return type & 0x0f;
        } else if (type == MessagePackTypes.MAP16) {
            offset += 3;
            return buffer.getShort(offset - 2) & 0xffff;
        } else if (type == MessagePackTypes.MAP32) {
            offset += 5;
            int u32 = buffer.getInt(offset - 4);
            if (u32 < 0) throw new MessagePackException("UINT32 overflow");
            return u32;
        } else {
            throw new MessagePackException("MAP", typeToName(type), offset);
        }
    }

    public @Nullable JsonNode getJson() {
        if (isNextNull()) return null;
        else if (isNextString()) return TextNode.valueOf(getString());
        else if (isNextInt()) return IntNode.valueOf(getInt());
        else if (isNextLong()) return LongNode.valueOf(getLong());
        else if (isNextFloat()) return FloatNode.valueOf(getFloat());
        else if (isNextDouble()) return DoubleNode.valueOf(getDouble());
        else if (isNextBoolean()) return BooleanNode.valueOf(getBoolean());
        else if (isNextArray()) return getJsonArray();
        else if (isNextMap()) return getJsonObject();
        else throw new MessagePackException("JSON", typeToName(buffer.getByte(offset)), offset);
    }

    private ArrayNode getJsonArray() {
        int length = getArrayLength();
        ArrayNode node = JsonNodeFactory.instance.arrayNode();
        for (int i = 0; i < length; i++) {
            node.add(getJson());
        }
        return node;
    }

    private ObjectNode getJsonObject() {
        int size = getMapSize();
        ObjectNode node = JsonNodeFactory.instance.objectNode();
        for (int i = 0; i < size; i++) {
            node.replace(
                    getString(),
                    getJson()
            );
        }
        return node;
    }
}

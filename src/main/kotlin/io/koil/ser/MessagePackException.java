package io.koil.ser;

public class MessagePackException extends RuntimeException {
    public MessagePackException() {
    }

    public MessagePackException(String message) {
        super(message);
    }

    public MessagePackException(String expected, String actual, int offset) {
        this("Expected " + expected + " but got " + actual + " (offset " + offset + ")");
    }
}

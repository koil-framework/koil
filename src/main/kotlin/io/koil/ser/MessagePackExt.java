package io.koil.ser;

import io.vertx.core.buffer.Buffer;

public record MessagePackExt(
        byte type,
        Buffer content
) {
}

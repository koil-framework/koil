package io.koil.ser;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.DoubleNode;
import com.fasterxml.jackson.databind.node.FloatNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.vertx.core.buffer.Buffer;

import java.nio.charset.StandardCharsets;
import java.util.Map;

public final class MessagePackEncoder {
    /* This class should only be used statically */
    private MessagePackEncoder() {
    }

    public static void putInt(Buffer buffer, int value) {
        if (value < -(1 << 5)) {
            if (value < -(1 << 15)) {
                buffer.appendByte(MessagePackTypes.INT32);
                buffer.appendInt(value);
            } else if (value < -(1 << 7)) {
                buffer.appendByte(MessagePackTypes.INT16);
                buffer.appendShort((short) value);
            } else {
                buffer.appendByte(MessagePackTypes.INT8);
                buffer.appendByte((byte) value);
            }
        } else if (value < (1 << 7)) {
            buffer.appendByte((byte) value);
        } else {
            if (value < (1 << 8)) {
                buffer.appendByte(MessagePackTypes.UINT8);
                buffer.appendByte((byte) value);
            } else if (value < (1 << 16)) {
                buffer.appendByte(MessagePackTypes.UINT16);
                buffer.appendShort((short) value);
            } else {
                buffer.appendByte(MessagePackTypes.UINT32);
                buffer.appendInt(value);
            }
        }
    }

    public static void putLong(Buffer buffer, long value) {
        if (value < -(1L << 5)) {
            if (value < -(1L << 15)) {
                if (value < -(1L << 31)) {
                    buffer.appendByte(MessagePackTypes.INT64);
                    buffer.appendLong(value);
                } else {
                    buffer.appendByte(MessagePackTypes.INT32);
                    buffer.appendInt((int) value);
                }
            } else {
                if (value < -(1 << 7)) {
                    buffer.appendByte(MessagePackTypes.INT16);
                    buffer.appendShort((short) value);
                } else {
                    buffer.appendByte(MessagePackTypes.INT8);
                    buffer.appendByte((byte) value);
                }
            }
        } else if (value < (1 << 7)) {
            // fixnum
            buffer.appendByte((byte) value);
        } else {
            if (value < (1L << 16)) {
                if (value < (1 << 8)) {
                    buffer.appendByte(MessagePackTypes.UINT8);
                    buffer.appendByte((byte) value);
                } else {
                    buffer.appendByte(MessagePackTypes.UINT16);
                    buffer.appendShort((short) value);
                }
            } else {
                if (value < (1L << 32)) {
                    buffer.appendByte(MessagePackTypes.UINT32);
                    buffer.appendInt((int) value);
                } else {
                    buffer.appendByte(MessagePackTypes.UINT64);
                    buffer.appendLong(value);
                }
            }
        }
    }

    public static int getLongSize(long value) {
        if (value < -(1L << 5)) {
            if (value < -(1L << 15)) {
                if (value < -(1L << 31)) return 9;
                else return 5;
            } else {
                if (value < -(1 << 7)) return 3;
                else return 2;
            }
        } else if (value < (1 << 7)) {
            // fixnum
            return 1;
        } else {
            if (value < (1L << 16)) {
                if (value < (1 << 8)) return 2;
                else return 3;
            } else {
                if (value < (1L << 32)) return 5;
                else return 9;
            }
        }
    }

    public static void putFloat(Buffer buffer, float value) {
        buffer.appendByte(MessagePackTypes.FLOAT32);
        buffer.appendFloat(value);
    }

    public static void putDouble(Buffer buffer, double value) {
        buffer.appendByte(MessagePackTypes.FLOAT64);
        buffer.appendDouble(value);
    }

    public static void putBoolean(Buffer buffer, boolean value) {
        if (value) buffer.appendByte(MessagePackTypes.TRUE);
        else buffer.appendByte(MessagePackTypes.FALSE);
    }

    public static void putString(Buffer buffer, String value) {
        if (value.isEmpty()) {
            putStringHeader(buffer, 0);
            return;
        }

        byte[] bytes = value.getBytes(StandardCharsets.UTF_8);
        putStringHeader(buffer, bytes.length);
        buffer.appendBytes(bytes);
    }

    public static void putRawString(Buffer buffer, byte[] value) {
        if (value.length == 0) {
            putStringHeader(buffer, 0);
            return;
        }

        putStringHeader(buffer, value.length);
        buffer.appendBytes(value);
    }

    private static void putStringHeader(Buffer buffer, int length) {
        if (length < (1 << 5)) {
            buffer.appendByte((byte) (MessagePackTypes.FIXSTR_PREFIX | length));
        } else if (length < (1 << 8)) {
            buffer.appendByte(MessagePackTypes.STR8);
            buffer.appendByte((byte) length);
        } else if (length < (1 << 16)) {
            buffer.appendByte(MessagePackTypes.STR16);
            buffer.appendShort((short) length);
        } else {
            buffer.appendByte(MessagePackTypes.STR32);
            buffer.appendInt(length);
        }
    }

    public static int getStringSize(int length) {
        // 1 byte type/length + string content
        if (length < (1 << 5)) return 1 + length;
            // 1 byte type + 1 bytes length + string content
        else if (length < (1 << 8)) return 2 + length;
            // 1 byte type + 2 bytes length + string content
        else if (length < (1 << 16)) return 3 + length;
            // 1 byte type + 4 bytes length + string content
        else return 5 + length;
    }

    public static void putNull(Buffer buffer) {
        buffer.appendByte(MessagePackTypes.NIL);
    }

    public static void putArrayHeader(Buffer buffer, int size) {
        if (size < 0) {
            throw new IllegalArgumentException("array size must be >= 0");
        }

        if (size < (1 << 4)) {
            buffer.appendByte((byte) (MessagePackTypes.FIXARRAY_PREFIX | size));
        } else if (size < (1 << 16)) {
            buffer.appendByte(MessagePackTypes.ARRAY16);
            buffer.appendShort((short) size);
        } else {
            buffer.appendByte(MessagePackTypes.ARRAY32);
            buffer.appendInt(size);
        }
    }

    public static void putBin(Buffer buffer, byte[] value) {
        if (value.length < (1 << 8)) {
            buffer.appendByte(MessagePackTypes.BIN8);
            buffer.appendByte((byte) value.length);
        } else if (value.length < (1 << 16)) {
            buffer.appendByte(MessagePackTypes.BIN16);
            buffer.appendShort((short) value.length);
        } else {
            buffer.appendByte(MessagePackTypes.BIN32);
            buffer.appendInt(value.length);
        }
        buffer.appendBytes(value);
    }

    public static void putBin(Buffer buffer, Buffer value) {
        int length = value.length();
        if (length < (1 << 8)) {
            buffer.appendByte(MessagePackTypes.BIN8);
            buffer.appendByte((byte) length);
        } else if (length < (1 << 16)) {
            buffer.appendByte(MessagePackTypes.BIN16);
            buffer.appendShort((short) length);
        } else {
            buffer.appendByte(MessagePackTypes.BIN32);
            buffer.appendInt(length);
        }
        buffer.appendBuffer(value);
    }

    public static void putExtHeader(Buffer buffer, byte extType, int length) {
        if (length < (1 << 8)) {
            if (length > 0 && (length & (length - 1)) == 0) { // check whether dataLen == 2^x
                if (length == 1) {
                    buffer.appendByte(MessagePackTypes.FIXEXT1);
                } else if (length == 2) {
                    buffer.appendByte(MessagePackTypes.FIXEXT2);
                } else if (length == 4) {
                    buffer.appendByte(MessagePackTypes.FIXEXT4);
                } else if (length == 8) {
                    buffer.appendByte(MessagePackTypes.FIXEXT8);
                } else if (length == 16) {
                    buffer.appendByte(MessagePackTypes.FIXEXT16);
                } else {
                    buffer.appendByte(MessagePackTypes.EXT8);
                    buffer.appendByte((byte) length);
                }
            } else {
                buffer.appendByte(MessagePackTypes.EXT8);
                buffer.appendByte((byte) length);
            }
        } else if (length < (1 << 16)) {
            buffer.appendByte(MessagePackTypes.EXT16);
            buffer.appendShort((short) length);
        } else {
            buffer.appendByte(MessagePackTypes.EXT32);
            buffer.appendInt(length);
        }

        buffer.appendByte(extType);
    }

    public static void putExt(Buffer buffer, byte extType, Buffer value) {
        putExtHeader(buffer, extType, value.length());
        buffer.appendBuffer(value);
    }

    public static int getMapHeaderSize(int size) {
        if (size < 0) throw new IllegalArgumentException("map size must be >= 0");

        if (size < (1 << 4)) return 1;
        else if (size < (1 << 16)) return 3;
        else return 5;
    }

    public static void putMapHeader(Buffer buffer, int size) {
        if (size < 0) throw new IllegalArgumentException("map size must be >= 0");

        if (size < (1 << 4)) {
            buffer.appendByte((byte) (MessagePackTypes.FIXMAP_PREFIX | size));
        } else if (size < (1 << 16)) {
            buffer.appendByte(MessagePackTypes.MAP16);
            buffer.appendShort((short) size);
        } else {
            buffer.appendByte(MessagePackTypes.MAP32);
            buffer.appendInt(size);
        }
    }

    public static void putJson(Buffer buffer, JsonNode json) {
        if (json == null || json.isNull() || json.isMissingNode()) putNull(buffer);
        else if (json.isTextual()) putString(buffer, json.textValue());
        else if (json.isIntegralNumber()) putLong(buffer, json.longValue());
        else if (json instanceof FloatNode) putFloat(buffer, json.floatValue());
        else if (json instanceof DoubleNode) putDouble(buffer, json.doubleValue());
        else if (json.isBoolean()) putBoolean(buffer, json.booleanValue());
        else if (json instanceof ArrayNode) putJsonArray(buffer, (ArrayNode) json);
        else if (json instanceof ObjectNode) putJsonObject(buffer, (ObjectNode) json);
        else throw new MessagePackException("Unhandled json type " + json.getNodeType().name());
    }

    private static void putJsonArray(Buffer buffer, ArrayNode json) {
        putArrayHeader(buffer, json.size());
        for (int i = 0; i < json.size(); i++) {
            putJson(buffer, json.get(i));
        }
    }

    private static void putJsonObject(Buffer buffer, ObjectNode json) {
        putMapHeader(buffer, json.size());
        for (Map.Entry<String, JsonNode> property : json.properties()) {
            putString(buffer, property.getKey());
            putJson(buffer, property.getValue());
        }
    }
}

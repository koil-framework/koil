package io.koil.di

/**
 * Allows a registered instance to register dependencies via callback
 *
 * WARNING: will only be used when this instance scope is SINGLETON
 * or an instance is provided via registerInstance
 */
interface DiProvider {
    fun provide(container: DiContainer)
}

package io.koil.di

import io.github.classgraph.AnnotationEnumValue
import io.github.classgraph.ClassGraph
import java.lang.reflect.Parameter


// Inspiration for the future: https://github.com/InsertKoinIO/koin/tree/main

// TODO returnFirstIfNameNotFound: returns the first instance regardless of its name when no instance is found for the requested name

private data class DiEntryKey(
    val cls: Class<out Any>,
    val name: String?,
)

private interface DiEntry {
    val cls: Class<out Any>
    val name: String?

    fun matches(key: DiEntryKey): Boolean {
        return cls.isAssignableFrom(key.cls) && name == key.name
    }
}

private data class DiDefEntry(
    override val cls: Class<out Any>,
    override val name: String?,
    val scope: DiScope,
    val ctor: (container: DiContainer) -> Any,
    var instance: DiInstanceEntry?,
) : DiEntry

private data class DiInstanceEntry(
    override val cls: Class<out Any>,
    override val name: String?,
    val instance: Any,
    /** Indicates this instance does not have a backing definition (ie. registered via registerInstance, cannot be recreated) */
    val noDef: Boolean = false,
) : DiEntry

class DiContainer(
    private val opts: DiOpts = DiOpts(),
) {
    private val instances = mutableListOf<DiInstanceEntry>()
    private val entries = mutableListOf<DiDefEntry>()
    private var parentContainer: DiContainer? = null

    fun registerModule(module: DiModule): DiContainer {
        module.register(this)
        return this
    }

    inline fun <reified T : Any> register(
        noinline ctor: (container: DiContainer) -> T,
        scope: DiScope = DiScope.SINGLETON,
        name: String? = null,
        preInit: Boolean = false,
    ): DiContainer = register(T::class.java, ctor, scope, name, preInit)

    fun register(
        cls: Class<out Any>,
        ctor: (container: DiContainer) -> Any,
        scope: DiScope = DiScope.SINGLETON,
        name: String? = null,
        preInit: Boolean = false,
    ): DiContainer = apply {
        val entry = DiDefEntry(cls, name, scope, ctor, null)
        entries.add(entry)
        if (preInit) createFromDef(this, cls, name, entry)
    }

    inline fun <reified T : Any> register(
        scope: DiScope = DiScope.SINGLETON,
        name: String? = null,
        preInit: Boolean = false
    ): DiContainer =
        register(T::class.java, scope, name, preInit)

    fun register(
        cls: Class<out Any>,
        scope: DiScope = DiScope.SINGLETON,
        name: String? = null,
        preInit: Boolean = false,
    ): DiContainer = apply {
        val mainConstructor = cls.constructors[0]
        val def = DiDefEntry(
            cls,
            name,
            scope,
            { container ->
                mainConstructor.newInstance(*mainConstructor.parameters.map {
                    resolveControllerArg(container, it)
                }.toTypedArray())
            },
            null
        )
        entries.add(def)

        if (preInit) createFromDef(this, cls, name, def)
    }

    fun autoRegister(vararg packageNames: String): DiContainer = apply {
        ClassGraph()
            .enableClassInfo()
            .enableAnnotationInfo()
            .acceptPackages(*packageNames)
            .scan()
            .use {
                for (classInfo in it.getClassesWithAnnotation(DiRegister::class.java)) {
                    val annotation = classInfo.getAnnotationInfo(DiRegister::class.java)
                    val values = annotation.parameterValues

                    val scope =
                        (values.get("scope").value as AnnotationEnumValue).loadClassAndReturnEnumValue() as DiScope
                    val name = values.get("value").value as String

                    register(classInfo.loadClass(), scope, name.ifBlank { null })
                }
            }
    }

    private fun resolveControllerArg(container: DiContainer, param: Parameter): Any {
        val all = param.getAnnotation(All::class.java)
        if (all != null) return container.getAll(all.value.java)

        val allNamed = param.getAnnotation(AllNamed::class.java)
        if (allNamed != null) return container.getAllNamed(allNamed.value.java)

        val named = param.getAnnotation(Named::class.java)
        return container.getInstance(param.type, named?.value)
    }

    /** This is an alias for provide, for backwards compatibility purposes */
    fun registerInstance(instance: Any, name: String? = null): DiContainer = provide(instance, name)

    fun provide(instance: Any, name: String? = null): DiContainer = apply {
        instances.add(DiInstanceEntry(instance::class.java, name, instance, true))

        if (instance is DiProvider) instance.provide(this)
    }

    /**
     * Deletes any created, non-instance entries (i.e. entries registered using registerInstance)
     *
     * Use this when you want to refresh dependencies.
     *
     * @return the number of destroyed instances
     */
    fun destroy(cls: Class<out Any>, name: String? = null): Int {
        val key = DiEntryKey(cls, name)
        var destroyed = 0
        val it = instances.iterator()
        while (it.hasNext()) {
            val instance = it.next()
            if (instance.matches(key)) {
                destroyed++
                it.remove()
            }
        }
        return destroyed
    }

    /**
     * Reloads an already registered instance (through registerInstance).
     * @return false if the instance could not be found
     */
    fun reloadInstance(cls: Class<out Any>, name: String? = null, newInstance: Any): Boolean {
        val key = DiEntryKey(cls, name)
        for (i in instances.indices) {
            val instance = instances[i]
            if (instance.matches(key) && instance.noDef) {
                instances[i] = instance.copy(instance = newInstance)
                return true
            }
        }

        return false
    }

    private fun createFromDef(innerContainer: DiContainer, cls: Class<out Any>, name: String?, def: DiDefEntry): Any {
        val instance = try {
            def.ctor(innerContainer)
        } catch (e: DiFailedException) {
            throw DiFailedException(cls, name, e)
        }
        if (def.scope == DiScope.SINGLETON) {
            def.instance = DiInstanceEntry(def.cls, def.name, instance)
            instances.add(def.instance!!)
            if (instance is DiProvider) instance.provide(this)
        }
        return instance
    }

    inline fun <reified V> get(name: String? = null): V = getInstance(V::class.java, name)
    inline fun <reified V> getNullable(name: String? = null): V? = getInstanceNullable(V::class.java, name)

    fun <V> getInstance(cls: Class<out V>, name: String? = null): V =
        getInstanceImpl(cls, name) ?: throw DiFailedException(cls, name)

    fun <V> getInstanceNullable(cls: Class<out V>, name: String? = null): V? = getInstanceImpl(cls, name)

    private fun <V> getInstanceImpl(cls: Class<out V>, name: String? = null, innerContainer: DiContainer? = null): V? {
        if (cls.isAssignableFrom(DiContainer::class.java)) return cls.cast(this)

        val inst = instances.firstOrNull { cls.isAssignableFrom(it.cls) && name == it.name }
        if (inst != null) return cls.cast(inst.instance)

        val def = entries.firstOrNull { cls.isAssignableFrom(it.cls) && name == it.name }
        if (def == null) return parentContainer?.getInstanceImpl(cls, name, innerContainer ?: this)

        return cls.cast(createFromDef(innerContainer ?: this, cls, name, def))
    }

    inline fun <reified V> getAllNamed(): Map<String, V> = getAllNamed(V::class.java)

    inline fun <reified V> getAll(names: Set<String>? = null): Set<V> = getAll(V::class.java, names)

    fun <V> getAll(cls: Class<out V>, names: Set<String>? = null): Set<V> = getAllImpl(cls, names)

    fun <V> getAllNamed(cls: Class<out V>): Map<String, V> = getAllNamedImpl(cls)

    fun <V> getAllImpl(cls: Class<out V>, names: Set<String>? = null, innerContainer: DiContainer? = null): Set<V> {
        val result = mutableSetOf<V>()
        for (inst in instances) {
            if (cls.isAssignableFrom(inst.cls) && (names == null || names.contains(inst.name))) {
                result.add(cls.cast(inst.instance))
            }
        }

        for (def in entries) {
            if (def.instance == null && cls.isAssignableFrom(def.cls) && (names == null || names.contains(def.name))) {
                result.add(cls.cast(createFromDef(innerContainer ?: this, cls, def.name, def)))
            }
        }

        parentContainer?.let { result.addAll(it.getAllImpl(cls, names, innerContainer ?: this)) }
        return result
    }

    fun <V> getAllNamedImpl(cls: Class<out V>, innerContainer: DiContainer? = null): Map<String, V> {
        val result = mutableMapOf<String, V>()
        for (inst in instances) {
            if (cls.isAssignableFrom(inst.cls) && inst.name != null) {
                result[inst.name] = cls.cast(inst.instance)
            }
        }

        for (def in entries) {
            if (def.instance == null && cls.isAssignableFrom(def.cls) && def.name != null) {
                result[def.name] = cls.cast(createFromDef(innerContainer ?: this, cls, def.name, def))
            }
        }

        parentContainer?.let { result.putAll(it.getAllNamedImpl(cls, innerContainer ?: this)) }
        return result
    }

    private fun copy(): DiContainer {
        val container = DiContainer(opts)
        container.parentContainer = parentContainer?.copy()
        container.instances.addAll(instances)
        container.entries.addAll(entries)
        return container
    }

    private fun setParent(parent: DiContainer): DiContainer = apply { parentContainer = parent }

    fun createChild(): DiContainer {
        return DiContainer(opts)
            .setParent(if (opts.preventParentScopePollution) copy() else this)
    }
}

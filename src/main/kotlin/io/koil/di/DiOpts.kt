package io.koil.di

data class DiOpts(
    val preventParentScopePollution: Boolean = true
)

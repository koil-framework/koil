package io.koil.di

fun interface DiModule {
    fun register(container: DiContainer)
}

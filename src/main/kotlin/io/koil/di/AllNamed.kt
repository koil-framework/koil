package io.koil.di

import kotlin.reflect.KClass

annotation class AllNamed(val value: KClass<*>)

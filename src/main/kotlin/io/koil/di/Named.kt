package io.koil.di

annotation class Named(val value: String)

package io.koil.di

enum class DiScope {
    SINGLETON,
    INSTANCE,
}

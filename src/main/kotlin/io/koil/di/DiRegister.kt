package io.koil.di

annotation class DiRegister(
    val value: String = "",
    val scope: DiScope = DiScope.SINGLETON,
)

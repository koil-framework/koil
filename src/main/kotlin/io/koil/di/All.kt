package io.koil.di

import kotlin.reflect.KClass

annotation class All(val value: KClass<*>)

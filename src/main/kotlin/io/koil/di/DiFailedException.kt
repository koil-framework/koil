package io.koil.di

class DiFailedException(val cls: Class<out Any>, val name: String?, cause: DiFailedException? = null) :
    RuntimeException(
        "Failed to get instance of $cls${if (name == null) "" else " (named $name)"}: ${if (cause == null) "no instance or definition found" else "failed to get dependency"}",
        cause
    )

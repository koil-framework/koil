package io.koil.db

interface RepositoryDefinition {
    fun tables(): List<TableSchema>
}

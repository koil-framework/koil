package io.koil.db

enum class SqlJoinType {
    INNER,
    OUTER,
    LEFT,
    RIGHT,
}

data class SqlJoin(
    val table: String,
    val type: SqlJoinType,
    val conditions: List<String>,
)

private fun appendConditionList(query: StringBuilder, conditions: List<String>) {
    var first = true
    for (condition in conditions) {
        if (first) first = false
        else query.append(" AND ")
        query.append(condition)
    }
}

interface ConditionalSqlQuery {
    fun where(vararg conditions: String): ConditionalSqlQuery
    fun where(conditions: Collection<String>): ConditionalSqlQuery
}

class SelectSqlQuery : ConditionalSqlQuery {
    private val withs = mutableMapOf<String, SelectSqlQuery>()
    private val selectedColumns = mutableListOf<String>()
    private lateinit var table: String
    private val joins = mutableListOf<SqlJoin>()
    private val conditions = mutableListOf<String>()
    private val orderBy = mutableListOf<String>()
    private val groupBy = mutableListOf<String>()
    private var offset: Int? = null
    private var limit: Int? = null

    fun with(alias: String, query: SelectSqlQuery): SelectSqlQuery = apply { withs[alias] = query }
    fun select(vararg columns: String): SelectSqlQuery = apply { selectedColumns.addAll(columns) }
    fun from(table: String): SelectSqlQuery = apply { this.table = table }
    fun join(type: SqlJoinType, table: String, vararg conditions: String): SelectSqlQuery =
        apply { joins.add(SqlJoin(table, type, conditions.toList())) }

    override fun where(vararg conditions: String): SelectSqlQuery = apply { this.conditions.addAll(conditions) }
    override fun where(conditions: Collection<String>): SelectSqlQuery = apply { this.conditions.addAll(conditions) }
    fun orderBy(vararg orderBy: String): SelectSqlQuery = apply { this.orderBy.addAll(orderBy) }
    fun orderBy(orderBy: Collection<String>): SelectSqlQuery = apply { this.orderBy.addAll(orderBy) }
    fun groupBy(vararg groupBy: String): SelectSqlQuery = apply { this.groupBy.addAll(groupBy) }
    fun groupBy(groupBy: Collection<String>): SelectSqlQuery = apply { this.groupBy.addAll(groupBy) }
    fun offset(offset: Int): SelectSqlQuery = apply { this.offset = offset }
    fun limit(limit: Int): SelectSqlQuery = apply { this.limit = limit }

    fun build(): String {
        val query = StringBuilder()

        var first = true
        for ((alias, subQuery) in withs) {
            if (first) {
                query.append("WITH ")
                first = false
            } else query.append(", ")

            query.append(alias).append(" AS (").append(subQuery.build()).append(")")
        }
        if (withs.isNotEmpty()) query.append(" ")

        query.append("SELECT ")
            .append(selectedColumns.joinToString(", "))
            .append(" FROM ")
            .append(table)

        for (join in joins) {
            when (join.type) {
                SqlJoinType.INNER -> query.append(" INNER JOIN ")
                SqlJoinType.OUTER -> query.append(" OUTER JOIN ")
                SqlJoinType.LEFT -> query.append(" LEFT JOIN ")
                SqlJoinType.RIGHT -> query.append(" RIGHT JOIN ")
            }
            query.append(join.table)
                .append(" ON ")
            appendConditionList(query, join.conditions)
        }

        if (conditions.isNotEmpty()) {
            query.append(" WHERE ")
            appendConditionList(query, conditions)
        }

        if (groupBy.isNotEmpty()) {
            query.append(" GROUP BY ")
                .append(groupBy.joinToString(", "))
        }

        if (orderBy.isNotEmpty()) {
            query.append(" ORDER BY ")
                .append(orderBy.joinToString(", "))
        }

        if (offset != null) query.append(" OFFSET ").append(offset)
        if (limit != null) query.append(" LIMIT ").append(limit)

        return query.toString()
    }
}

class UpdateSqlQuery(private val table: String) : ConditionalSqlQuery {
    private val updatedColumns = mutableListOf<String>()
    private val conditions = mutableListOf<String>()

    fun set(vararg columns: String): UpdateSqlQuery = apply { updatedColumns.addAll(columns) }
    fun set(columns: Collection<String>): UpdateSqlQuery = apply { updatedColumns.addAll(columns) }
    fun set(column: String, value: String): UpdateSqlQuery = apply { updatedColumns.add("$column = $value") }
    fun set(column: String, position: Int): UpdateSqlQuery = apply { updatedColumns.add("$column = \$$position") }
    override fun where(vararg conditions: String): UpdateSqlQuery = apply { this.conditions.addAll(conditions) }
    override fun where(conditions: Collection<String>): UpdateSqlQuery = apply { this.conditions.addAll(conditions) }

    fun build(): String {
        val query = StringBuilder("UPDATE ")
            .append(table)
            .append(" SET ")
            .append(updatedColumns.joinToString(", "))

        if (conditions.isNotEmpty()) {
            query.append(" WHERE ")
            appendConditionList(query, conditions)
        }

        return query.toString()
    }
}

class InsertSqlQuery(private val table: String) {
    private val columns = mutableListOf<String>()
    private val values = mutableListOf<String>()

    fun columns(vararg columns: String): InsertSqlQuery = apply { this.columns.addAll(columns) }
    fun columns(columns: Collection<String>): InsertSqlQuery = apply { this.columns.addAll(columns) }
    fun values(vararg values: String): InsertSqlQuery = apply { this.values.addAll(values) }
    fun values(values: Collection<String>): InsertSqlQuery = apply { this.values.addAll(values) }

    fun build(): String {
        return StringBuilder("INSERT INTO ")
            .append(table)
            .append(columns.joinToString(", ", prefix = "(", postfix = ")"))
            .append(" VALUES ")
            .append(values.joinToString(", ", prefix = "(", postfix = ")"))
            .toString()
    }
}

class DeleteSqlQuery(private val table: String) : ConditionalSqlQuery {
    private val conditions = mutableListOf<String>()

    override fun where(vararg conditions: String): DeleteSqlQuery = apply { this.conditions.addAll(conditions) }
    override fun where(conditions: Collection<String>): DeleteSqlQuery = apply { this.conditions.addAll(conditions) }

    fun build(): String {
        val query = StringBuilder("DELETE FROM ")
            .append(table)
        if (conditions.isNotEmpty()) {
            query.append(" WHERE ")
            appendConditionList(query, conditions)
        }
        return query.toString()
    }
}

package io.koil.db

// TODO Manage multiple database dialects for a same schema

enum class TableColumnRole {
    PRIMARY_KEY,
    CREATION_TIMESTAMP,
    LAST_MODIFICATION_TIMESTAMP,
    DELETION_FLAG,
}

enum class TableColumnType {
    UUID,
    STRING,
    BOOLEAN,
    FLOAT,
    DOUBLE,
    INT,
    LONG,
    INSTANT,
    OFFSET_DATETIME,
    BINARY,
    ENUM,
    ARRAY,
    JSON,
}

data class TableColumn(
    /** The name of the column */
    val name: String,
    /** The name of the property in the code */
    val propertyName: String,
    /** A description of the column */
    val description: String?,
    /** The SQL type of the column, in the target database dialect */
    val type: String,
    /** The type of the property in the code */
    val propertyType: TableColumnType,
    /** The type of the array item in the code if this property is an array */
    val propertyArrayItemType: TableColumnType?,
    /** If the property is an enum, the class of the enum */
    val enumClass: Class<*>?,
    /** True to make the column nullable */
    val nullable: Boolean,
    /** True to allow updates on this column (through the default update methods) */
    val editable: Boolean,
    /** The special role of the column, if any */
    val role: TableColumnRole?,
) {
    companion object {
        fun create(name: String, propertyName: String = name): Builder = Builder(name, propertyName)
    }

    class Builder(private val name: String, private val propertyName: String) {
        private var description: String? = null
        private lateinit var type: String
        private lateinit var propertyType: TableColumnType
        private var propertyArrayItemType: TableColumnType? = null
        private var enumClass: Class<*>? = null
        private var nullable: Boolean = false
        private var editable: Boolean = false
        private var role: TableColumnRole? = null

        fun description(description: String?): Builder = apply { this.description = description }
        fun type(type: String, propertyType: TableColumnType): Builder =
            apply { this.type = type; this.propertyType = propertyType }

        fun array(type: String, arrayItemType: TableColumnType): Builder = apply {
            this.type = type
            this.propertyType = TableColumnType.ARRAY
            this.propertyArrayItemType = arrayItemType
        }

        fun enumClass(enumClass: Class<out Enum<*>>?): Builder = apply { this.enumClass = enumClass }
        fun nullable(nullable: Boolean = true): Builder = apply { this.nullable = nullable }
        fun editable(editable: Boolean = true): Builder = apply { this.editable = editable }
        fun role(role: TableColumnRole?): Builder = apply { this.role = role }

        fun build(): TableColumn = TableColumn(
            name,
            propertyName,
            description,
            type,
            propertyType,
            propertyArrayItemType,
            enumClass,
            nullable,
            editable,
            role
        )
    }
}

enum class TableRelationType {
    /** The related data will always be fetched with the main resource */
    INTERNAL,

    /** The related data can optionally be fetched using extensions */
    EXTENSION,

    /** The relation is only here for documentation purposes. Code allowing to fetch data for this relation will not be generated. */
    EXTERNAL,
}

data class TableRelationColumn(
    /** The name of the column for the left table */
    val leftColumnName: String,
    /** The name of the column for the right table */
    val rightColumnName: String,
)

data class TableRelation(
    /** The name of the right table */
    val rightTableName: String,
    /** The name of the variable holding the result in the left table resource, if this is an internal relation */
    val variableName: String?,
    /** Indicates this relation will fetch many results in the right table */
    val oneToMany: Boolean,
    /** How should this relation be handled */
    val relationType: TableRelationType,
    /** True to make the result optional (nullable). Will always be true for one-to-many relations */
    val optional: Boolean,
    /** The list of columns allowing to match rows from the left table in the right table */
    val onColumns: List<TableRelationColumn>,
) {
    companion object {
        fun internal(rightTableName: String, variableName: String): Builder =
            Builder(TableRelationType.INTERNAL, rightTableName)
                .variableName(variableName)

        fun extension(rightTableName: String): Builder =
            Builder(TableRelationType.EXTENSION, rightTableName)

        fun external(rightTableName: String): Builder =
            Builder(TableRelationType.EXTERNAL, rightTableName)
    }

    class Builder internal constructor(
        private val relationType: TableRelationType,
        private val rightTableName: String
    ) {
        private var oneToMany: Boolean = false
        private var optional: Boolean = false
        private val onColumns = mutableListOf<TableRelationColumn>()
        private var variableName: String? = null

        fun variableName(variableName: String?): Builder = apply { this.variableName = variableName }
        fun oneToMany(oneToMany: Boolean = true): Builder = apply { this.oneToMany = oneToMany }
        fun optional(optional: Boolean = true): Builder = apply { this.optional = optional }
        fun column(columns: TableRelationColumn): Builder = apply { onColumns.add(columns) }
        fun column(leftColumnName: String, rightColumnName: String): Builder =
            apply { onColumns.add(TableRelationColumn(leftColumnName, rightColumnName)) }

        fun build(): TableRelation = TableRelation(
            rightTableName,
            variableName,
            oneToMany,
            relationType,
            optional,
            onColumns,
        )
    }
}

data class TableSchema(
    /** The name of the table */
    val name: String,
    /** The name of the object in the code */
    val objectName: String,
    /** If the database supports schemas, and the table is in a schema, the name of the schema */
    val schema: String?,
    /** A description of the table */
    val description: String?,
    /** The columns of the table */
    val columns: List<TableColumn>,
) {
    companion object {
        fun create(name: String, objectName: String = name): Builder = Builder(name, objectName)
    }

    val tableIdentifier: String get() = if (schema.isNullOrBlank()) name else "$schema.$name"

    fun validate(): Boolean {
        return true
    }

    class Builder(private val name: String, private val objectName: String) {
        private var schema: String? = null
        private var description: String? = null
        private val columns = mutableListOf<TableColumn>()

        fun schema(schema: String?): Builder = apply { this.schema = schema }
        fun description(description: String?): Builder = apply { this.description = description }

        fun column(column: TableColumn): Builder = apply { this.columns.add(column) }
        fun column(column: TableColumn.Builder): Builder = apply { this.columns.add(column.build()) }

        fun build(): TableSchema = TableSchema(name, objectName, schema, description, columns)
    }
}

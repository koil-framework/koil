package io.koil.web.verticles

import com.fasterxml.jackson.databind.json.JsonMapper
import io.koil.core.config.HttpServerConfig
import io.koil.di.DiContainer
import io.koil.web.Problem
import io.koil.web.ProblemException
import io.koil.web.ValidationException
import io.koil.web.end
import io.koil.web.server.*
import io.vertx.core.Promise
import io.vertx.core.http.HttpMethod
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.coAwait
import org.slf4j.LoggerFactory

class HttpServerVerticle(
    private val container: DiContainer,
    private val httpServerConfigName: String? = null,
) : CoroutineVerticle() {
    companion object {
        private val logger = LoggerFactory.getLogger(HttpServerVerticle::class.java)
    }

    private val gracefulShutdownHandler = GracefulShutdownHandler()

    override suspend fun start() {
        val childContainer = container.createChild()
            .registerInstance(vertx)
            .register({ AuthorizationProcessor(it.getAll()) })

        val serverConfig = childContainer.get<HttpServerConfig>(httpServerConfigName)
        val routerFactories = childContainer.getAll<RouterFactory>()
        val jsonMapper = childContainer.get<JsonMapper>()

        val router = Router.router(vertx)
            .registerErrorHandlers(jsonMapper)
            .withAccessControl(serverConfig)

        val mainRoute = router.route()
            .handler(gracefulShutdownHandler)
            .handler(RequestContextHandler(jsonMapper))
            .failureHandler {
                routingExceptionHandler(it, it.failure()) { logger.error("Unhandled exception", it.failure()) }
            }

        val authHandler = childContainer.getNullable<AuthenticationHandler>()
        if (authHandler == null) {
            logger.warn("AuthenticationHandler not found")
        } else {
            mainRoute.handler(authHandler)
        }

        for (routerFactory in routerFactories) {
            routerFactory.register(childContainer, router)
        }

        val simpleControllers = childContainer.getAll<SimpleController>()
        for (simpleController in simpleControllers) {
            simpleController.register(router)
        }

        vertx.createHttpServer(serverConfig.createOptions())
            .requestHandler(router)
            .listen()
            .coAwait()
        logger.info("Started HTTP server on ${serverConfig.host ?: ""}:${serverConfig.port}")
    }

    override suspend fun stop() {
        gracefulShutdownHandler.shutdown = true
        if (gracefulShutdownHandler.requestsInFlight == 0) {
            return
        }

        val promise = Promise.promise<Unit>()
        gracefulShutdownHandler.promise = promise
        logger.info("Shutting down HTTP server, {} requests in-flight", gracefulShutdownHandler.requestsInFlight)

        promise.future().coAwait()
    }
}

// TODO Handle those cases: https://vertx.io/docs/vertx-web/java/#_route_match_failures
// We need to pass the json mapper here because it might not be set by routes when we trigger those handlers
private fun Router.registerErrorHandlers(jsonMapper: JsonMapper): Router =
    errorHandler(404) {
        it.end(Problem.pathNotFound(it.request().path(), it.request().method().name()).build(), jsonMapper)
    }.errorHandler(405) {
        it.end(Problem.pathNotFound(it.request().path(), it.request().method().name()).build(), jsonMapper)
    }

private fun Router.withAccessControl(config: HttpServerConfig): Router {
    route("/*").handler {
        it.response().putHeader("Access-Control-Allow-Origin", config.allowOrigin)
            .putHeader("Access-Control-Allow-Headers", config.allowHeaders)
            .putHeader("Access-Control-Allow-Methods", config.allowMethods)

        if (it.request().method() == HttpMethod.OPTIONS) {
            it.response().end()
        } else {
            it.next()
        }
    }
    return this
}

private fun routingExceptionHandler(ctx: RoutingContext, e: Throwable, onInternalError: () -> Unit) {
    ctx.end(
        when (e) {
            is ValidationException -> Problem.badRequest(e.errors).build()
            is ProblemException -> e.problem
            is NotImplementedError -> Problem.notImplemented().build()
            else -> {
                onInternalError.invoke()
                Problem.internalError().build()
            }
        }
    )
}

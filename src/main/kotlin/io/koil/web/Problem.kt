package io.koil.web

import com.fasterxml.jackson.annotation.JsonAnyGetter
import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.databind.json.JsonMapper
import io.koil.core.Failable
import io.koil.core.toBase62String
import io.koil.core.writeValueAsBuffer
import io.koil.schemas.JsonSchema
import io.koil.web.server.getJsonMapper
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.Future
import io.vertx.ext.web.RoutingContext
import java.util.*

object ProblemTypes {
    const val BASE_URL = "https://specs.koil.io/problems"

    const val INTERNAL_ERROR = "$BASE_URL/internal-error"
    const val NOT_FOUND = "$BASE_URL/not-found"
    const val PATH_NOT_FOUND = "$BASE_URL/path-not-found"
    const val AUTH_REQUIRED = "$BASE_URL/authentication-required"
    const val SIGN_IN_FAILED = "$BASE_URL/sign-in-failed"
    const val ACCOUNT_DISABLED = "$BASE_URL/account-disabled"
    const val INVALID_ACCESS_TOKEN = "$BASE_URL/invalid-access-token"
    const val ACCESS_TOKEN_EXPIRED = "$BASE_URL/access-token-expired"
    const val ACCESS_TOKEN_REVOKED = "$BASE_URL/access-token-revoked"
    const val VALIDATION_FAILED = "$BASE_URL/validation-failed"
    const val NOT_ENOUGH_PERMISSIONS = "$BASE_URL/not-enough-permissions"
    const val NO_PERMISSIONS = "$BASE_URL/no-permissions"

    const val CONFLICT = "$BASE_URL/conflict"

    const val NOT_IMPLEMENTED = "$BASE_URL/not-implemented"
}

class ProblemException(val problem: Problem) : Exception(problem.title, null, true, false)

data class ValidationError(val field: String, val message: String)

class ValidationException(val errors: List<ValidationError>) : Exception("Validation failed", null, true, false)

fun RoutingContext.end(problem: Problem, jsonMapper: JsonMapper = getJsonMapper()) {
    if (response().ended()) return

    response().statusCode = problem.status
    response().putHeader("Content-Type", "application/problem+json")
    end(jsonMapper.writeValueAsBuffer(problem))
}

data class Problem(
    val type: String,
    val title: String,
    val status: Int,
    val detail: String?,
    val instance: String?,
    @get:JsonAnyGetter
    @JsonAnySetter
    val extensions: Map<String, Any> = mapOf(),
) {
    class Builder(
        private val type: String,
        private val title: String,
        private val status: Int
    ) {
        private var detail: String? = null
        private var instance: String? = null
        private val extensions: MutableMap<String, Any> = mutableMapOf()

        constructor(type: String, status: HttpResponseStatus) : this(type, status.reasonPhrase(), status.code())

        fun withDetail(detail: String): Builder {
            this.detail = detail
            return this
        }

        fun withInstance(instance: String): Builder {
            this.instance = instance
            return this
        }

        fun with(key: String, value: Any): Builder {
            extensions[key] = value
            return this
        }

        fun with(vararg extensions: Pair<String, Any>): Builder {
            for (pair in extensions) {
                this.extensions[pair.first] = pair.second
            }
            return this
        }

        fun build(): Problem {
            return Problem(type, title, status, detail, instance, extensions)
        }

        fun buildException(): ProblemException {
            return ProblemException(build())
        }

        fun <T> buildFailed(): Future<T> {
            return Future.failedFuture(buildException())
        }

        fun <T> buildFailable(): Failable<T, Problem> {
            return Failable.Failed(build())
        }

        fun <T> buildFFuture(): Future<Failable<T, Problem>> {
            return Future.succeededFuture(buildFailable())
        }
    }

    fun toException(): ProblemException {
        return ProblemException(this)
    }

    fun <T> toFailable(): Failable<T, Problem> {
        return Failable.Failed(this)
    }

    fun <T> toFailed(): Future<T> {
        return Future.failedFuture(toException())
    }

    fun <T> toFFuture(): Future<Failable<T, Problem>> {
        return Future.succeededFuture(toFailable())
    }

    companion object {
        fun internalError(): Builder {
            return Builder(ProblemTypes.INTERNAL_ERROR, HttpResponseStatus.INTERNAL_SERVER_ERROR)
                .withDetail("An internal error happened, please try again later")
        }

        fun notFound(resource: String, id: String): Builder {
            return Builder(ProblemTypes.NOT_FOUND, "Resource not found", 404)
                .withInstance("/$resource/$id")
        }

        fun notFound(resource: String, id: UUID): Builder = notFound(resource, id.toBase62String())

        fun pathNotFound(path: String, method: String): Builder {
            return Builder(ProblemTypes.PATH_NOT_FOUND, "Path not found", 404)
                .withInstance(path)
                .withDetail("The path you are trying to reach does not exist")
                .with("method", method)
        }

        fun authenticationRequired(): Builder {
            return Builder(ProblemTypes.AUTH_REQUIRED, "Authentication required", 401)
                .withDetail("Authentication is required to access this resource. The JWT token must be passed in the Authorization header using the Bearer type.")
        }

        fun signInFailed(): Builder {
            return Builder(ProblemTypes.SIGN_IN_FAILED, "Sign-in failed", 401)
                .withDetail("The provided credentials are not valid.")
        }

        fun accountDisabled(): Builder {
            return Builder(ProblemTypes.ACCOUNT_DISABLED, "Account is disabled", 403)
                .withDetail("The account you are trying to log in is disabled, please contact your tenant administrator.")
        }

        fun invalidAccessToken(message: String? = null): Builder {
            return Builder(ProblemTypes.INVALID_ACCESS_TOKEN, "Invalid access token", 401)
                .withDetail(message ?: "The provided access token is invalid.")
        }

        fun accessTokenExpired(): Builder {
            return Builder(ProblemTypes.ACCESS_TOKEN_EXPIRED, "Access token expired", 401)
                .withDetail("The provided access token is expired, re-authentication is required.")
        }

        fun accessTokenRevoked(): Builder {
            return Builder(ProblemTypes.ACCESS_TOKEN_REVOKED, "Access token revoked", 401)
                .withDetail("The provided access token has been revoked, re-authentication is required.")
        }

        fun badRequest(errors: List<ValidationError>): Builder {
            return Builder(ProblemTypes.VALIDATION_FAILED, "Validation failed", 400)
                .withDetail("Request validation failed due to invalid parameters, please check your request.")
                .with("errors", errors)
        }

        fun badRequest(field: String, message: String): Builder {
            return badRequest(listOf(ValidationError(field, message)))
        }

        fun notEnoughPermissions(key: String, required: String, actual: String): Builder {
            return Builder(ProblemTypes.NOT_ENOUGH_PERMISSIONS, "Not enough permissions", 403)
                .withDetail("The required permission level for $key is '$required', the currently logged-in user level is '$actual'")
        }

        fun noPermissions(): Builder {
            return Builder(ProblemTypes.NO_PERMISSIONS, "No permissions", 403)
                .withDetail("Permissions are required to access this resource")
        }

        fun notImplemented(): Builder {
            return Builder(ProblemTypes.NOT_IMPLEMENTED, "Not implemented", 501)
                .withDetail("This endpoint is not implemented on this version")
        }

        fun conflict(title: String, message: String): Builder {
            return Builder(ProblemTypes.CONFLICT, title, 409)
                .withDetail(message)
        }

        val schema = JsonSchema.obj("Problem")
            .description("An error object")
            .implIdentifier(Problem::class.qualifiedName!!)
            .property(
                "uri",
                JsonSchema.string()
                    .format("uri")
                    .description("The type of problem")
                //.example(ProblemTypes.INTERNAL_ERROR)
            ).property(
                "title",
                JsonSchema.string()
                    .description("The textual representation of the HTTP status code")
                //.example("Internal Server Error")
            ).property(
                "status",
                JsonSchema.int()
                    .description("The HTTP status code")
                    .minimum(100)
                    .maximum(599),
                //.example(500)
                true,
            ).property(
                "detail",
                JsonSchema.string()
                    .description("A human-readable error description"),
                //.example("An internal error happened, please try again later")
                true,
            ).property(
                "instance",
                JsonSchema.string()
                    .description("The path or URI responsible for the problem"),
                //.example("/v1/test")
                true,
            )
            .additionalProperties(JsonSchema.string(), JsonSchema.string())
            .build()
    }
}

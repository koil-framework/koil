package io.koil.web.auth

import io.vertx.ext.web.RoutingContext

fun RoutingContext.setPrincipal(principal: Principal) = put("koil_principal", principal)
fun RoutingContext.getPrincipal(): Principal? = this["koil_principal"]

interface Principal {
    val accountId: String
}

/**
 * This trait implies the principal was authenticated using an access token
 */
interface AccessTokenTrait {
    val accessToken: String
}

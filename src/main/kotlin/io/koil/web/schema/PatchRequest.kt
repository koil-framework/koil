package io.koil.web.schema

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.databind.node.ObjectNode
import io.koil.patch.JsonPatch
import io.koil.schemas.*

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.EXISTING_PROPERTY,
    property = "type",
    visible = false
)
@JsonSubTypes(
    JsonSubTypes.Type(MergePatchRequest::class, name = MergePatchRequest.TYPE),
    JsonSubTypes.Type(JsonPatchPatchRequest::class, name = JsonPatchPatchRequest.TYPE),
)
sealed class PatchRequest(
    val type: String
)

data class MergePatchRequest(
    private val partial: ObjectNode,
) : PatchRequest(TYPE) {
    companion object {
        const val TYPE = "merge"
    }
}

data class JsonPatchPatchRequest(
    private val patches: List<JsonPatch>,
) : PatchRequest(TYPE) {
    companion object {
        const val TYPE = "patch"
    }
}

fun patchRequestSchema(partialSchema: JsonSchema): JsonSchema {
    val mergeSchema = JsonSchema.obj("MergePatchRequest")
        .description("A request to update an object using a partial object")
        .implIdentifier(MergePatchRequest::class.qualifiedName!!)
        .polymorphicValue(MergePatchRequest.TYPE)
        .property("partial", partialSchema)
        .build()
    val patchSchema = JsonSchema.obj("JsonPatchPatchRequest")
        .description("A request to update an object using json patches")
        .implIdentifier(JsonPatchPatchRequest::class.qualifiedName!!)
        .polymorphicValue(JsonPatchPatchRequest.TYPE)
        .property(
            "patches",
            JsonSchema.array(JsonPatch.schema)
                .description("The list of json patches to apply")
        )
        .build()

    return JsonSchema.obj("PatchRequest")
        .description("A request to update an object")
        .implIdentifier("io.koil.web.schema.PatchRequest")
        .property(
            JsonSchemaObjectProperty(
                "type",
                JsonSchema.string()
                    .description("The type of update to apply")
                    .enum(setOf(MergePatchRequest.TYPE, JsonPatchPatchRequest.TYPE))
                    .build(),
                true,
                false,
                "type",
                emptySet(),
                null,
            )
        )
        .subType(mergeSchema)
        .subType(patchSchema)
        .build()
}

package io.koil.web.schema

import io.koil.core.fromBase62ToUUID
import io.koil.schemas.ValidationConstraintError
import java.util.*

fun interface Validator<T> {
    fun validate(value: T): ValidationConstraintError?
}

fun interface Converter<T> {
    fun convert(value: String): T?
}

object StringConverter : Converter<String> {
    override fun convert(value: String): String = value
}

object UUIDConverter : Converter<UUID> {
    override fun convert(value: String): UUID {
        return if (value.contains("-")) UUID.fromString(value)
        else value.fromBase62ToUUID()
    }
}

object UUIDValidator : Validator<String> {
    private val uuidRegex = Regex("^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$")
    private val base62UuidRegex = Regex("^[a-zA-Z0-9]{1,22}$")

    override fun validate(value: String): ValidationConstraintError? {
        return if (value.length == 36) {
            if (uuidRegex.matches(value)) null
            else ValidationConstraintError.badType("UUID")
        } else if (!base62UuidRegex.matches(value)) ValidationConstraintError.badType("UUID")
        else null
    }
}

object LongConverter : Converter<Long> {
    override fun convert(value: String): Long? = value.toLongOrNull()
}

object IntConverter : Converter<Int> {
    override fun convert(value: String): Int? = value.toIntOrNull()
}

object DoubleConverter : Converter<Double> {
    override fun convert(value: String): Double? = value.toDoubleOrNull()
}

object FloatConverter : Converter<Float> {
    override fun convert(value: String): Float? = value.toFloatOrNull()
}

object BooleanConverter : Converter<Boolean> {
    override fun convert(value: String): Boolean {
        return value.isNotBlank() && value != "0" && !value.equals("false", true)
    }
}

object StringListConverter : Converter<List<String>> {
    override fun convert(value: String): List<String> {
        if (value.isBlank()) return emptyList()
        return value.split(",")
            .filter { str -> str.isNotBlank() }
    }
}

object StringSetConverter : Converter<Set<String>> {
    override fun convert(value: String): Set<String> {
        if (value.isBlank()) return emptySet()
        return value.split(",")
            .filter { str -> str.isNotBlank() }
            .toSet()
    }
}

data class FormatDefinition<T>(
    val converter: Converter<T>,
    val validator: Validator<T>?,
) {
    companion object {
        fun create(): FormatDefinitionBuilder = FormatDefinitionBuilder()
    }

    class FormatDefinitionBuilder {
        fun <T> converter(converter: Converter<T>): ConvertedFormatDefinition<T> {
            return ConvertedFormatDefinition(converter)
        }

        fun withValidation(validator: Validator<String>): FormatDefinition<String> {
            return FormatDefinition(StringConverter, validator)
        }

        fun build(): FormatDefinition<String> = FormatDefinition(StringConverter, null)
    }

    class ConvertedFormatDefinition<T>(private val converter: Converter<T>) {
        fun withValidation(validator: Validator<T>): FormatDefinition<T> = FormatDefinition(converter, validator)

        fun build(): FormatDefinition<T> = FormatDefinition(converter, null)
    }
}

object DefaultFormats {
    val string = FormatDefinition.create().build()
    val uuid = FormatDefinition.create()
        .converter(UUIDConverter)
        .build()
    val int = FormatDefinition.create()
        .converter(IntConverter)
        .build()
    val long = FormatDefinition.create()
        .converter(LongConverter)
        .build()
    val float = FormatDefinition.create()
        .converter(FloatConverter)
        .build()
    val double = FormatDefinition.create()
        .converter(DoubleConverter)
        .build()
    val boolean = FormatDefinition.create()
        .converter(BooleanConverter)
        .build()
    val stringList = FormatDefinition.create()
        .converter(StringListConverter)
        .build()
    val stringSet = FormatDefinition.create()
        .converter(StringSetConverter)
        .build()
}

package io.koil.web.client

import io.koil.core.toBase62String
import io.koil.web.auth.Principal
import io.koil.web.server.TracingContext
import java.util.*

data class ClientContext(
    val principal: Principal?,
    val tracing: TracingContext?,
    val requestId: String,
    /** Allows to overwrite the base client host config on a per-request basis */
    val remoteHost: String?,
    /** Allows to overwrite the base client port config on a per-request basis */
    val remotePort: Int?,
    /** Allows to overwrite the base client ssl config on a per-request basis */
    val remoteSsl: Boolean?,
) {
    companion object {
        fun create(
            principal: Principal? = null,
            tracing: TracingContext? = null,
            remoteHost: String? = null,
            remotePort: Int? = null,
            remoteSsl: Boolean? = null,
        ): ClientContext =
            ClientContext(principal, tracing, UUID.randomUUID().toBase62String(), remoteHost, remotePort, remoteSsl)
    }
}

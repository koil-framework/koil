package io.koil.web.client

import com.fasterxml.jackson.databind.json.JsonMapper
import io.koil.core.BufferInputStream
import io.koil.core.Failable
import io.koil.core.config.HttpClientConfig
import io.koil.web.Problem
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpClient
import io.vertx.core.http.HttpClientOptions
import io.vertx.core.http.HttpClientResponse
import io.vertx.core.http.RequestOptions
import org.slf4j.LoggerFactory

open class BaseClient(
    protected val upstreamServiceName: String,
    vertx: Vertx,
    protected val config: HttpClientConfig,
    options: HttpClientOptions,
    protected val jsonMapper: JsonMapper,
) {
    companion object {
        private val logger = LoggerFactory.getLogger(BaseClient::class.java)
    }

    private var host = config.host
    private var port = config.port
    private var ssl = config.ssl

    protected val httpClient: HttpClient

    init {
        config.holder.onUpdate("sslPrivateKeyPath", "sslCertPath", "caCertPath") {
            reloadSslCerts()
        }
        config.holder.onUpdate("host", "port", "ssl") {
            host = config.host
            port = config.port
            ssl = config.ssl
        }

        httpClient = vertx.createHttpClient(config.configure(options))
    }

    private fun reloadSslCerts() {
        httpClient.updateSSLOptions(config.sslOptions())
    }

    protected fun createRequestOptions(context: ClientContext): RequestOptions = RequestOptions()
        .setSsl(context.remoteSsl ?: ssl ?: false)
        .setHost(context.remoteHost ?: host)
        .setPort(context.remotePort ?: port)

    protected fun <V> handleError(operationId: String, res: HttpClientResponse): Future<V> {
        val contentTypeHeader = res.getHeader("Content-Type")
        if (contentTypeHeader.contains("application/problem+json")) {
            return res.body().map {
                val result = processJsonBody(it, Problem::class.java)
                if (Failable.isFailed(result)) throw UpstreamCallFailure.Deserialization(
                    upstreamServiceName,
                    operationId,
                    "application/problem+json",
                    result.error
                ) else throw UpstreamCallFailure.Problem(upstreamServiceName, operationId, result.value)
            }
        }

        // Unknown error
        logger.error("Got status code {} with content type {}", res.statusCode(), contentTypeHeader)
        return Future.failedFuture(
            UpstreamCallFailure.Unknown(
                upstreamServiceName,
                operationId,
                res.statusCode(),
                contentTypeHeader
            )
        )
    }

    protected fun <V> responseBodyHandler(operationId: String, res: HttpClientResponse, cls: Class<V>): Future<V> {
        return res.body().map {
            val result = processJsonBody(it, cls)
            if (Failable.isSuccess(result)) result.value
            else throw UpstreamCallFailure.Deserialization(
                upstreamServiceName,
                operationId,
                "application/json",
                result.error
            )
        }
    }

    protected fun <V> processJsonBody(
        body: Buffer,
        cls: Class<V>,
    ): Failable<V, Exception> {
        return try {
            Failable.Success(jsonMapper.readValue(BufferInputStream(body), cls))
        } catch (e: Exception) {
            logger.error("Failed to deserialize ${cls.simpleName} json response body", e)
            Failable.Failed(e)
        }
    }
}

package io.koil.web.client

import io.koil.web.ProblemException

sealed class UpstreamCallFailure(
    val upstreamServiceName: String,
    val operationId: String,
    message: String,
    cause: Throwable?
) : Exception(message, cause) {
    class Network(
        upstreamServiceName: String,
        operationId: String,
        cause: Throwable?,
    ) : UpstreamCallFailure(upstreamServiceName, operationId, "A network error happened", cause)

    class Problem(
        upstreamServiceName: String,
        operationId: String,
        val upstreamProblem: io.koil.web.Problem
    ) : UpstreamCallFailure(
        upstreamServiceName,
        operationId,
        "The upstream called failed with a problem",
        ProblemException(upstreamProblem),
    )

    class Deserialization(
        upstreamServiceName: String,
        operationId: String,
        val contentType: String,
        cause: Throwable?,
    ) : UpstreamCallFailure(upstreamServiceName, operationId, "Error while deserializing the upstream response", cause)

    class Unknown(
        upstreamServiceName: String,
        operationId: String,
        val statusCode: Int,
        val contentType: String
    ) : UpstreamCallFailure(
        upstreamServiceName,
        operationId,
        "The upstream returned an error in an unknown format",
        null
    )
}

package io.koil.web.client

import io.vertx.core.http.RequestOptions

interface RequestProcessor {
    fun process(context: ClientContext, request: RequestOptions)
}

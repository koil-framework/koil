package io.koil.web.webdef

import io.koil.schemas.JsonSchema
import io.koil.web.Problem
import io.vertx.core.Handler
import io.vertx.core.http.HttpMethod
import io.vertx.ext.web.RoutingContext

enum class EndpointHandlerReturnType {
    FAILABLE,
    RESPONSE_TYPE,
    UNIT,
    PROBLEM,
}

enum class EndpointContentType {
    JSON,
    BINARY,
    TEXT,
    NONE,
}

data class EndpointRequestBody(
    val schema: JsonSchema?,
    val type: EndpointContentType,
    /** The expected HTTP content type */
    val expectedContentType: String,
    /** True to enable client-side request streaming. Only affects clients for now. Otherwise, the method will request a Buffer */
    val stream: Boolean,
)

data class EndpointResponse(
    val status: Int,
    val description: String,
    val type: EndpointContentType,
    val schema: JsonSchema?,
    val example: Any?,
    /** True to enable client-side response streaming. Only affects clients for now */
    val stream: Boolean,
) {
    constructor(status: Int, description: String, schema: JsonSchema, example: Any? = null) :
            this(status, description, EndpointContentType.JSON, schema, example, false)

    constructor(status: Int, description: String, type: EndpointContentType, stream: Boolean) :
            this(status, description, type, null, null, stream)

    companion object {
        fun problem(example: Problem): EndpointResponse = EndpointResponse(
            example.status,
            example.detail ?: example.title,
            Problem.schema,
            example,
        )
    }
}

fun interface MiddlewareFactory {
    fun create(endpoint: Endpoint): Handler<RoutingContext>
}

data class Endpoint(
    val method: HttpMethod,
    val namespace: String,
    val apiVersion: Int,

    val path: String,
    val regexPath: Boolean,
    val summary: String,
    val controllerName: String,
    val methodName: String,
    val deprecated: Boolean,
    val operationId: String,
    val tags: Set<String>,
    val description: String,
    /** True to make sure the user is authenticated, false if authentication is optional */
    val requiresAuthentication: Boolean,
    val authorizationRequirement: AuthorizationRequirement?,
    /** Null means no request body */
    val requestBody: EndpointRequestBody,
    val params: List<RequestParameter<*>>,
    val mainResponse: EndpointResponse,
    val errorResponses: List<EndpointResponse>,
    /**
     * True to prevent the response from being processed, allowing to return anything other than json
     * as long as it's handled by the controller
     */
    val noResponseProcessing: Boolean,
    val middlewares: List<Class<MiddlewareFactory>>,
    val hidden: Boolean,
) {
    val returnType: EndpointHandlerReturnType
        get() =
            if (noResponseProcessing || mainResponse.schema == null) {
                if (errorResponses.isNotEmpty()) EndpointHandlerReturnType.PROBLEM
                else EndpointHandlerReturnType.UNIT
            } else if (errorResponses.isNotEmpty()) EndpointHandlerReturnType.FAILABLE
            else EndpointHandlerReturnType.RESPONSE_TYPE

    companion object {
        const val API_NAMESPACE = "api"

        fun create(
            method: HttpMethod,
            apiVersion: Int,
            path: String,
            regexPath: Boolean = false,
        ): EpIdentityBuilder = create(method, API_NAMESPACE, apiVersion, path, regexPath)

        fun create(
            method: HttpMethod,
            namespace: String,
            apiVersion: Int,
            path: String,
            regexPath: Boolean = false,
        ): EpIdentityBuilder = EpIdentityBuilder(EpUriData(method, namespace, apiVersion, path, regexPath))

        fun get(apiVersion: Int, path: String, regexPath: Boolean = false): EpIdentityBuilder =
            create(HttpMethod.GET, apiVersion, path, regexPath)

        fun get(namespace: String, apiVersion: Int, path: String, regexPath: Boolean = false): EpIdentityBuilder =
            create(HttpMethod.GET, namespace, apiVersion, path, regexPath)

        fun post(apiVersion: Int, path: String, regexPath: Boolean = false): EpIdentityBuilder =
            create(HttpMethod.POST, apiVersion, path, regexPath)

        fun post(namespace: String, apiVersion: Int, path: String, regexPath: Boolean = false): EpIdentityBuilder =
            create(HttpMethod.POST, namespace, apiVersion, path, regexPath)

        fun put(apiVersion: Int, path: String, regexPath: Boolean = false): EpIdentityBuilder =
            create(HttpMethod.PUT, apiVersion, path, regexPath)

        fun put(namespace: String, apiVersion: Int, path: String, regexPath: Boolean = false): EpIdentityBuilder =
            create(HttpMethod.PUT, namespace, apiVersion, path, regexPath)

        fun patch(apiVersion: Int, path: String, regexPath: Boolean = false): EpIdentityBuilder =
            create(HttpMethod.PATCH, apiVersion, path, regexPath)

        fun patch(namespace: String, apiVersion: Int, path: String, regexPath: Boolean = false): EpIdentityBuilder =
            create(HttpMethod.PATCH, namespace, apiVersion, path, regexPath)

        fun delete(apiVersion: Int, path: String, regexPath: Boolean = false): EpIdentityBuilder =
            create(HttpMethod.DELETE, apiVersion, path, regexPath)

        fun delete(namespace: String, apiVersion: Int, path: String, regexPath: Boolean = false): EpIdentityBuilder =
            create(HttpMethod.DELETE, namespace, apiVersion, path, regexPath)
    }
}

internal data class EpUriData(
    val method: HttpMethod,
    val namespace: String,
    val apiVersion: Int,
    val path: String,
    val regexPath: Boolean,
)

internal data class EpIdentityData(
    val operationId: String,
    val tags: Set<String>,
    val summary: String,
)

internal data class EpHandlerData(
    val controllerName: String,
    val methodName: String,
    val hidden: Boolean,
    val middlewares: List<Class<MiddlewareFactory>>,
)

internal data class EpDescriptionData(
    val description: String
)

internal data class EpAuthData(
    val requiresLogin: Boolean,
    val authorizationRequirement: AuthorizationRequirement?,
)

internal data class EpRequestData(
    val requestBody: EndpointRequestBody,
)

internal data class EpResponseData(
    val mainResponse: EndpointResponse,
    val errorResponses: List<EndpointResponse>,
    val noResponseProcessing: Boolean,
)

class EpIdentityBuilder internal constructor(
    private val uriData: EpUriData,
) {
    fun identity(operationId: String, tags: Set<String>, summary: String): EpHandlerBuilder {
        return EpHandlerBuilder(uriData, EpIdentityData(operationId, tags, summary))
    }
}

class EpHandlerBuilder internal constructor(
    private val uriData: EpUriData,
    private val identityData: EpIdentityData,
) {
    fun handler(
        controllerName: String,
        methodName: String,
        hidden: Boolean = false,
        vararg middlewares: Class<MiddlewareFactory>,
    ): EpDescriptionBuilder {
        return EpDescriptionBuilder(
            uriData,
            identityData,
            EpHandlerData(controllerName, methodName, hidden, middlewares.toList())
        )
    }
}

class EpDescriptionBuilder internal constructor(
    private val uriData: EpUriData,
    private val identityData: EpIdentityData,
    private val handlerData: EpHandlerData,
) {
    fun description(description: String): EpAuthBuilder {
        return EpAuthBuilder(uriData, identityData, handlerData, EpDescriptionData(description))
    }
}

class EpAuthBuilder internal constructor(
    private val uriData: EpUriData,
    private val identityData: EpIdentityData,
    private val handlerData: EpHandlerData,
    private val descriptionData: EpDescriptionData,
) {
    fun allowAnonymous(): EpRequestBuilder {
        return EpRequestBuilder(uriData, identityData, handlerData, descriptionData, EpAuthData(false, null))
    }

    fun requiresAuthentication(): EpRequestBuilder {
        return EpRequestBuilder(uriData, identityData, handlerData, descriptionData, EpAuthData(true, null))
    }

    fun requiresAuthorization(authorizationRequirement: AuthorizationRequirement): EpRequestBuilder {
        return EpRequestBuilder(
            uriData,
            identityData,
            handlerData,
            descriptionData,
            EpAuthData(true, authorizationRequirement)
        )
    }
}

abstract class EpReqResBuilder internal constructor(
    private val requestData: EpRequestData,
) {
    internal abstract val uriData: EpUriData
    internal abstract val identityData: EpIdentityData
    internal abstract val handlerData: EpHandlerData
    internal abstract val descriptionData: EpDescriptionData
    internal abstract val authData: EpAuthData

    private fun buildNextStage(current: EpResponseData): EpParamBuilder = EpParamBuilder(
        uriData,
        identityData,
        handlerData,
        descriptionData,
        authData,
        requestData,
        current,
    )

    private val errorResponses = mutableListOf<EndpointResponse>()

    fun returnsError(vararg response: EndpointResponse) = apply { errorResponses.addAll(response) }
    fun returns404() = apply { errorResponses.add(EndpointResponse(404, "Resource not found", Problem.schema)) }

    fun returnsOk(
        description: String,
        schema: JsonSchema,
    ): EpParamBuilder = buildNextStage(
        EpResponseData(EndpointResponse(200, description, schema), errorResponses, false)
    )

    fun returnsOk(
        description: String,
        responseContentType: EndpointContentType,
        noResponseProcessing: Boolean = true,
        stream: Boolean = false,
    ): EpParamBuilder = buildNextStage(
        EpResponseData(
            EndpointResponse(200, description, responseContentType, stream),
            errorResponses,
            noResponseProcessing
        )
    )

    fun returnsNoContent(description: String): EpParamBuilder = buildNextStage(
        EpResponseData(
            EndpointResponse(204, description, EndpointContentType.NONE, null, null, false),
            errorResponses,
            false
        )
    )

    fun returnsAccepted(
        description: String,
        schema: JsonSchema,
    ): EpParamBuilder = buildNextStage(
        EpResponseData(EndpointResponse(202, description, schema), errorResponses, false)
    )

    fun returnsAccepted(
        description: String,
        type: EndpointContentType,
        noResponseProcessing: Boolean = true,
        stream: Boolean = false,
    ): EpParamBuilder = buildNextStage(
        EpResponseData(
            EndpointResponse(202, description, type, stream),
            errorResponses,
            noResponseProcessing
        )
    )
}

class EpRequestBuilder internal constructor(
    override val uriData: EpUriData,
    override val identityData: EpIdentityData,
    override val handlerData: EpHandlerData,
    override val descriptionData: EpDescriptionData,
    override val authData: EpAuthData,
) : EpReqResBuilder(EpRequestData(EndpointRequestBody(null, EndpointContentType.NONE, "", false))) {
    fun withBody(requestBody: JsonSchema): EpReqResBuilder {
        return EpResponseBuilder(
            uriData,
            identityData,
            handlerData,
            descriptionData,
            authData,
            EpRequestData(EndpointRequestBody(requestBody, EndpointContentType.JSON, "application/json", false))
        )
    }

    fun withBody(type: EndpointContentType, expectedContentType: String, stream: Boolean = false): EpReqResBuilder {
        return EpResponseBuilder(
            uriData,
            identityData,
            handlerData,
            descriptionData,
            authData,
            EpRequestData(EndpointRequestBody(null, type, expectedContentType, stream))
        )
    }
}

class EpResponseBuilder internal constructor(
    override val uriData: EpUriData,
    override val identityData: EpIdentityData,
    override val handlerData: EpHandlerData,
    override val descriptionData: EpDescriptionData,
    override val authData: EpAuthData,
    requestData: EpRequestData,
) : EpReqResBuilder(requestData)

class EpParamBuilder internal constructor(
    private val uriData: EpUriData,
    private val identityData: EpIdentityData,
    private val handlerData: EpHandlerData,
    private val descriptionData: EpDescriptionData,
    private val authData: EpAuthData,
    private val requestData: EpRequestData,
    private val responseData: EpResponseData,
) {
    private var deprecated = false
    private val params = mutableListOf<RequestParameter<*>>()

    fun deprecated(deprecated: Boolean = true) = apply { this.deprecated = deprecated }

    fun addParam(param: RequestParameter<*>) = apply { params.add(param) }

    fun addParams(vararg p: RequestParameter<*>) = apply { params.addAll(p) }

    fun build(): Endpoint = Endpoint(
        uriData.method,
        uriData.namespace,
        uriData.apiVersion,
        uriData.path,
        uriData.regexPath,
        identityData.summary,
        handlerData.controllerName,
        handlerData.methodName,
        deprecated,
        identityData.operationId,
        identityData.tags,
        descriptionData.description,
        authData.requiresLogin,
        authData.authorizationRequirement,
        requestData.requestBody,
        params,
        responseData.mainResponse,
        responseData.errorResponses,
        responseData.noResponseProcessing,
        handlerData.middlewares,
        handlerData.hidden,
    )
}

package io.koil.web.webdef

import io.koil.schemas.BooleanJsonSchema
import io.koil.schemas.JsonSchema
import io.koil.web.ValidationError
import io.koil.web.schema.Converter
import io.koil.web.schema.DefaultFormats
import io.koil.web.schema.FormatDefinition
import io.vertx.ext.web.RoutingContext
import java.util.*

enum class RequestParameterType {
    QUERY,
    PATH,
    HEADER,
    COOKIE,
}

fun <T> RoutingContext.getParameter(name: String): T? = this["koil_prm_$name"]
fun <T> RoutingContext.setParameter(name: String, value: T?) = this.put("koil_prm_$name", value)

data class RequestParameter<T>(
    val type: RequestParameterType,
    val name: String,
    val specName: String,
    val description: String,
    val example: String,
    val schema: JsonSchema,
    val format: FormatDefinition<T>,
    val required: Boolean,
    val defaultValue: T?,
) {
    fun process(context: RoutingContext): ValidationError? {
        val sourceParam = when (type) {
            RequestParameterType.PATH -> context.pathParam(name)
            RequestParameterType.QUERY -> context.queryParam(name).firstOrNull()
            RequestParameterType.HEADER -> context.request().getHeader(name)
            RequestParameterType.COOKIE -> context.request().getCookie(name)?.value
        }

        if (sourceParam.isNullOrBlank()) {
            return if (required) ValidationError(name, "The parameter must be provided in the query")
            else {
                context.setParameter(name, defaultValue)
                null
            }
        }

        val schemaValidation = schema.validateRaw(!required, sourceParam)
        if (schemaValidation != null) {
            return ValidationError(name, "$name ${schemaValidation.message}")
        }

        val targetParam = format.converter.convert(sourceParam)
        if (targetParam == null) {
            return if (required) ValidationError(name, "Failed to convert param")
            else {
                context.setParameter(name, defaultValue)
                null
            }
        }

        if (format.validator != null) {
            val validationResult = format.validator.validate(targetParam)
            if (validationResult != null) return ValidationError(name, "$name ${validationResult.message}")
        }

        context.setParameter(name, targetParam)
        return null
    }

    companion object {
        private fun normalizeParamName(name: String): String {
            return name.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
        }

        fun query(name: String, example: String, specName: String? = null): IdentityBuilder {
            return IdentityBuilder(RequestParameterType.QUERY, name, specName ?: normalizeParamName(name), example)
        }

        fun path(name: String, example: String, specName: String? = null): IdentityBuilder {
            return IdentityBuilder(RequestParameterType.PATH, name, specName ?: normalizeParamName(name), example)
        }

        fun header(name: String, example: String, specName: String? = null): IdentityBuilder {
            // TODO Header spec name normalization
            return IdentityBuilder(RequestParameterType.HEADER, name, specName ?: normalizeParamName(name), example)
        }

        fun cookie(name: String, example: String, specName: String? = null): IdentityBuilder {
            return IdentityBuilder(RequestParameterType.COOKIE, name, specName ?: normalizeParamName(name), example)
        }
    }

    class IdentityBuilder internal constructor(
        private val type: RequestParameterType,
        private val name: String,
        private val specName: String,
        private val example: String,
    ) {
        fun description(description: String): TypeBuilder {
            return TypeBuilder(type, name, specName, description, example)
        }
    }

    class TypeBuilder internal constructor(
        private val type: RequestParameterType,
        private val name: String,
        private val specName: String,
        private val description: String,
        private val example: String,
    ) {
        fun string(schema: JsonSchema = JsonSchema.string().build()): RequirementBuilder<String> {
            return RequirementBuilder(type, name, specName, description, example, schema, DefaultFormats.string)
        }

        fun uuid(schema: JsonSchema = JsonSchema.uuid().build()): RequirementBuilder<UUID> {
            return RequirementBuilder(type, name, specName, description, example, schema, DefaultFormats.uuid)
        }

        fun int(schema: JsonSchema = JsonSchema.int().build()): RequirementBuilder<Int> {
            return RequirementBuilder(type, name, specName, description, example, schema, DefaultFormats.int)
        }

        fun long(schema: JsonSchema = JsonSchema.long().build()): RequirementBuilder<Long> {
            return RequirementBuilder(type, name, specName, description, example, schema, DefaultFormats.long)
        }

        fun bool(schema: JsonSchema = BooleanJsonSchema()): RequirementBuilder<Boolean> {
            return RequirementBuilder(type, name, specName, description, example, schema, DefaultFormats.boolean)
        }

        fun stringList(schema: JsonSchema = JsonSchema.string().build()): RequirementBuilder<List<String>> {
            return RequirementBuilder(type, name, specName, description, example, schema, DefaultFormats.stringList)
        }

        fun stringSet(schema: JsonSchema = JsonSchema.string().build()): RequirementBuilder<Set<String>> {
            return RequirementBuilder(type, name, specName, description, example, schema, DefaultFormats.stringSet)
        }

        fun <T> custom(schema: JsonSchema, format: FormatDefinition<T>): RequirementBuilder<T> {
            return RequirementBuilder(type, name, specName, description, example, schema, format)
        }
    }

    class RequirementBuilder<T> internal constructor(
        private val type: RequestParameterType,
        private val name: String,
        private val specName: String,
        private val description: String,
        private val example: String,
        private val schema: JsonSchema,
        private val format: FormatDefinition<T>,
    ) {
        fun required(): RequestParameter<T> = build(true, null)
        fun optional(defaultValue: T? = null): RequestParameter<T> = build(false, defaultValue)

        private fun build(required: Boolean, defaultValue: T?): RequestParameter<T> = RequestParameter(
            type,
            name,
            specName,
            description,
            example,
            schema,
            format,
            required,
            defaultValue,
        )
    }
}

data class SortParams(
    val fields: List<SortParam>,
)

data class SortParam(val field: String, val desc: Boolean)

object SortParamConverter : Converter<SortParams> {
    override fun convert(value: String): SortParams {
        if (value.isBlank()) return SortParams(emptyList())
        return SortParams(
            value.split(",")
                .filter { str -> str.isNotBlank() }
                .map {
                    if (it.startsWith("-")) SortParam(it.substring(1), true)
                    else if (it.startsWith("+")) SortParam(it.substring(1), false)
                    else SortParam(it, false)
                }
        )
    }
}

object RequestParameters {
    // TODO Sort param doesn't really work !!
    fun querySort(fields: Set<String>, name: String = "sort"): RequestParameter<SortParams> {
        val description = StringBuilder(
            """
            A list of fields to sort on.
            Fields can be prepended by `-` to signal the sort should be descending (instead of ascending by default).
            
            The following fields can be sorted on:
        """.trimIndent()
        )

        fields.forEach { description.append("\n- ").append(it) }

        return RequestParameter.query(name, fields.first())
            .description(description.toString())
            .custom(
                JsonSchema.string().implIdentifier(SortParams::class.qualifiedName!!).enum(fields).build(),
                FormatDefinition.create().converter(SortParamConverter).build()
            )
            .optional()
    }

    fun queryPage(
        name: String = "page",
        description: String = "The number of the page to return, starting at 1"
    ) = RequestParameter.query(name, "1")
        .description(description)
        .int(JsonSchema.int().minimum(1).build())
        .optional(1)

    fun queryPageSize(
        name: String = "pageSize",
        description: String = "The maximum number of elements to return in a single page",
        maximum: Long = 100L,
    ) = RequestParameter.query(name, "50")
        .description(description)
        .int(JsonSchema.int().minimum(1).maximum(maximum).build())
        .optional(50)
}

package io.koil.web.webdef

import io.koil.schemas.JsonSchema

interface ApiDefinition {
    fun endpoints(): List<Endpoint>

    fun schemasWithIdentifiers(): Map<String, JsonSchema> = emptyMap()
}

package io.koil.web.webdef

interface AuthorizationRequirement {
    /**
     * Should return a markdown block that will be added to the description of the endpoint,
     * allowing to describe authorization requirements directly in the description
     */
    fun generateDocBlock(): String
}

package io.koil.web.server

import com.fasterxml.jackson.databind.JsonNode
import io.koil.core.BufferInputStream
import io.koil.schemas.JsonSchema
import io.koil.web.Problem
import io.koil.web.end
import io.koil.web.webdef.EndpointContentType
import io.vertx.core.Handler
import io.vertx.core.buffer.Buffer
import io.vertx.ext.web.RoutingContext
import org.slf4j.LoggerFactory

fun RoutingContext.getRawRequestBody(): Buffer? = get("koil_reqBody_raw")
fun RoutingContext.getJsonRequestBody(): JsonNode? = get("koil_reqBody_json")

class RequestBodyHandler(
    private val contentType: EndpointContentType,
    private val expectedContentType: String,
    private val jsonSchema: JsonSchema?,
    private val type: Class<*>?,
    private val validator: RequestBodyValidator<Any>?,
) : Handler<RoutingContext> {
    companion object {
        private val logger = LoggerFactory.getLogger(RequestBodyHandler::class.java)
    }

    override fun handle(event: RoutingContext) {
        val contentTypeHeader = event.request().getHeader("Content-Type")
            ?: return event.end(
                Problem.badRequest(
                    "Content-Type",
                    "A request body is expected, with a Content-Type header"
                ).build()
            )
        if (!contentTypeHeader.startsWith(expectedContentType)) {
            return event.end(
                Problem.badRequest(
                    "Content-Type",
                    "Expected content type $expectedContentType, but got $contentTypeHeader"
                ).build()
            )
        }

        event.request().body().onSuccess {
            event.put("koil_reqBody_raw", it)
            try {
                processRequestBody(event, it)
            } catch (e: Exception) {
                logger.error("Failed to process request body", e)
                event.end(Problem.internalError().build())
            }
        }.onFailure {
            logger.error("Failed to get request body", it)
            event.end(Problem.internalError().build())
        }

        if (!event.request().isEnded) {
            event.request().resume()
        }
    }

    private fun processRequestBody(ctx: RoutingContext, body: Buffer) {
        when (contentType) {
            EndpointContentType.JSON -> parseJsonBody(ctx, body)
            EndpointContentType.TEXT -> parseTextBody(ctx, body)
            EndpointContentType.BINARY -> parseBinaryBody(ctx, body)
            // This should not happen
            EndpointContentType.NONE -> ctx.next()
        }
    }

    private fun parseTextBody(ctx: RoutingContext, body: Buffer) {
        ctx.put("koil_reqBody", body.toString(Charsets.UTF_8))
        ctx.next()
    }

    private fun parseBinaryBody(ctx: RoutingContext, body: Buffer) {
        ctx.put("koil_reqBody", body)
        ctx.next()
    }

    private fun parseJsonBody(ctx: RoutingContext, body: Buffer) {
        val jsonMapper = ctx.getJsonMapper()

        val json = try {
            jsonMapper.readTree(BufferInputStream(body))
        } catch (e: Exception) {
            logger.debug("Failed to parse JSON request body", e)
            ctx.end(Problem.badRequest("request-body", "The provided request body is not valid json").build())
            return
        }
        ctx.put("koil_reqBody_json", json)

        if (jsonSchema == null) {
            ctx.put("koil_reqBody", json)
            return ctx.next()
        }

        val err = try {
            jsonSchema.validate(false, json)
        } catch (e: Exception) {
            logger.error("Request body schema validation failed", e)
            ctx.end(Problem.internalError().build())
            return
        }
        if (err != null) {
            ctx.end(Problem.badRequest("request-body", "Request body ${err.message}").build())
            return
        }

        if (type == null) {
            ctx.put("koil_reqBody", json)
            return ctx.next()
        }

        val typedBody = try {
            jsonMapper.convertValue(json, type)
        } catch (e: Exception) {
            logger.error("Failed to convert JSON request body to POJO", e)
            ctx.end(Problem.internalError().build())
            return
        }

        if (!processTypedValidation(ctx, typedBody)) return

        ctx.put("koil_reqBody", typedBody)
        ctx.next()
    }

    private fun processTypedValidation(ctx: RoutingContext, body: Any): Boolean {
        if (validator == null) return true

        try {
            val err = validator.validate(body) ?: return true
            ctx.end(Problem.badRequest("request-body", err.message).build())
        } catch (e: Exception) {
            logger.error("Failed to process additional validation", e)
            ctx.end(Problem.internalError().build())
        }

        return false
    }
}

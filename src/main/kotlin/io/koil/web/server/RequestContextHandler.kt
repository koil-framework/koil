package io.koil.web.server

import com.fasterxml.jackson.databind.json.JsonMapper
import io.koil.core.LoggingContext
import io.koil.core.toBase62String
import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext
import java.util.*

fun RoutingContext.setRequestId(requestId: String) = put("koil_requestId", requestId)
fun RoutingContext.getRequestId(): String = get("koil_requestId")

fun RoutingContext.setSpanId(requestId: String) = put("koil_spanId", requestId)
fun RoutingContext.getSpanId(): String? = get("koil_spanId")

private fun RoutingContext.setJsonMapper(jsonMapper: JsonMapper) = put("koil_jsonMapper", jsonMapper)
fun RoutingContext.getJsonMapper(): JsonMapper = get("koil_jsonMapper")

class RequestContextHandler(private val jsonMapper: JsonMapper) : Handler<RoutingContext> {
    override fun handle(event: RoutingContext) {
        event.request().pause()
        event.setJsonMapper(jsonMapper)

        // Get the request id from the header, or create one
        val requestId = event.request().getHeader("X-Request-Id")
        val actualRequestId = if (requestId.isNullOrBlank()) UUID.randomUUID().toBase62String()
        else requestId
        LoggingContext.set("requestId", actualRequestId)
        event.setRequestId(actualRequestId)

        // Try to extract a span id
        val spanId = event.request().getHeader("X-Span-Id")
        if (!spanId.isNullOrBlank()) event.setSpanId(spanId)

        event.next()
    }
}

package io.koil.web.server

import io.koil.web.auth.getPrincipal
import io.koil.web.client.ClientContext
import io.vertx.ext.web.RoutingContext

data class TracingContext(
    val traceId: String,
    val spanId: String,
)

fun RoutingContext.getTracingContext(): TracingContext? {
    val requestId = getRequestId()
    val spanId = getSpanId()
    return if (spanId == null) null
    else TracingContext(requestId, spanId)
}

fun RoutingContext.getClientContext(): ClientContext = ClientContext.create(getPrincipal(), getTracingContext())

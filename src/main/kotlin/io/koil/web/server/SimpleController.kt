package io.koil.web.server

import io.vertx.ext.web.Router

interface SimpleController {
    suspend fun register(router: Router)
}

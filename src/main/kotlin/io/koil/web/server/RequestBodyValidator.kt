package io.koil.web.server

import io.koil.web.schema.Validator

interface RequestBodyValidator<T> : Validator<T> {
    val forCls: Class<T>
}

package io.koil.web.server

import io.koil.web.Problem
import io.koil.web.auth.getPrincipal
import io.koil.web.end
import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext

object AuthenticationRequiredHandler : Handler<RoutingContext> {
    override fun handle(event: RoutingContext) {
        val principal = event.getPrincipal()
        if (principal == null) {
            event.end(Problem.authenticationRequired().build())
        } else {
            event.next()
        }
    }
}

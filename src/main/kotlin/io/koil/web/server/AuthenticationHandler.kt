package io.koil.web.server

import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext

interface AuthenticationHandler : Handler<RoutingContext>

package io.koil.web.server

import io.koil.web.Problem
import io.koil.web.end
import io.koil.web.webdef.RequestParameter
import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext
import org.slf4j.LoggerFactory

class ParametersHandler(
    private val parameters: List<RequestParameter<*>>
) : Handler<RoutingContext> {
    companion object {
        private val logger = LoggerFactory.getLogger(ParametersHandler::class.java)
    }

    override fun handle(event: RoutingContext) {
        for (p in parameters) {
            try {
                val err = p.process(event)
                if (err != null) {
                    event.end(Problem.badRequest(listOf(err)).build())
                    return
                }
            } catch (e: Exception) {
                logger.error("Failed to process param ${p.name}", e)
                event.end(Problem.internalError().build())
                return
            }
        }
        event.next()
    }
}

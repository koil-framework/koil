package io.koil.web.server

import io.koil.web.webdef.AuthorizationRequirement
import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext

interface AuthorizationMiddlewareFactory {
    fun supportedRequirements(): List<Class<out AuthorizationRequirement>>

    fun create(requirement: AuthorizationRequirement): Handler<RoutingContext>
}

class AuthorizationMiddlewareNotFound(requirement: AuthorizationRequirement) :
    Exception("Authorization middleware not found for requirement $requirement")

class AuthorizationProcessor(private val middlewareFactories: Set<AuthorizationMiddlewareFactory>) {
    fun getMiddleware(requirement: AuthorizationRequirement): Handler<RoutingContext> {
        if (middlewareFactories.isEmpty()) throw AuthorizationMiddlewareNotFound(requirement)

        for (middlewareFactory in middlewareFactories) {
            val supportedRequirements = middlewareFactory.supportedRequirements()
            for (supportedRequirement in supportedRequirements) {
                if (supportedRequirement.isAssignableFrom(requirement.javaClass)) {
                    return middlewareFactory.create(requirement)
                }
            }
        }

        throw AuthorizationMiddlewareNotFound(requirement)
    }
}

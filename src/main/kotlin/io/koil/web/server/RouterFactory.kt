package io.koil.web.server

import io.koil.di.DiContainer
import io.vertx.ext.web.Router

interface RouterFactory {
    suspend fun register(diContainer: DiContainer, router: Router)

    fun findRequestBodyValidator(
        validators: Set<RequestBodyValidator<Any>>,
        targetCls: Class<out Any>,
    ): RequestBodyValidator<Any>? {
        return validators.firstOrNull { targetCls.isAssignableFrom(it.forCls) }
    }
}

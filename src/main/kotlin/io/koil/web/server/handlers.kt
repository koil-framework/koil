package io.koil.web.server

import com.fasterxml.jackson.databind.json.JsonMapper
import io.koil.core.Failable
import io.koil.core.writeValueAsBuffer
import io.koil.di.DiContainer
import io.koil.web.Problem
import io.koil.web.ProblemException
import io.koil.web.ValidationException
import io.koil.web.end
import io.koil.web.webdef.Endpoint
import io.koil.web.webdef.MiddlewareFactory
import io.vertx.core.Future
import io.vertx.core.buffer.Buffer
import io.vertx.ext.web.Route
import io.vertx.ext.web.RoutingContext
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

private val logger = LoggerFactory.getLogger("io.koil.web.server.ControllerHandler")

fun Route.registerGlobalMiddlewares(middlewares: Set<MiddlewareFactory>, endpoint: Endpoint): Route {
    for (middleware in middlewares) {
        handler(middleware.create(endpoint))
    }

    return this
}

fun Route.registerEndpointMiddlewares(diContainer: DiContainer, endpoint: Endpoint): Route {
    for (middleware in endpoint.middlewares) {
        val middlewareInstance = diContainer.getInstance(middleware)
        handler(middlewareInstance.create(endpoint))
    }

    return this
}

fun Route.registerControllerHandler(
    noResponseProcessing: Boolean,
    errorHandler: EndpointErrorHandler,
    controllerHandler: (RoutingContext) -> Any?
) {
    handler { ctx ->
        val result = try {
            controllerHandler(ctx)
        } catch (t: Throwable) {
            return@handler errorHandler.handleError(ctx, t)
        }

        val jsonMapper = ctx.getJsonMapper()
        try {
            processResponse(ctx, jsonMapper, noResponseProcessing, errorHandler, result)
        } catch (e: Exception) {
            logger.error("Failed to process response from controller", e)
            ctx.end(Problem.internalError().build())
        }
    }
}

fun interface EndpointErrorHandler {
    fun handleError(ctx: RoutingContext, e: Throwable)
}

@OptIn(DelicateCoroutinesApi::class)
fun Route.registerCoroutineControllerHandler(
    noResponseProcessing: Boolean,
    errorHandler: EndpointErrorHandler,
    controllerHandler: suspend (RoutingContext) -> Any?
) {
    handler { ctx ->
        GlobalScope.launch(ctx.vertx().dispatcher()) {
            val result = try {
                controllerHandler(ctx)
            } catch (t: Throwable) {
                return@launch errorHandler.handleError(ctx, t)
            }

            val jsonMapper = ctx.getJsonMapper()
            try {
                processResponse(ctx, jsonMapper, noResponseProcessing, errorHandler, result)
            } catch (e: Exception) {
                logger.error("Failed to process response from controller", e)
                ctx.end(Problem.internalError().build())
            }
        }
    }
}

fun interface ErrorMapper {
    fun map(ctx: RoutingContext, e: Throwable): Problem
}

val defaultErrorMapper = ErrorMapper { _, e ->
    when (e) {
        is ValidationException -> Problem.badRequest(e.errors).build()
        is ProblemException -> e.problem
        is NotImplementedError -> Problem.notImplemented().build()
        else -> {
            logger.error("Unhandled exception in controller", e)
            Problem.internalError().build()
        }
    }
}

fun defaultErrorHandler(errorMapper: ErrorMapper = defaultErrorMapper): EndpointErrorHandler {
    return EndpointErrorHandler { ctx, e ->
        val problem = errorMapper.map(ctx, e)
        if (!ctx.response().ended() && !ctx.response().closed()) {
            ctx.end(problem)
        }
    }
}

fun processResponse(
    ctx: RoutingContext,
    jsonMapper: JsonMapper,
    noResponseProcessing: Boolean,
    errorHandler: EndpointErrorHandler,
    result: Any?
) {
    if (ctx.response().ended()) return

    // Only handle errors in this case
    if (noResponseProcessing) {
        if (result is Future<*>) {
            result.onFailure { errorHandler.handleError(ctx, it) }
        } else if (result is Failable<*, *> && Failable.isFailed(result)) {
            ctx.end((result as Failable.Failed<Any?, Problem>).error)
        }

        return
    }

    if (result is Future<*>) {
        result.onSuccess {
            processResponse(ctx, jsonMapper, false, errorHandler, it)
        }.onFailure {
            errorHandler.handleError(ctx, it)
        }
    } else if (result is Failable<*, *>) {
        result as Failable<Any?, Problem>
        if (Failable.isFailed(result)) {
            ctx.end(result.error)
        } else {
            sendResponse(ctx, jsonMapper, result.value)
        }
    } else if (result is Problem) {
        ctx.end(result, jsonMapper)
    } else {
        sendResponse(ctx, jsonMapper, result)
    }
}

private fun serializeResponse(jsonMapper: JsonMapper, response: Any?): Buffer? {
    return try {
        jsonMapper.writeValueAsBuffer(response)
    } catch (e: Exception) {
        logger.error("Failed to serialize response", e)
        null
    }
}

private fun sendResponse(ctx: RoutingContext, jsonMapper: JsonMapper, response: Any?) {
    // TODO Alternative serialization support
    try {
        if (response == null || response is Unit) {
            ctx.response().statusCode = 204
            ctx.end()
        } else {
            val res = serializeResponse(jsonMapper, response)
                ?: return ctx.end(Problem.internalError().build())

            ctx.response().putHeader("Content-Type", "application/json")
            ctx.response().statusCode = 200
            ctx.end(res)
        }
    } catch (e: Exception) {
        logger.error("Failed to send response", e)
        ctx.end(Problem.internalError().build())
    }
}

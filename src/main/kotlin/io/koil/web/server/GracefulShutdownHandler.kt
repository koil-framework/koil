package io.koil.web.server

import io.vertx.core.Handler
import io.vertx.core.Promise
import io.vertx.ext.web.RoutingContext

class GracefulShutdownHandler : Handler<RoutingContext> {
    var promise: Promise<Unit>? = null
    var shutdown = false
    var requestsInFlight: Int = 0
        private set

    override fun handle(event: RoutingContext) {
        if (shutdown) {
            // Refuse to process the request
            event.response()
                .setStatusCode(503)
                .end()
            return
        }

        requestsInFlight++
        try {
            event.next()
        } finally {
            requestsInFlight--
            if (requestsInFlight == 0 && promise != null) {
                promise!!.complete()
            }
        }
    }
}

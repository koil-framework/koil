package io.koil.core.cli

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class CommandLineArgsTest {
    @ParameterizedTest
    @MethodSource("args")
    fun testArgsParsing(args: Array<String>) {
        val cliArgs = CommandLineArgs.parse(args)

        assertEquals("dev", cliArgs.env)
        assertEquals(1, cliArgs.configPaths.size)
        assertEquals("/config", cliArgs.configPaths.first())
        assertNull(cliArgs.instanceId)
    }

    companion object {
        @JvmStatic
        fun args(): Stream<Arguments> = Stream.of(
            Arguments.of(arrayOf("-p=dev", "-c=/config")),
            Arguments.of(arrayOf("-c=/config", "-p=dev")),
        )
    }
}

package io.koil.core.config

import com.fasterxml.jackson.databind.json.JsonMapper
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class ConfigDefinitionTest {
    @Test
    fun testOverrideOrder() {
        val mapper = JsonMapper.builder().build()
        val future = ConfigDefinition.load(
            mapper, listOf(
                JsonConfigDefinition(
                    mapper.createObjectNode()
                        .put("field1", "test1")
                        .put("field2", "test2")
                        .put("field3", "test3")
                ),
                JsonConfigDefinition(
                    mapper.createObjectNode()
                        .put("field2", "test22")
                ),
                JsonConfigDefinition(
                    mapper.createObjectNode()
                        .put("field2", "test222")
                        .put("field3", "test333")
                ),
            )
        )

        val resolved = future.toCompletionStage().toCompletableFuture().join()
        assertEquals(
            mapper.createObjectNode()
                .put("field1", "test1")
                .put("field2", "test222")
                .put("field3", "test333"),
            resolved
        )
    }
}

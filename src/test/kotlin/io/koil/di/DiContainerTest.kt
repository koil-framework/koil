package io.koil.di

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

interface TestParent {
    val str: String
}

class TestDep(@param:Named("test") val str: String) {
    fun get(): String = str
}

class TestChild(val testDep: TestDep, override val str: String) : TestParent
class TestChild2(override val str: String) : TestParent

class TestDep2(val set: Set<TestParent>)

class DiContainerTest {
    @Test
    fun testGetInstance() {
        val container = DiContainer()
            .registerInstance("string")

        val instance = container.getInstance(String::class.java)
        assertEquals("string", instance)
    }

    @Test
    fun testGetInstanceFromParent() {
        val container = DiContainer()
            .registerInstance("string")
        val childContainer = container.createChild()

        val instance = childContainer.getInstance(String::class.java)
        assertEquals("string", instance)
    }

    @Test
    fun testGetInstanceOverrideFromParent() {
        val container = DiContainer()
            .registerInstance("string")
        val childContainer = container.createChild()
            .registerInstance("test")

        val instance = childContainer.getInstance(String::class.java)
        assertEquals("test", instance)
    }

    @Test
    fun testGetNamedInstance() {
        val container = DiContainer()
            .registerInstance("string", "named")

        val instance = container.getInstance(String::class.java, "named")
        assertEquals("string", instance)
    }

    @Test
    fun testGetNamedInstanceWithNullName() {
        val container = DiContainer()
            .registerInstance("string")

        val ex = assertThrows(DiFailedException::class.java) {
            container.getInstance(
                String::class.java,
                "named"
            )
        }
        assertEquals(String::class.java, ex.cls)
        assertEquals("named", ex.name)
    }

    @Test
    fun testGetNamedInstanceWithName() {
        val container = DiContainer()
            .registerInstance("string", "named")

        val ex = assertThrows(DiFailedException::class.java) { container.getInstance(String::class.java) }
        assertEquals(String::class.java, ex.cls)
        assertNull(ex.name)
    }

    @Test
    fun testDefaultRegistration() {
        val container = DiContainer()
            .registerInstance("test1", "test")
            .register(TestDep::class.java)

        val instance = container.getInstance(TestDep::class.java)
        assertEquals("test1", instance.str)
    }

    @Test
    fun testCtorRegistration() {
        val container = DiContainer()
            .register(TestDep::class.java, { TestDep("test2") })

        val instance = container.getInstance(TestDep::class.java)
        assertEquals("test2", instance.str)
    }

    @Test
    fun testInstanceScope() {
        val container = DiContainer()
            .register(TestDep::class.java, { TestDep("test2") }, DiScope.INSTANCE)

        val instance = container.getInstance(TestDep::class.java)
        assertEquals("test2", instance.str)

        val instance2 = container.getInstance(TestDep::class.java)
        assertEquals("test2", instance2.str)

        assertNotEquals(instance, instance2)
    }

    @Test
    fun testSingletonScope() {
        val container = DiContainer()
            .register(TestDep::class.java, { TestDep("test2") }, DiScope.SINGLETON)

        val instance = container.getInstance(TestDep::class.java)
        assertEquals("test2", instance.str)

        val instance2 = container.getInstance(TestDep::class.java)
        assertEquals("test2", instance2.str)

        assertEquals(instance, instance2)
    }

    @Test
    fun testGetInstanceUsingChildClass() {
        val container = DiContainer()
            .register(TestDep::class.java, { TestDep("test2") })
            .registerInstance("teststr")
            .register(TestChild::class.java)

        val instance = container.getInstance(TestChild::class.java)
        assertEquals("teststr", instance.str)

        val depInstance = container.getInstance(TestDep::class.java)
        assertEquals(depInstance, instance.testDep)
    }

    @Test
    fun testGetInstanceUsingParentClass() {
        val container = DiContainer()
            .register(TestDep::class.java, { TestDep("test2") })
            .registerInstance("teststr")
            .register(TestChild::class.java)

        val instance = container.getInstance(TestParent::class.java)
        assertEquals("teststr", instance.str)
        assertInstanceOf(TestChild::class.java, instance)

        val depInstance = container.getInstance(TestDep::class.java)
        assertEquals(depInstance, (instance as TestChild).testDep)
    }

    @Test
    fun testMultipleChildrenOverride() {
        val container = DiContainer()
            .register(TestDep::class.java, { TestDep("test2") })
            .registerInstance("teststr")
            .register(TestChild::class.java)
        val childContainer = container.createChild()
            .register(TestChild2::class.java)

        val instance = childContainer.getInstance(TestParent::class.java)
        assertEquals("teststr", instance.str)
        assertInstanceOf(TestChild2::class.java, instance)
    }

    @Test
    fun testGetAll() {
        val container = DiContainer()
            .register(TestDep::class.java, { TestDep("test2") })
            .registerInstance("teststr")
            .register(TestChild::class.java)
        val childContainer = container.createChild()
            .register(TestChild2::class.java)

        val instances = childContainer.getAll(TestParent::class.java).toList()
        assertEquals(2, instances.size)
        assertInstanceOf(TestChild2::class.java, instances[0])
        assertInstanceOf(TestChild::class.java, instances[1])
    }

    @Test
    fun testGetAllNamed() {
        val container = DiContainer()
            .register(TestDep::class.java, { TestDep("test2") })
            .registerInstance("teststr")
            .register(TestChild::class.java, name = "named")
        val childContainer = container.createChild()
            .register(TestChild2::class.java)

        val instances = childContainer.getAll(TestParent::class.java, setOf("named")).toList()
        assertEquals(1, instances.size)
        assertInstanceOf(TestChild::class.java, instances[0])
    }

    @Test
    fun testGetAllWithoutName() {
        val container = DiContainer()
            .register(TestDep::class.java, { TestDep("test2") })
            .registerInstance("teststr")
            .register(TestChild::class.java, name = "named")
        val childContainer = container.createChild()
            .register(TestChild2::class.java)

        val instances = childContainer.getAll(TestParent::class.java).toList()
        assertEquals(2, instances.size)
        assertInstanceOf(TestChild2::class.java, instances[0])
        assertInstanceOf(TestChild::class.java, instances[1])
    }

    @Test
    fun testProperErrorPropagation() {
        val container = DiContainer()
            .register(TestDep::class.java)
            .registerInstance("teststr")
            .register(TestChild::class.java)

        val ex = assertThrows(DiFailedException::class.java) { container.getInstance(TestChild::class.java) }
        assertEquals(TestChild::class.java, ex.cls)
        assertNull(ex.name)
        assertNotNull(ex.cause)

        val cause = ex.cause
        assertInstanceOf(DiFailedException::class.java, cause)
        cause as DiFailedException
        assertEquals(TestDep::class.java, cause.cls)
        assertNull(cause.name)
        assertNotNull(cause.cause)

        val parentCause = cause.cause
        assertInstanceOf(DiFailedException::class.java, parentCause)
        parentCause as DiFailedException
        assertEquals(String::class.java, parentCause.cls)
        assertEquals("test", parentCause.name)
        assertNull(parentCause.cause)
    }

    @Test
    fun testSetAutoInjection() {
        val container = DiContainer()
            .register(TestDep::class.java, { TestDep("test2") })
            .registerInstance("teststr")
            .register(TestChild::class.java)
        val childContainer = container.createChild()
            .register(TestChild2::class.java)
            .register<Set<TestParent>>({ it.getAll() })
            .register(TestDep2::class.java)

        val instance = childContainer.getInstance(TestDep2::class.java)
        assertEquals(2, instance.set.size)
    }

    @Test
    fun testInjectDiContainerInstance() {
        val container = DiContainer()
        val childContainer = container.createChild()

        val instance = childContainer.get<DiContainer>()
        assertTrue(childContainer == instance)
    }

    @Test
    fun testInjectNamedDiContainerInstance() {
        val container = DiContainer()
        val childContainer = container.createChild()

        val instance = childContainer.get<DiContainer>("test")
        assertTrue(childContainer == instance)
    }
}

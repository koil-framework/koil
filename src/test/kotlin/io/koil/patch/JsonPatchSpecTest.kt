package io.koil.patch

import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.databind.node.*
import io.koil.schemas.JsonSchema
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class JsonPatchSpecTest {
    @ParameterizedTest(name = "{0}")
    @MethodSource("specSource")
    fun jsonPatchSpecTest(
        name: String,
        doc: ObjectNode,
        schema: JsonSchema,
        patches: List<JsonPatch>,
        expected: ObjectNode,
        error: Boolean,
    ) {
        val obj = doc.deepCopy()
        var err = false
        for (patch in patches) {
            if (!patch.apply(schema, obj)) {
                err = true
                break
            }
        }

        assertEquals(error, err)
        assertEquals(expected, obj)
    }

    companion object {
        private val jsonMapper = JsonMapper.builder()
            .build()

        @JvmStatic
        fun specSource(): Stream<Arguments> = Stream.of(
            // Json conformance
            Arguments.of(
                "empty list, empty docs",
                jsonMapper.readTree("{}") as ObjectNode,
                JsonSchema.obj("Obj").build(),
                emptyList<JsonPatch>(),
                jsonMapper.readTree("{}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "empty patch list",
                jsonMapper.readTree("{\"foo\": 1}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.int())
                    .build(),
                emptyList<JsonPatch>(),
                jsonMapper.readTree("{\"foo\": 1}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "rearrangements OK?",
                jsonMapper.readTree("{\"foo\": 1, \"bar\": 2}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.int())
                    .property("bar", JsonSchema.int())
                    .build(),
                emptyList<JsonPatch>(),
                jsonMapper.readTree("{\"bar\":2, \"foo\": 1}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "rearrangements OK?  How about one level down ... array",
                jsonMapper.readTree("{\"value\":[{\"foo\": 1, \"bar\": 2}]}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property(
                        "value",
                        JsonSchema.array(
                            JsonSchema.obj("Nested")
                                .property("foo", JsonSchema.int())
                                .property("bar", JsonSchema.int())
                        )
                    )
                    .build(),
                emptyList<JsonPatch>(),
                jsonMapper.readTree("{\"value\":[{\"bar\":2, \"foo\": 1}]}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "rearrangements OK?  How about one level down ...",
                jsonMapper.readTree("{\"foo\":{\"foo\": 1, \"bar\": 2}}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property(
                        "foo",
                        JsonSchema.obj("Nested")
                            .property("foo", JsonSchema.int())
                            .property("bar", JsonSchema.int())
                    )
                    .build(),
                emptyList<JsonPatch>(),
                jsonMapper.readTree("{\"foo\":{\"bar\":2, \"foo\": 1}}") as ObjectNode,
                false,
            ),

            //
            Arguments.of(
                "add replaces any existing field",
                jsonMapper.readTree("{\"foo\": null}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.int(), true)
                    .build(),
                listOf(
                    AddJsonPatch(JsonPath.parse("/foo")!!, IntNode.valueOf(1))
                ),
                jsonMapper.readTree("{\"foo\": 1}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "toplevel object, numeric string",
                jsonMapper.readTree("{}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.string(), true)
                    .build(),
                listOf(
                    AddJsonPatch(JsonPath.parse("/foo")!!, TextNode.valueOf("1"))
                ),
                jsonMapper.readTree("{\"foo\":\"1\"}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "toplevel object, integer",
                jsonMapper.readTree("{}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.int(), true)
                    .build(),
                listOf(
                    AddJsonPatch(JsonPath.parse("/foo")!!, IntNode.valueOf(1))
                ),
                jsonMapper.readTree("{\"foo\":1}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "Add, / target",
                jsonMapper.readTree("{}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("", JsonSchema.int(), true)
                    .build(),
                listOf(
                    AddJsonPatch(JsonPath.parse("/")!!, IntNode.valueOf(1))
                ),
                jsonMapper.readTree("{\"\":1}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "Add, /foo/ deep target (trailing slash)",
                jsonMapper.readTree("{\"foo\": {}}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.obj("Nested").property("", JsonSchema.int(), true))
                    .build(),
                listOf(
                    AddJsonPatch(JsonPath.parse("/foo/")!!, IntNode.valueOf(1))
                ),
                jsonMapper.readTree("{\"foo\":{\"\": 1}}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "Add composite value at top level",
                jsonMapper.readTree("{\"foo\": 1}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.int())
                    .property("bar", JsonSchema.array(JsonSchema.int()), true)
                    .build(),
                listOf(
                    AddJsonPatch(JsonPath.parse("/bar")!!, JsonNodeFactory.instance.arrayNode().add(1).add(2))
                ),
                jsonMapper.readTree("{\"foo\": 1, \"bar\": [1, 2]}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "Add into composite value",
                jsonMapper.readTree("{\"foo\": 1, \"baz\": [{\"qux\": \"hello\"}]}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.int())
                    .property(
                        "baz", JsonSchema.array(
                            JsonSchema.obj("Nested")
                                .property("qux", JsonSchema.string())
                                .property("foo", JsonSchema.string(), true)
                        )
                    )
                    .build(),
                listOf(
                    AddJsonPatch(JsonPath.parse("/baz/0/foo")!!, TextNode.valueOf("world"))
                ),
                jsonMapper.readTree("{\"foo\": 1, \"baz\": [{\"qux\": \"hello\", \"foo\": \"world\"}]}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "Out of bounds (upper)",
                jsonMapper.readTree("{\"bar\": [1, 2]}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("bar", JsonSchema.array(JsonSchema.int()))
                    .build(),
                listOf(
                    AddJsonPatch(JsonPath.parse("/bar/8")!!, IntNode.valueOf(5))
                ),
                jsonMapper.readTree("{\"bar\": [1, 2]}") as ObjectNode,
                true,
            ),
            Arguments.of(
                "Out of bounds (lower)",
                jsonMapper.readTree("{\"bar\": [1, 2]}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("bar", JsonSchema.array(JsonSchema.int()))
                    .build(),
                listOf(
                    AddJsonPatch(JsonPath.parse("/bar/-1")!!, IntNode.valueOf(5))
                ),
                jsonMapper.readTree("{\"bar\": [1, 2]}") as ObjectNode,
                true,
            ),
            Arguments.of(
                "Add, boolean true",
                jsonMapper.readTree("{\"foo\": 1}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.int())
                    .property("bar", JsonSchema.boolean(), true)
                    .build(),
                listOf(
                    AddJsonPatch(JsonPath.parse("/bar")!!, BooleanNode.TRUE)
                ),
                jsonMapper.readTree("{\"foo\": 1, \"bar\": true}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "Add, boolean false",
                jsonMapper.readTree("{\"foo\": 1}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.int())
                    .property("bar", JsonSchema.boolean(), true)
                    .build(),
                listOf(
                    AddJsonPatch(JsonPath.parse("/bar")!!, BooleanNode.FALSE)
                ),
                jsonMapper.readTree("{\"foo\": 1, \"bar\": false}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "Add, null",
                jsonMapper.readTree("{\"foo\": 1}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.int())
                    .property("bar", JsonSchema.boolean(), true)
                    .build(),
                listOf(
                    AddJsonPatch(JsonPath.parse("/bar")!!, NullNode.instance)
                ),
                jsonMapper.readTree("{\"foo\": 1, \"bar\": null}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "0 can be an array index or object element name",
                jsonMapper.readTree("{\"foo\": 1}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.int())
                    .property("0", JsonSchema.string(), true)
                    .build(),
                listOf(
                    AddJsonPatch(JsonPath.parse("/0")!!, TextNode.valueOf("bar"))
                ),
                jsonMapper.readTree("{\"foo\": 1, \"0\": \"bar\"}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "test against implementation-specific numeric parsing",
                jsonMapper.readTree("{\"1e0\": \"foo\"}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("1e0", JsonSchema.string(), true)
                    .build(),
                listOf(
                    TestJsonPatch(JsonPath.parse("/1e0")!!, TextNode.valueOf("foo"))
                ),
                jsonMapper.readTree("{\"1e0\": \"foo\"}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "Remove, array",
                jsonMapper.readTree("{\"foo\": 1, \"bar\": [1, 2, 3, 4]}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.int())
                    .property("bar", JsonSchema.array(JsonSchema.int()), true)
                    .build(),
                listOf(
                    RemoveJsonPatch(JsonPath.parse("/bar")!!)
                ),
                jsonMapper.readTree("{\"foo\": 1}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "Remove, nested object property in array item",
                jsonMapper.readTree("{\"foo\": 1, \"baz\": [{\"qux\": \"hello\"}]}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.int())
                    .property(
                        "baz",
                        JsonSchema.array(
                            JsonSchema.obj("Nested")
                                .property("qux", JsonSchema.string(), true)
                        )
                    )
                    .build(),
                listOf(
                    RemoveJsonPatch(JsonPath.parse("/baz/0/qux")!!)
                ),
                jsonMapper.readTree("{\"foo\": 1, \"baz\": [{}]}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "Replace, nested object property in array item",
                jsonMapper.readTree("{\"foo\": [1, 2, 3, 4], \"baz\": [{\"qux\": \"hello\"}]}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.array(JsonSchema.int()))
                    .property(
                        "baz",
                        JsonSchema.array(
                            JsonSchema.obj("Nested")
                                .property("qux", JsonSchema.string())
                        )
                    )
                    .build(),
                listOf(
                    ReplaceJsonPatch(JsonPath.parse("/baz/0/qux")!!, TextNode.valueOf("world"))
                ),
                jsonMapper.readTree("{\"foo\": [1, 2, 3, 4], \"baz\": [{\"qux\": \"world\"}]}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "test replace with missing parent key should fail",
                jsonMapper.readTree("{\"foo\": \"bar\"}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.string())
                    .property(
                        "foo",
                        JsonSchema.obj("Nested")
                            .property("bar", JsonSchema.boolean())
                    )
                    .build(),
                listOf(
                    ReplaceJsonPatch(JsonPath.parse("/foo/bar")!!, BooleanNode.FALSE)
                ),
                jsonMapper.readTree("{\"foo\": \"bar\"}") as ObjectNode,
                true,
            ),
            Arguments.of(
                "null value should be valid obj property",
                jsonMapper.readTree("{\"foo\": null}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.string(), true)
                    .build(),
                listOf(
                    TestJsonPatch(JsonPath.parse("/foo")!!, NullNode.instance)
                ),
                jsonMapper.readTree("{\"foo\": null}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "null value should be valid obj property to be replaced with something truthy",
                jsonMapper.readTree("{\"foo\": null}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.string(), true)
                    .build(),
                listOf(
                    ReplaceJsonPatch(JsonPath.parse("/foo")!!, TextNode.valueOf("truthy"))
                ),
                jsonMapper.readTree("{\"foo\": \"truthy\"}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "null value should be valid obj property to be moved",
                jsonMapper.readTree("{\"foo\": null}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.string(), true)
                    .property("bar", JsonSchema.string(), true)
                    .build(),
                listOf(
                    MoveJsonPatch(JsonPath.parse("/bar")!!, JsonPath.parse("/foo")!!)
                ),
                jsonMapper.readTree("{\"bar\": null}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "null value should be valid obj property to be moved",
                jsonMapper.readTree("{\"foo\": null}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.string(), true)
                    .property("bar", JsonSchema.string(), true)
                    .build(),
                listOf(
                    CopyJsonPatch(JsonPath.parse("/bar")!!, JsonPath.parse("/foo")!!)
                ),
                jsonMapper.readTree("{\"foo\": null, \"bar\": null}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "null value should be valid obj property to be moved",
                jsonMapper.readTree("{\"foo\": null}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.string(), true)
                    .build(),
                listOf(
                    RemoveJsonPatch(JsonPath.parse("/foo")!!)
                ),
                jsonMapper.readTree("{}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "null value should still be valid obj property replace other value",
                jsonMapper.readTree("{\"foo\": \"bar\"}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.string(), true)
                    .build(),
                listOf(
                    ReplaceJsonPatch(JsonPath.parse("/foo")!!, NullNode.instance)
                ),
                jsonMapper.readTree("{\"foo\": null}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "test should pass despite rearrangement",
                jsonMapper.readTree("{\"foo\": {\"foo\": 1, \"bar\": 2}}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property(
                        "foo",
                        JsonSchema.obj("Nested")
                            .property("foo", JsonSchema.int())
                            .property("bar", JsonSchema.int())
                    )
                    .build(),
                listOf(
                    TestJsonPatch(JsonPath.parse("/foo")!!, jsonMapper.readTree("{\"bar\": 2, \"foo\": 1}"))
                ),
                jsonMapper.readTree("{\"foo\": {\"foo\": 1, \"bar\": 2}}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "test should pass despite (nested) rearrangement",
                jsonMapper.readTree("{\"foo\": [{\"foo\": 1, \"bar\": 2}]}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property(
                        "foo",
                        JsonSchema.array(
                            JsonSchema.obj("Nested")
                                .property("foo", JsonSchema.int())
                                .property("bar", JsonSchema.int())
                        )
                    )
                    .build(),
                listOf(
                    TestJsonPatch(JsonPath.parse("/foo")!!, jsonMapper.readTree("[{\"bar\": 2, \"foo\": 1}]"))
                ),
                jsonMapper.readTree("{\"foo\": [{\"foo\": 1, \"bar\": 2}]}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "test remove on array",
                jsonMapper.readTree("{\"foo\": [1, 2, 3, 4]}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.array(JsonSchema.int()))
                    .build(),
                listOf(
                    RemoveJsonPatch(JsonPath.parse("/foo/0")!!)
                ),
                jsonMapper.readTree("{\"foo\": [2, 3, 4]}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "test repeated removes",
                jsonMapper.readTree("{\"foo\": [1, 2, 3, 4]}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.array(JsonSchema.int()))
                    .build(),
                listOf(
                    RemoveJsonPatch(JsonPath.parse("/foo/1")!!),
                    RemoveJsonPatch(JsonPath.parse("/foo/2")!!)
                ),
                jsonMapper.readTree("{\"foo\": [1, 3]}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "Move to same location has no effect",
                jsonMapper.readTree("{\"foo\": 1}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.int())
                    .build(),
                listOf(
                    MoveJsonPatch(JsonPath.parse("/foo")!!, JsonPath.parse("/foo")!!),
                ),
                jsonMapper.readTree("{\"foo\": 1}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "A.2.  Adding an Array Element",
                jsonMapper.readTree("{\"foo\": [\"bar\", \"baz\"]}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.array(JsonSchema.string()))
                    .build(),
                listOf(
                    AddJsonPatch(JsonPath.parse("/foo/1")!!, TextNode.valueOf("qux"))
                ),
                jsonMapper.readTree("{\"foo\": [\"bar\", \"qux\", \"baz\"]}") as ObjectNode,
                false,
            ),
            Arguments.of(
                "A.7.  Moving an Array Element",
                jsonMapper.readTree("{\"foo\": [\"all\", \"grass\", \"cows\", \"eat\"]}") as ObjectNode,
                JsonSchema.obj("Obj")
                    .property("foo", JsonSchema.array(JsonSchema.string()))
                    .build(),
                listOf(
                    MoveJsonPatch(JsonPath.parse("/foo/3")!!, JsonPath.parse("/foo/1")!!)
                ),
                jsonMapper.readTree("{\"foo\": [\"all\", \"cows\", \"eat\", \"grass\"]}") as ObjectNode,
                false,
            ),
        )
    }
}


/*
[
  {
    "comment": "4.1. add with missing object",
    "doc": { "q": { "bar": 2 } },
    "patch": [ {"op": "add", "path": "/a/b", "value": 1} ],
    "error":
       "path /a does not exist -- missing objects are not created recursively"
  },

  {
    "comment": "A.6.  Moving a Value",
    "doc": {
  "foo": {
    "bar": "baz",
    "waldo": "fred"
  },
  "qux": {
    "corge": "grault"
  }
},
    "patch": [
  { "op": "move", "from": "/foo/waldo", "path": "/qux/thud" }
],
    "expected": {
  "foo": {
    "bar": "baz"
  },
  "qux": {
    "corge": "grault",
    "thud": "fred"
  }
}
  },

  {
    "comment": "A.8.  Testing a Value: Success",
    "doc": {
  "baz": "qux",
  "foo": [ "a", 2, "c" ]
},
    "patch": [
  { "op": "test", "path": "/baz", "value": "qux" },
  { "op": "test", "path": "/foo/1", "value": 2 }
],
    "expected": {
     "baz": "qux",
     "foo": [ "a", 2, "c" ]
    }
  },

  {
    "comment": "A.9.  Testing a Value: Error",
    "doc": {
  "baz": "qux"
},
    "patch": [
  { "op": "test", "path": "/baz", "value": "bar" }
],
    "error": "string not equivalent"
  },

  {
    "comment": "A.10.  Adding a nested Member Object",
    "doc": {
  "foo": "bar"
},
    "patch": [
  { "op": "add", "path": "/child", "value": { "grandchild": { } } }
],
    "expected": {
  "foo": "bar",
  "child": {
    "grandchild": {
    }
  }
}
  },

  {
    "comment": "A.11.  Ignoring Unrecognized Elements",
    "doc": {
  "foo":"bar"
},
    "patch": [
  { "op": "add", "path": "/baz", "value": "qux", "xyz": 123 }
],
    "expected": {
  "foo":"bar",
  "baz":"qux"
}
  },

 {
    "comment": "A.12.  Adding to a Non-existent Target",
    "doc": {
  "foo": "bar"
},
    "patch": [
  { "op": "add", "path": "/baz/bat", "value": "qux" }
],
    "error": "add to a non-existent target"
  },

 {
    "comment": "A.13 Invalid JSON Patch Document",
    "doc": {
     "foo": "bar"
    },
    "patch": [
  { "op": "add", "path": "/baz", "value": "qux", "op": "remove" }
],
    "error": "operation has two 'op' members",
    "disabled": true
  },

  {
    "comment": "A.14. ~ Escape Ordering",
    "doc": {
       "/": 9,
       "~1": 10
    },
    "patch": [{"op": "test", "path": "/~01", "value": 10}],
    "expected": {
       "/": 9,
       "~1": 10
    }
  },

  {
    "comment": "A.15. Comparing Strings and Numbers",
    "doc": {
       "/": 9,
       "~1": 10
    },
    "patch": [{"op": "test", "path": "/~01", "value": "10"}],
    "error": "number is not equal to string"
  },

  {
    "comment": "A.16. Adding an Array Value",
    "doc": {
       "foo": ["bar"]
    },
    "patch": [{ "op": "add", "path": "/foo/-", "value": ["abc", "def"] }],
    "expected": {
      "foo": ["bar", ["abc", "def"]]
    }
  }

]


    { "doc": {"foo": {"bar": [1, 2, 5, 4]}},
      "patch": [{"op": "test", "path": "/foo", "value": {"bar": [1, 2, 5, 4]}}],
      "expected": {"foo": {"bar": [1, 2, 5, 4]}},
      "comment": "test should pass - no error" },

    { "doc": {"foo": {"bar": [1, 2, 5, 4]}},
      "patch": [{"op": "test", "path": "/foo", "value": [1, 2]}],
      "error": "test op should fail" },

    { "comment": "Empty-string element",
      "doc": { "": 1 },
      "patch": [{"op": "test", "path": "/", "value": 1}],
      "expected": { "": 1 } },

    { "doc": {
            "foo": ["bar", "baz"],
            "": 0,
            "a/b": 1,
            "c%d": 2,
            "e^f": 3,
            "g|h": 4,
            "i\\j": 5,
            "k\"l": 6,
            " ": 7,
            "m~n": 8
            },
      "patch": [{"op": "test", "path": "/foo", "value": ["bar", "baz"]},
                {"op": "test", "path": "/foo/0", "value": "bar"},
                {"op": "test", "path": "/", "value": 0},
                {"op": "test", "path": "/a~1b", "value": 1},
                {"op": "test", "path": "/c%d", "value": 2},
                {"op": "test", "path": "/e^f", "value": 3},
                {"op": "test", "path": "/g|h", "value": 4},
                {"op": "test", "path":  "/i\\j", "value": 5},
                {"op": "test", "path": "/k\"l", "value": 6},
                {"op": "test", "path": "/ ", "value": 7},
                {"op": "test", "path": "/m~0n", "value": 8}],
      "expected": {
            "": 0,
            " ": 7,
            "a/b": 1,
            "c%d": 2,
            "e^f": 3,
            "foo": [
                "bar",
                "baz"
            ],
            "g|h": 4,
            "i\\j": 5,
            "k\"l": 6,
            "m~n": 8
        }
    },

    { "doc": {"foo": 1, "baz": [{"qux": "hello"}]},
      "patch": [{"op": "move", "from": "/foo", "path": "/bar"}],
      "expected": {"baz": [{"qux": "hello"}], "bar": 1} },

    { "doc": {"baz": [{"qux": "hello"}], "bar": 1},
      "patch": [{"op": "copy", "from": "/baz/0", "path": "/boo"}],
      "expected": {"baz":[{"qux":"hello"}],"bar":1,"boo":{"qux":"hello"}} },

    { "comment": "Adding to \"/-\" adds to the end of the array",
      "doc": [ 1, 2 ],
      "patch": [ { "op": "add", "path": "/-", "value": { "foo": [ "bar", "baz" ] } } ],
      "expected": [ 1, 2, { "foo": [ "bar", "baz" ] } ]},

    { "comment": "Adding to \"/-\" adds to the end of the array, even n levels down",
      "doc": [ 1, 2, [ 3, [ 4, 5 ] ] ],
      "patch": [ { "op": "add", "path": "/2/1/-", "value": { "foo": [ "bar", "baz" ] } } ],
      "expected": [ 1, 2, [ 3, [ 4, 5, { "foo": [ "bar", "baz" ] } ] ] ]},

    { "comment": "test remove with bad number should fail",
      "doc": {"foo": 1, "baz": [{"qux": "hello"}]},
      "patch": [{"op": "remove", "path": "/baz/1e0/qux"}],
      "error": "remove op shouldn't remove from array with bad number" },

    { "comment": "test remove with bad index should fail",
      "doc": [1, 2, 3, 4],
      "patch": [{"op": "remove", "path": "/1e0"}],
      "error": "remove op shouldn't remove from array with bad number" },

    { "comment": "test replace with bad number should fail",
      "doc": [""],
      "patch": [{"op": "replace", "path": "/1e0", "value": false}],
      "error": "replace op shouldn't replace in array with bad number" },

    { "comment": "test copy with bad number should fail",
      "doc": {"baz": [1,2,3], "bar": 1},
      "patch": [{"op": "copy", "from": "/baz/1e0", "path": "/boo"}],
      "error": "copy op shouldn't work with bad number" },

    { "comment": "test move with bad number should fail",
      "doc": {"foo": 1, "baz": [1,2,3,4]},
      "patch": [{"op": "move", "from": "/baz/1e0", "path": "/foo"}],
      "error": "move op shouldn't work with bad number" },

    { "comment": "test add with bad number should fail",
      "doc": ["foo", "sil"],
      "patch": [{"op": "add", "path": "/1e0", "value": "bar"}],
      "error": "add op shouldn't add to array with bad number" },

    { "comment": "missing from location to copy",
      "doc": { "foo": 1 },
      "patch": [ { "op": "copy", "from": "/bar", "path": "/foo" } ],
      "error": "missing 'from' location" },

    { "comment": "missing from location to move",
      "doc": { "foo": 1 },
      "patch": [ { "op": "move", "from": "/bar", "path": "/foo" } ],
      "error": "missing 'from' location" },

    { "comment": "Removing nonexistent field",
      "doc": {"foo" : "bar"},
      "patch": [{"op": "remove", "path": "/baz"}],
      "error": "removing a nonexistent field should fail" },

    { "comment": "Removing deep nonexistent path",
      "doc": {"foo" : "bar"},
      "patch": [{"op": "remove", "path": "/missing1/missing2"}],
      "error": "removing a nonexistent field should fail" },

    { "comment": "Patch with different capitalisation than doc",
       "doc": {"foo":"bar"},
       "patch": [{"op": "add", "path": "/FOO", "value": "BAR"}],
       "expected": {"foo": "bar", "FOO": "BAR"}
    }
]
 */

/*
TODO when feature is implemented
{ "comment": "replace whole document",
  "doc": {"foo": "bar"},
  "patch": [{"op": "replace", "path": "", "value": {"baz": "qux"}}],
  "expected": {"baz": "qux"} },
{ "comment": "Whole document",
  "doc": { "foo": 1 },
  "patch": [{"op": "test", "path": "", "value": {"foo": 1}}],
  "disabled": true },
{ "comment": "replacing the root of the document is possible with add",
  "doc": {"foo": "bar"},
  "patch": [{"op": "add", "path": "", "value": {"baz": "qux"}}],
  "expected": {"baz":"qux"}},
 */

/*
???

{ "comment": "spurious patch properties",
      "doc": {"foo": 1},
      "patch": [{"op": "test", "path": "/foo", "value": 1, "spurious": 1}],
      "expected": {"foo": 1} },
 */

/*
[
    { "comment": "toplevel array, no change",
      "doc": ["foo"],
      "patch": [],
      "expected": ["foo"] },

    { "comment": "toplevel array",
      "doc": [],
      "patch": [{"op": "add", "path": "/0", "value": "foo"}],
      "expected": ["foo"] },
    { "comment": "Toplevel scalar values OK?",
      "doc": "foo",
      "patch": [{"op": "replace", "path": "", "value": "bar"}],
      "expected": "bar",
      "disabled": true },
    { "comment": "replace object document with array document?",
      "doc": {},
      "patch": [{"op": "add", "path": "", "value": []}],
      "expected": [] },

    { "comment": "replace array document with object document?",
      "doc": [],
      "patch": [{"op": "add", "path": "", "value": {}}],
      "expected": {} },

    { "comment": "append to root array document?",
      "doc": [],
      "patch": [{"op": "add", "path": "/-", "value": "hi"}],
      "expected": ["hi"] },

    { "doc": ["foo"],
      "patch": [{"op": "add", "path": "/1", "value": "bar"}],
      "expected": ["foo", "bar"] },

    { "doc": ["foo", "sil"],
      "patch": [{"op": "add", "path": "/1", "value": "bar"}],
      "expected": ["foo", "bar", "sil"] },

    { "doc": ["foo", "sil"],
      "patch": [{"op": "add", "path": "/0", "value": "bar"}],
      "expected": ["bar", "foo", "sil"] },

    { "comment": "push item to array via last index + 1",
      "doc": ["foo", "sil"],
      "patch": [{"op":"add", "path": "/2", "value": "bar"}],
      "expected": ["foo", "sil", "bar"] },

    { "comment": "add item to array at index > length should fail",
      "doc": ["foo", "sil"],
      "patch": [{"op":"add", "path": "/3", "value": "bar"}],
      "error": "index is greater than number of items in array" },
    { "comment": "test with bad number should fail",
      "doc": ["foo", "bar"],
      "patch": [{"op": "test", "path": "/1e0", "value": "bar"}],
      "error": "test op shouldn't get array element 1" },

    { "doc": ["foo", "sil"],
      "patch": [{"op": "add", "path": "/bar", "value": 42}],
      "error": "Object operation on array target" },

    { "doc": ["foo", "sil"],
      "patch": [{"op": "add", "path": "/1", "value": ["bar", "baz"]}],
      "expected": ["foo", ["bar", "baz"], "sil"],
      "comment": "value in array add not flattened" },
    { "doc": ["foo"],
      "patch": [{"op": "replace", "path": "/0", "value": "bar"}],
      "expected": ["bar"] },

    { "doc": [""],
      "patch": [{"op": "replace", "path": "/0", "value": 0}],
      "expected": [0] },

    { "doc": [""],
      "patch": [{"op": "replace", "path": "/0", "value": true}],
      "expected": [true] },

    { "doc": [""],
      "patch": [{"op": "replace", "path": "/0", "value": false}],
      "expected": [false] },

    { "doc": [""],
      "patch": [{"op": "replace", "path": "/0", "value": null}],
      "expected": [null] },

    { "doc": ["foo", "sil"],
      "patch": [{"op": "replace", "path": "/1", "value": ["bar", "baz"]}],
      "expected": ["foo", ["bar", "baz"]],
      "comment": "value in array replace not flattened" },

    { "comment": "test with bad array number that has leading zeros",
      "doc": ["foo", "bar"],
      "patch": [{"op": "test", "path": "/00", "value": "foo"}],
      "error": "test op should reject the array value, it has leading zeros" },

    { "comment": "test with bad array number that has leading zeros",
      "doc": ["foo", "bar"],
      "patch": [{"op": "test", "path": "/01", "value": "bar"}],
      "error": "test op should reject the array value, it has leading zeros" },
    { "comment": "Removing nonexistent index",
      "doc": ["foo", "bar"],
      "patch": [{"op": "remove", "path": "/2"}],
      "error": "removing a nonexistent index should fail" },
    { "comment": "test remove on array",
      "doc": [1, 2, 3, 4],
      "patch": [{"op": "remove", "path": "/0"}],
      "expected": [2, 3, 4] },

    { "comment": "test repeated removes",
      "doc": [1, 2, 3, 4],
      "patch": [{ "op": "remove", "path": "/1" },
                { "op": "remove", "path": "/2" }],
      "expected": [1, 3] },
]
 */

/*
Those can't work because we have strong typings and schema validation

{ "doc": {"foo": 1, "baz": [{"qux": "hello"}]},
  "patch": [{"op": "replace", "path": "/foo", "value": [1, 2, 3, 4]}],
  "expected": {"foo": [1, 2, 3, 4], "baz": [{"qux": "hello"}]} },
{ "doc": {"baz": [{"qux": "hello"}], "bar": 1},
  "patch": [{"op": "move", "from": "/baz/0/qux", "path": "/baz/1"}],
  "expected": {"baz": [{}, "hello"], "bar": 1} },
 */

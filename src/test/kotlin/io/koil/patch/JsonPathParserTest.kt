package io.koil.patch

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class JsonPathParserTest {
    @Test
    fun testParseSimpleJsonPointer() {
        val ptr = "/test"

        val path = JsonPath.parse(ptr)
        assertNotNull(path)
        path!!
        assertTrue(path.pointer)
        assertEquals(ptr, path.original)
        assertFalse(path.targetsMultipleElements)
        assertEquals(ptr, path.toString())

        val head = assertSegment(path.head, "test")
        assertNull(head.previous)

        val tail = assertSegment(path.tail, "test")
        assertNull(tail.next)
    }

    @Test
    fun testParseNestedJsonPointer() {
        val ptr = "/test/subPath"

        val path = JsonPath.parse(ptr)
        assertNotNull(path)
        path!!
        assertTrue(path.pointer)
        assertEquals(ptr, path.original)
        assertFalse(path.targetsMultipleElements)
        assertEquals(ptr, path.toString())

        val head = assertSegment(path.head, "subPath")
        val testSegment = assertSegment(head.previous, "test")
        assertNull(testSegment.previous)

        val tail = assertSegment(path.tail, "test")
        val subPathSegment = assertSegment(tail.next, "subPath")
        assertNull(subPathSegment.next)
    }

    @Test
    fun testParseArrayIndexJsonPointer() {
        val ptr = "/test/0/subPath"

        val path = JsonPath.parse(ptr)
        assertNotNull(path)
        path!!
        assertTrue(path.pointer)
        assertEquals(ptr, path.original)
        assertFalse(path.targetsMultipleElements)
        assertEquals(ptr, path.toString())

        val head = assertSegment(path.head, "subPath")
        val arrayIndexSegment = assertSegment(head.previous, "0")
        assertInstanceOf(JsonPathSegment.Property::class.java, arrayIndexSegment)
        arrayIndexSegment as JsonPathSegment.Property
        assertTrue(arrayIndexSegment.maybeArrayIndex)
        val arrayIndex = arrayIndexSegment.arrayIndex
        assertNotNull(arrayIndex)
        assertEquals(0, arrayIndex)
        val testSegment = assertSegment(arrayIndexSegment.previous, "test")
        assertNull(testSegment.previous)

        val tail = assertSegment(path.tail, "test")
        val iSegment = assertSegment(tail.next, "0")
        val subPathSegment = assertSegment(iSegment.next, "subPath")
        assertNull(subPathSegment.next)
    }

    private fun assertSegment(segment: JsonPathSegment?, expected: String): JsonPathSegment {
        assertNotNull(segment)
        segment!!
        assertEquals(expected, segment.content)
        return segment
    }
}

package io.koil.patch

import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.databind.node.TextNode
import io.koil.schemas.JsonSchema
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class JsonPatchTest {
    @Test
    fun testPathValidation() {
        val schema = JsonSchema.obj("Test")
            .property("someProp", JsonSchema.string())
            .build()
        val ptr = "/someProp"

        val path = JsonPath.parse(ptr)
        assertNotNull(path)
        val patch = AddJsonPatch(path!!, TextNode.valueOf("test"))

        assertTrue(patch.validate(schema))
    }

    @Test
    fun testInvalidPathValidation() {
        val schema = JsonSchema.obj("Test")
            .property("someProp", JsonSchema.string())
            .build()
        val ptr = "/someProp2"

        val path = JsonPath.parse(ptr)
        assertNotNull(path)
        val patch = AddJsonPatch(path!!, TextNode.valueOf("test"))

        assertFalse(patch.validate(schema))
    }

    @Test
    fun testObjectPathValidation() {
        val schema = JsonSchema.obj("Test")
            .property("someProp", JsonSchema.string())
            .property(
                "nested",
                JsonSchema.obj("Nested")
                    .property("someProp2", JsonSchema.string())
            )
            .build()
        val ptr = "/nested"

        val path = JsonPath.parse(ptr)
        assertNotNull(path)
        val patch = AddJsonPatch(path!!, JsonNodeFactory.instance.objectNode().put("someProp2", "test"))

        assertTrue(patch.validate(schema))
    }

    @Test
    fun testNestedPathValidation() {
        val schema = JsonSchema.obj("Test")
            .property("someProp", JsonSchema.string())
            .property(
                "nested",
                JsonSchema.obj("Nested")
                    .property("someProp2", JsonSchema.string())
            )
            .build()
        val ptr = "/nested/someProp2"

        val path = JsonPath.parse(ptr)
        assertNotNull(path)
        val patch = AddJsonPatch(path!!, TextNode.valueOf("test"))

        assertTrue(patch.validate(schema))
    }

    @Test
    fun testNestedInvalidPathValidation() {
        val schema = JsonSchema.obj("Test")
            .property("someProp", JsonSchema.string())
            .property(
                "nested",
                JsonSchema.obj("Nested")
                    .property("someProp2", JsonSchema.string())
            )
            .build()
        val ptr = "/nested/someProp"

        val path = JsonPath.parse(ptr)
        assertNotNull(path)
        val patch = AddJsonPatch(path!!, TextNode.valueOf("test"))

        assertFalse(patch.validate(schema))
    }

    @Test
    fun testApplyReplace() {
        val schema = JsonSchema.obj("Test")
            .property("someProp", JsonSchema.string())
            .property(
                "nested",
                JsonSchema.obj("Nested")
                    .property("someProp2", JsonSchema.string())
            )
            .build()
        val ptr = "/nested/someProp2"

        val path = JsonPath.parse(ptr)
        assertNotNull(path)
        val patch = ReplaceJsonPatch(path!!, TextNode.valueOf("test"))
        val obj = JsonNodeFactory.instance.objectNode()
            .put("someProp", "someValue")
        obj.putObject("nested")
            .put("someProp2", "someValue2")

        assertTrue(patch.apply(schema, obj))

        val nested = obj.get("nested")
        assertNotNull(nested)
        assertInstanceOf(ObjectNode::class.java, nested)
        val someProp2 = nested.get("someProp2")
        assertInstanceOf(TextNode::class.java, someProp2)
        someProp2 as TextNode
        assertEquals("test", someProp2.textValue())
    }

    @Test
    fun testApplyReplaceArrayItem() {
        val schema = JsonSchema.obj("Test")
            .property("someProp", JsonSchema.string())
            .property(
                "nested",
                JsonSchema.array(JsonSchema.string())
            )
            .build()
        val ptr = "/nested/0"

        val path = JsonPath.parse(ptr)
        assertNotNull(path)
        val patch = ReplaceJsonPatch(path!!, TextNode.valueOf("test"))
        val obj = JsonNodeFactory.instance.objectNode()
            .put("someProp", "someValue")
        obj.putArray("nested")
            .add("someValue2")

        assertTrue(patch.apply(schema, obj))

        val nested = obj.get("nested")
        assertNotNull(nested)
        assertInstanceOf(ArrayNode::class.java, nested)
        val someProp2 = nested.get(0)
        assertInstanceOf(TextNode::class.java, someProp2)
        someProp2 as TextNode
        assertEquals("test", someProp2.textValue())
    }

    @Test
    fun testApplyAddArrayItem() {
        val schema = JsonSchema.obj("Test")
            .property("someProp", JsonSchema.string())
            .property(
                "nested",
                JsonSchema.array(JsonSchema.string())
            )
            .build()
        val ptr = "/nested/0"

        val path = JsonPath.parse(ptr)
        assertNotNull(path)
        val patch = AddJsonPatch(path!!, TextNode.valueOf("test"))
        val obj = JsonNodeFactory.instance.objectNode()
            .put("someProp", "someValue")
        obj.putArray("nested")

        assertTrue(patch.apply(schema, obj))

        val nested = obj.get("nested")
        assertNotNull(nested)
        assertInstanceOf(ArrayNode::class.java, nested)
        val someProp2 = nested.get(0)
        assertInstanceOf(TextNode::class.java, someProp2)
        someProp2 as TextNode
        assertEquals("test", someProp2.textValue())
    }
}

package io.koil.schemas

import com.fasterxml.jackson.databind.node.JsonNodeFactory
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class ObjectJsonSchemaTest {
    @Test
    fun testPolymorphicValidation() {
        val subType = JsonSchema.obj("SubType")
            .polymorphicValue("val1")
            .property("prop", JsonSchema.string())
            .build()

        val parentType = JsonSchema.obj("ParentType")
            .property(
                JsonSchemaObjectProperty(
                    "type",
                    JsonSchema.string()
                        .enum(setOf("val1"))
                        .build(),
                    true,
                    false,
                    "type",
                    emptySet(),
                    null,
                )
            )
            .subType(subType)
            .build()

        val value = JsonNodeFactory.instance.objectNode()
            .put("type", "val1")
            .put("prop", "something")

        assertNull(parentType.validate(false, value))
    }

    @Test
    fun testPolymorphicValidationUnknownProperty() {
        val subType = JsonSchema.obj("SubType")
            .polymorphicValue("val1")
            .property("prop", JsonSchema.string())
            .build()

        val parentType = JsonSchema.obj("ParentType")
            .property(
                JsonSchemaObjectProperty(
                    "type",
                    JsonSchema.string()
                        .enum(setOf("val1"))
                        .build(),
                    true,
                    false,
                    "type",
                    emptySet(),
                    null,
                )
            )
            .subType(subType)
            .build()

        val value = JsonNodeFactory.instance.objectNode()
            .put("type", "val1")
            .put("prop", "something")
            .put("unknown", 0)

        val err = parentType.validate(false, value)
        assertNotNull(err)
        assertEquals(ValidationConstraintError("unknownProperty", "unknown property unknown"), err)
    }
}

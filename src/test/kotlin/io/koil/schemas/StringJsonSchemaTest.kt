package io.koil.schemas

import com.fasterxml.jackson.databind.node.TextNode
import io.koil.core.toBase62String
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.util.*

class StringJsonSchemaTest {
    @Test
    fun validateRegularUUIDValidation() {
        val schema = JsonSchema.uuid().build()
        val value = UUID.randomUUID().toString()

        val result = schema.validate(false, TextNode.valueOf(value))
        assertNull(result)
    }

    @Test
    fun validateBase62UUIDValidation() {
        val schema = JsonSchema.uuid().build()
        val value = UUID.randomUUID().toBase62String()

        val result = schema.validate(false, TextNode.valueOf(value))
        assertNull(result)
    }

    @Test
    fun validateEmptyUUIDValidation() {
        val schema = JsonSchema.uuid().build()
        val value = UUID.fromString("00000000-0000-0000-0000-000000000000").toString()

        val result = schema.validate(false, TextNode.valueOf(value))
        assertNull(result)
    }

    @Test
    fun validateEmptyBase62UUIDValidation() {
        val schema = JsonSchema.uuid().build()
        val value = UUID.fromString("00000000-0000-0000-0000-000000000000").toBase62String()

        val result = schema.validate(false, TextNode.valueOf(value))
        assertNull(result)
    }

    @Test
    fun validateInvalidBase62UUIDValidation() {
        val schema = JsonSchema.uuid().build()
        val value = "123456789azertyuio%%p"

        val result = schema.validate(false, TextNode.valueOf(value))
        assertNotNull(result)
        assertEquals("type", result?.constraint)
        assertEquals("is not a UUID", result?.message)
    }
}
